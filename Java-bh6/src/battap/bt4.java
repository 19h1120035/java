package battap;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class bt4 {
	public static void main(String[] args) throws IOException {
		System.out.println("====Exception Exercies====");
		System.out.println("19H1120035 - Đào Văn Thương");
		FileInputStream fi = null;
		FileOutputStream fo = null;
		try {
			Scanner sc = new Scanner(System.in);
			System.out.println("Nhập file nguồn: ");
			fi = new FileInputStream(sc.nextLine());
			System.out.println("Nhập file đích: ");
			fo = new FileOutputStream(sc.nextLine());
			int ch;
			while ((ch = fi.read()) != -1) {
				System.out.print((char) ch);
				fo.write(ch);
			}
		} catch (FileNotFoundException e) {
			System.err.println("Not found source file !");
		} finally {
			if (fi != null) {
				fi.close();
			}
			if (fo != null) {
				fo.close();
			}
		}

	}

}
