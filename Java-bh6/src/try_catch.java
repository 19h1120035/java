import java.util.Iterator;

public class try_catch {
	public static void main(String[] args) {
		problem();
	}

	static public void problem() {
		System.out.println("====Exception Exercies====");
		System.out.println("19H1120040-Nguyễn Đoàn Anh Tuấn");
		float s = 0;
		int num_orr = 0;
		for (int i = 1; i < 2021; i++) {
			try {
				s += (i * i * i + i * i + i) / (i % 123);
			} catch (Exception ex) {
				num_orr++;
			}
		}
		System.out.println("Số giá trị i gây ra lỗi là: " + num_orr);
		System.out.println("Kết quả giá trị của s là: " + s);
		System.out.println("Everything is OK! Bye.");
	}
}
