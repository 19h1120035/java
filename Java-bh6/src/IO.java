import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class IO {
	public static void main(String[] args) {
		ghiFile();
		docFile();
//		docFileByBufferChar();
	}

	public static void ghiFile() {
		FileOutputStream out = null;
		try {
			File f = new File("thongtin.dat");
			out = new FileOutputStream(f);
			Scanner sc = new Scanner(System.in);
			System.out.println("Nhập MSSV: ");
			String mssv = sc.nextLine();
			out.write(mssv.getBytes());
			System.out.println("Nhập họ và tên: ");
			String hoTen = sc.nextLine();
			out.write(hoTen.getBytes());
			System.out.println("Nhập năm sinh của bạn: ");
			String namSinh = sc.nextLine();
			out.write(namSinh.getBytes());
		} catch (Exception e) {
		}

	}

	public static void docFile() {
		File f = new File("thongtin.dat");
		FileReader fr = null;
		System.out.println("Xuất file: ");
		try {
			if (!f.exists()) {
				throw new Exception("File khong ton tai");
			}
			fr = new FileReader(f);
			int kq;
			while ((kq = fr.read()) != -1) {
				System.out.print((char) kq);
			}
		} catch (Exception ex) {
		}

	}
//	public static void docFileByBufferChar() {
//		try {
//		File f = new File("thongtin.dat");
//		FileReader read = new FileReader(f);
//		BufferedReader b = new BufferedReader(read);
//		String d;
//		while ((d = b.readLine()) != null) {
//		System.out.println(d);
//		}
//		b.close();
//		} catch (Exception ex) {/*...*/}
//		}
}
