package OOP_15;

public class MayBay extends PhuongTienDiChuyen {
	private String loaiNhienLieu;

	public MayBay(HangSanXuat hangSanXuat, String loaiNhienLieu) {
		super("Máy Bay", hangSanXuat);
		this.loaiNhienLieu = loaiNhienLieu;
	}

	public String getLoaiNhienLieu() {
		return loaiNhienLieu;
	}

	public void setLoaiNhienLieu(String loaiNhienLieu) {
		this.loaiNhienLieu = loaiNhienLieu;
	}

	public void catCanh() {
		System.out.println("Cất  Cánh!");
	}

	public void haCanh() {
		System.out.println("Hạ Cánh!");
	}

	@Override
	public double layVanToc() {
		return 500;
	}

}
