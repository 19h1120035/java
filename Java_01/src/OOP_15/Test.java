package OOP_15;
public class Test {
	public static void main(String[] args) {

		HangSanXuat hsx1 = new HangSanXuat("VietNamAirLine", "Mỹ");
		HangSanXuat hsx2 = new HangSanXuat("Toyota", "Nhật Bản");
		HangSanXuat hsx3 = new HangSanXuat("Xe đạp", "Việt Nam");

		MayBay pt1 = new MayBay(hsx1, "Dầu");
		PhuongTienDiChuyen pt2 = new XeOto(hsx2, "Xăng");
		PhuongTienDiChuyen pt3 = new XeDap(hsx3);

		System.out.println("===Lấy Hãng Sản Xuất===");
		System.out.println("\tMáy bay: "+pt1.layTenHangSanXuat());
		System.out.println("\tXe ô tô: "+pt2.layTenHangSanXuat());
		System.out.println("\tXe đạp: "+pt3.layTenHangSanXuat());
		System.out.println("===Tên Quốc Gia===");
		System.out.println("\tMáy bay: "+pt1.layTenQuocGia());
		System.out.println("\tXe ô tô: "+pt2.layTenQuocGia());
		System.out.println("\tXe đạp: "+pt3.layTenQuocGia());
		pt1.batDau();
		System.out.println("Máy bay: ");
		pt1.catCanh();
		pt1.tangToc();
		System.out.println("Lấy vận tốc: ");
		System.out.println("Máy bay: "+ pt1.layVanToc());
		System.out.println("Xe Ô tô: "+ pt2.layVanToc());
		System.out.println("Xe đạpi: "+ pt3.layVanToc());
		System.out.println("Hạ cánh: ");
		pt1.haCanh();
		pt1.dungLai();
	}

}
