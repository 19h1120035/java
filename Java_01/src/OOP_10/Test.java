package OOP_10;
public class Test {
	public static void main(String[] args) {
		Dog d = new Dog();
		BabyDog bbd = new BabyDog();
		Cat c= new Cat();
		Bird b= new Bird();
		d.eat();
		d.bark();
		bbd.eat();
		bbd.bark();
		bbd.weep();
		c.eat();
		c.meow();
		b.eat();
		b.fly();
		
	}
}
