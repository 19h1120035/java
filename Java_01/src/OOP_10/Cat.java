package OOP_10;
public class Cat extends Animal {

	public Cat() {
		super("Cat");
	}

	public void meow() {
		System.out.println("Meo Meo");
	}

}
