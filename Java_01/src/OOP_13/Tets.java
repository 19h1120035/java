package OOP_13;
public class Tets {
	public static void main(String[] args) {
		MyMath mm = new MyMath();
		System.out.println("Min(4,6) = "+mm.timMin(4, 6));
		System.out.println("Min(5.5,6.0) = "+mm.timMin(5.5, 6.0));
		System.out.println("Sum(5,6) = "+ mm.sum(5.5,6.0));
		double arr[] = new double[] {1,2,3,4,5};
		System.out.println("Sum(arr) = "+ mm.sum(arr));
		
	}
}
