package OOP_14;

public abstract class Hinh {
	public ToaDo getToaDo() {
		return toaDo;
	}

	public void setToaDo(ToaDo toaDo) {
		this.toaDo = toaDo;
	}

	protected ToaDo toaDo;

	public Hinh(ToaDo toaDo) {
		this.toaDo = toaDo;
	}
	public abstract double tinhDienTich();
}
