package OOP_14;

//trừu tượng abstract
public class Test {
	public static void main(String[] args) {
		ToaDo td1 = new ToaDo(4, 5);
		ToaDo td2 = new ToaDo(1, 3);
		ToaDo td3 = new ToaDo(2, 7);

//		Hinh h1 = new Hinh(td1); => lỗi biên dịch
		Hinh h1 = new Diem(td1);
		Hinh h2 = new HinhTron(td2, 10);
		Hinh h3 = new HinhChuNhat(td3, 5, 10);
		System.out.println("Area 1: "+ h1.tinhDienTich());
		System.out.println("Area 2: "+ h2.tinhDienTich());
		System.out.println("Area 3: "+ h3.tinhDienTich());
	}
}
