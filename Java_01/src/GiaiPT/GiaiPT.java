package GiaiPT;
import java.util.Scanner;

public class GiaiPT {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("=========GIAI PHUONG TRINH==========");
		System.out.println("1. Giai phuong trinh bac 1.         |");
		System.out.println("2. Giai phuong trinh bac 2.         |");
		System.out.println("0. Ket thuc.                        |");
		System.out.println("=====================================");
		while (true) {
			System.out.println("Nhap lua chon cua ban: ");

			int chon = sc.nextInt();
			switch (chon) {
			case 1: {
				GiaiPT.giaiPtB1();
				break;
			}
			case 2: {
				GiaiPT.giaiPtB2();
				break;
			}
			case 0:
				System.out.println("Tam biet !!");
				System.exit(0);
			default:
				System.err.println("Nhap sai ! Vui long nhap lai.");
			}
		}
	}

	public static void giaiPtB1() {
		double a, b;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap a: ");
		a = sc.nextDouble();
		System.out.println("Nhap b: ");
		b = sc.nextDouble();
		if (a == 0) {
			if (b == 0)
				System.out.println("Phuong trinh vo so nghiem !!!");
			else
				System.out.println("Phuong trinh vo nghiem !!!");
		} else
			System.out.println("Phuong trinh co nghiem la: x = " + (-b) / a);
	}

	public static void giaiPtB2() {
		int a, b, c;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap a: ");
		a = sc.nextInt();
		System.out.println("Nhap b: ");
		b = sc.nextInt();
		System.out.println("Nhap c: ");
		c = sc.nextInt();
		double delta = Math.pow(b, 2) - 4 * a * c;
		System.out.println("Delta = " + delta);

		if (delta < 0) {
			System.out.println("Phuong trinh vo nghiem !!!");
		} else if (delta == 0)
			System.out.println("Phuong trinh co 1 nghiem duy nhat la: x = " + (-b) / 2 * a);
		else
			System.out.println("Phuong trinh co 2 nghiem la: x1 = " + (-(b) + Math.sqrt(delta)) / 2 * a + " va x2 = "
					+ (-(b) - Math.sqrt(delta)) / 2 * a);

	}
}
