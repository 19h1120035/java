package GiaiPT;
import java.util.Scanner;

public class GiaiPTB2 {
	public static void main(String[] args) {
		int a, b, c;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap a: ");
		a = sc.nextInt();
		System.out.println("Nhap b: ");
		b = sc.nextInt();
		System.out.println("Nhap c: ");
		c = sc.nextInt();
		double delta = Math.pow(b, 2) - 4 * a * c;
		System.out.println("Delta = " + delta);
		if (delta < 0) {
			System.out.println("Phuong trinh vo nghiem !!!");
		} else if (delta == 0)
			System.out.println("Phuong trinh co 1 nghiem duy nhat la: x = " + (-b) / 2 * a);
		else
			System.out.println("Phuong trinh co 2 nghiem la: x1 = " + (-(b) + Math.sqrt(delta)) / 2 * a + " va x2 = "
					+ (-(b) - Math.sqrt(delta)) / 2 * a);
	}
}
