package GiaiPT;
import java.util.Scanner;

public class GiaiPTB1 {
	// giai pt ax+b=0
	// neu a = 0 thi b = 0
	// neu a != 0 thi b = -b/a
	public static void main(String[] args) {
		double a, b;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap a: ");
		a = sc.nextDouble();
		System.out.println("Nhap b: ");
		b = sc.nextDouble();
		if (a == 0) {
			if (b == 0)
				System.out.println("Phuong trinh vo so nghiem !!!");
			else
				System.out.println("Phuong trinh vo nghiem !!!");
		} else
			System.out.println("Nghiem cua phuong trinh la: x = (-b)/a = " + (-b) / a);
	}
}