package GiaiPT;
import java.util.Scanner;

public class DienTichHinhTron {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap ban kinh R: ");
		double r = sc.nextDouble();
		double chuVi = 2 * Math.PI * r;
		double dienTich = Math.PI * Math.pow(r, 2);
		System.out.println("Chu vi cua hinh tron la: " + chuVi);
		System.out.println("Dien tich cua hinh tron la: " + dienTich);
		System.out.println("Chu vi = " + Math.round(chuVi));
		System.out.println("Dien tich = " + Math.round(dienTich));
		System.out.println("Chu vi = " + Math.round(chuVi * 100.0) / 100.0);
		System.out.println("Dien tich = " + Math.round(dienTich * 100.0) / 100.0);
	}
}
