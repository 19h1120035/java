package OOP_04;
public class Test {
	public static void main(String[] args) {
		MyDate md1 = new MyDate(20, 4, 2001);
		MyDate md2 = new MyDate(25, 5, 2010);
		MyDate md3 = new MyDate(20, 4, 2001);
		MyDate md4 = new MyDate(16, 11, 2031);
		MyDate md5 = new MyDate(25, 11, 4301);
		System.out.println(md1);
		System.out.println(md2);
		System.out.println(md3);
		System.out.println(md4);
		System.out.println(md5);

		int a = 5;
		int b = 6;
		if (a ==b) {
			System.out.println("a=b");
		}
		else 
			System.out.println("a!=b");
		
		//
		System.out.println("Md1 so sanh bang md2 "+ md1.equals(md2));
		System.out.println("Md1 so sanh bang md3 "+ md1.equals(md3));
		System.out.println("Md1 so sanh bang md4 "+ md1.equals(md4));
		System.out.println("Md1 so sanh bang md5 "+ md1.equals(md5));
		System.out.println("Md1 so sanh bang md1 "+ md1.equals(md1));
		System.out.println("Hash code md1 "+ md1.hashCode());
		System.out.println("Hash code md2 "+ md2.hashCode());
		System.out.println("Hash code md3 "+ md3.hashCode());
		System.out.println("Hash code md4 "+ md4.hashCode());
		System.out.println("Hash code md5 "+ md5.hashCode());
		
	}
}
