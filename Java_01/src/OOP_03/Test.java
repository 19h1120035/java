package OOP_03;
public class Test {
	public static void main(String[] args) {
		MyDate md = new MyDate(20, 1, 2021);
		System.out.println("Day = " + md.getDay());
		System.out.println("Month = " + md.getMonth());
		System.out.println("Year = " + md.getYear());
		md.setDay(25);
		System.out.println("Day: "+ md.getDay());
		md.setDay(35);
		System.out.println("Day: "+ md.getDay());
		md.setMonth(8);
		System.out.println("Month: "+ md.getMonth());
		md.setMonth(13);
		System.out.println("Month: "+ md.getMonth());
		md.setYear(2025);
		System.out.println("Year: "+md.getYear());
		md.setYear(-2020);
		System.out.println("Year: "+md.getYear());
	}
}
