package OOP_12;
public class Cat extends Animal {

	public Cat() {
		super("Cat");
	}

	@Override
	public void eat() {
		System.out.println("Tôi ăn cá!");
		super.eat();
	}

	@Override
	public void makeSound() {
		System.out.println("Meo Meo !");
		super.makeSound();
	}

}
