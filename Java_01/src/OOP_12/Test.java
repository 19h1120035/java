package OOP_12;
public class Test {
	public static void main(String[] args) {
		Dog d = new Dog();
		Cat c = new Cat();
		Bird b = new Bird();
		d.eat();
		d.makeSound();
		d.sleep();
		c.eat();
		c.makeSound();
		c.sleep();
		b.eat();
		b.makeSound();
		b.sleep();
	}
}
