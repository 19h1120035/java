package OOP_12;
public class Dog extends Animal {

	public Dog() {
		super("Dog");
	}

	@Override
	public void eat() {
		System.out.println("Tôi ăn xương!");
		super.eat();
	}

	@Override
	public void makeSound() {
		System.out.println("Gâu Gâu!");
		super.makeSound();
	}

}
