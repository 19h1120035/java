package OOP_12;
// Ghi đè phương thức
public class Bird extends Animal {

	public Bird() {
		super("Bird");
	}

	@Override
	public void eat() {
		System.out.println("Tôi ăn cào cào !");
		super.eat();
	}

	@Override
	public void makeSound() {
		System.out.println("Líu lo!");
		super.makeSound();
	}

}
