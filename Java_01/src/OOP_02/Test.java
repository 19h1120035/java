package OOP_02;
import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		HoaDonCaPhe hd = new HoaDonCaPhe("Trung Nguyen", 100000, 10);
		System.out.println("Tong tien: "+ hd.tinhTongTien());
		System.out.println("Kiem tra khoi luong so voi 8kg: "+ hd.kiemTraKhoiLuongLonHon(8));
		System.out.println("Kiem tra khoi luong so voi 2kg: "+ hd.kiemTraKhoiLuongLonHon(2));
		System.out.println("Kiem tra gia tien so voi 500kVND: "+ hd.kiemTraGiaTien());
		System.out.println("So tien giam gia la: "+ hd.giamGia(10));
		System.out.println("Tong so tien KH phai tra khi duoc giam gia: "+ hd.soTienKHTraKhiGiam(10));
	}
}
