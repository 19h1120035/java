package OOP_09;
public class Test {
	public static void main(String[] args) {
		ConNguoi cn1= new ConNguoi("Đào Văn Thương", 2001);
		cn1.an();
		cn1.uong();
		cn1.ngu();
		
		HocSinh hs = new HocSinh("Đào Văn Thương", 2001, "12a6", "THPT Ngô Mây");
		hs.lamBaiTap();
		hs.an();
		hs.uong();
		hs.ngu();
	}
}
