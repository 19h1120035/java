package OOP_06;
/*
 * Xây dựng chương trình Java quản lí các bộ phim tại một rạp chiếu phim. 
 * Một bộ phim bao gồm các thông tin: tên phim, năm sản xuất, hãng sản xuât, giá vé, ngày chiếu. 
 * Hãng sản xuất bao gồm tên hãng sản xuất, quốc gia. 
 * Ngày chiếu bao gồm thông tin ngày, tháng, năm.
 * -Xây dựng lớp và thực hiện các phương thức sau: 
 * a. Kiểm tra xem giá vé của 1 bộ phim bất kì có rẻ hơn giá vé của 1 phim khác hay không?
 * b. Cho biết tên của hãng sản xuất phim?
 * c. Cho biết giá vé của phim khi có khuyến mãi (ví dụ như sau khi khuyeesns mãi 10%, 20%,...,), số tiến khuyến mãi được giảm trừ theo % giá bán.
 */
public class TEST {
	public static void main(String[] args) {
		Ngay ngay1 = new Ngay(15, 10, 2021);
		Ngay ngay2 = new Ngay(20, 04, 2020);
		Ngay ngay3 = new Ngay(16, 02, 2025);

		HangSanXuat hang1 = new HangSanXuat("Hãng A", "Việt Nam");
		HangSanXuat hang2 = new HangSanXuat("Hãng B", "Mỹ");
		HangSanXuat hang3 = new HangSanXuat("Hãng C", "Hàn Quốc");

		BoPhim phim1 = new BoPhim("Bố Già", 2020, hang1, 90000, ngay2);
		BoPhim phim2 = new BoPhim("Gái Già Lắm Chiêu", 2021, hang3, 100000, ngay1);
		BoPhim phim3 = new BoPhim("Chạy", 2024, hang2, 120000, ngay3);

		System.out.println("So sánh giá phim 1 rẻ hơn phim 2: " + phim1.kiemTraGiaVeReHon(phim2));
		System.out.println("So sánh giá phim 1 rẻ hơn phim 3: " + phim1.kiemTraGiaVeReHon(phim3));
		System.out.println("So sánh giá phim 2 rẻ hơn phim 3: " + phim2.kiemTraGiaVeReHon(phim3));
		System.out.println("So sánh giá phim 3 rẻ hơn phim 2: " + phim3.kiemTraGiaVeReHon(phim2));
		System.out.println("So sánh giá phim 3 rẻ hơn phim 1: " + phim3.kiemTraGiaVeReHon(phim1));

		System.out.println("\nTên của hãng Bộ phim 1 : " + phim1.layTenHangSanXuat());
		System.out.println("Tên của hãng Bộ phim 2 : " + phim2.layTenHangSanXuat());
		System.out.println("Tên của hãng Bộ phim 3 : " + phim3.layTenHangSanXuat());

		System.out.println("\nGiá vé sau khi được khuyến mãi " + phim1.giaVeSauKhiKhuyenMai(10));
		System.out.println("Giá vé sau khi được khuyến mãi " + phim2.giaVeSauKhiKhuyenMai(20));
		System.out.println("Giá vé sau khi được khuyến mãi " + phim3.giaVeSauKhiKhuyenMai(50));

	}
}
