package OOP_05;

public class Test {
	public static void main(String[] args) {
		Ngay ngay1 = new Ngay(20, 04, 2021);
		Ngay ngay2 = new Ngay(25, 11, 2020);
		Ngay ngay3 = new Ngay(20, 04, 2011);

		TacGia tacGia1 = new TacGia("Đào Văn Thương", ngay1);
		TacGia tacGia2 = new TacGia("Dương Bình Định", ngay2);
		TacGia tacGia3 = new TacGia("Hà Kiều Trang", ngay3);

		QuanLySach sach1 = new QuanLySach("Lập Trình Java", 5000, 2010, tacGia1);
		QuanLySach sach2 = new QuanLySach("Lập Trình C++", 10000, 2015, tacGia2);
		QuanLySach sach3 = new QuanLySach("Lập Trình Web", 50000, 2010, tacGia3);
		sach1.toString();
		sach2.toString();
		sach3.toString();
		System.out.println("Thông tin của sách: ");
		System.out.println("\tSách 1:" + sach1);
		System.out.println("\tSách 2: " + sach2);
		System.out.println("\tSách 3: " + sach3);
		System.out.println("Kiểm tra có cùng năm xuất bản: ");
		System.out.println("\tSách 1 và Sách 2 : " + sach1.kiemTraCungNamXB(sach2));
		System.out.println("\tSách 1 và Sách 3 : " + sach1.kiemTraCungNamXB(sach3));
		System.out.println("\tSách 1 và Sách 3 : " + sach1.kiemTraCungNamXB(sach3));
		System.out.println("Giá của sách sau khi giảm:");
		System.out.println("\tSách 1 giảm 10%: " + sach1.giaSauKhiGiam(10));
		System.out.println("\tSách 2 giảm 20%: " + sach2.giaSauKhiGiam(20));
		System.out.println("\tSách 3 giảm 50%: " + sach3.giaSauKhiGiam(50));

	}
}
