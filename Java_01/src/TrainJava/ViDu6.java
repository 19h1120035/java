package TrainJava;
import java.util.Scanner;

public class ViDu6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap vao so a: ");
		double a = sc.nextDouble();
		System.out.println("Nhap vao so b: ");
		double b = sc.nextDouble();
		// Gia tri tuyet doi
		System.out.println(Math.abs(a));
		// Gia tri nho nhat
		System.out.println(Math.min(a, b));
		// Gia tri lon nhat
		System.out.println(Math.max(a, b));
		System.out.println(Math.ceil(a));
		System.out.println(Math.floor(a));
		System.out.println(Math.sqrt(b));
		System.out.println(Math.pow(a, b));
	}
}
