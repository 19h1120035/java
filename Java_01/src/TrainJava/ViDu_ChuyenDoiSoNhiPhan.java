package TrainJava;
import java.util.Scanner;

public class ViDu_ChuyenDoiSoNhiPhan {
	public static void main(String[] args) {
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap vao so nguyen n: ");
		n = sc.nextInt();
		String nhiPhan = "";
		while (n > 0) {
			nhiPhan = (n % 2) + nhiPhan;
			n /= 2;
		}
		System.out.println("So he nhi phan la: " + nhiPhan);
		/*
		 * chuyển đổi số thập phân thành số nhị phan chia liên tục 10:2=5 dư 0 5:2=2 dư
		 * 1 2:2=1 dư 0 1:2=0 dư 1 => 1010
		 * 
		 */
	}
}
