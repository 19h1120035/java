package TrainJava;

public class ViDu4 {
	public static void main(String[] args) {
		double a = 5;
		boolean b = false;
		System.out.println("- => gia tri" + (-a));
		System.out.println("+ => gia tri" + (+a));
		System.out.println("!b => gia tri" + (!b));
		System.out.println("- => gia tri" + (-a));
		System.out.println("=========");
		System.out.println("a = " + a);
		System.out.println(a++);
		System.out.println(++a);
		System.out.println("a = " + a);
		System.out.println("a-- = " + (a--));
		System.out.println("--a = " + (--a));
		System.out.println("a = " + a);
	}
}
