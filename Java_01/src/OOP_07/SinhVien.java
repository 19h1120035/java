package OOP_07;
public class SinhVien {
	private String mssv;
	private String hoVaTen;
	private Ngay ngaySinh;
	private double diemTrungBinh;
	private Lop lop;

	public SinhVien(String mssv, String hoVaTen, Ngay ngaySinh, double diemTrungBinh, Lop lop) {

		this.mssv = mssv;
		this.hoVaTen = hoVaTen;
		this.ngaySinh = ngaySinh;
		this.diemTrungBinh = diemTrungBinh;
		this.lop = lop;
	}

	public String getMssv() {
		return mssv;
	}

	public void setMssv(String mssv) {
		this.mssv = mssv;
	}

	public String getHoVaTen() {
		return hoVaTen;
	}

	public void setHoVaTen(String hoVaTen) {
		this.hoVaTen = hoVaTen;
	}

	public Ngay getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(Ngay ngaySinh) {
		this.ngaySinh = ngaySinh;
	}

	public double getDiemTrungBinh() {
		return diemTrungBinh;
	}

	public void setDiemTrungBinh(double diemTrungBinh) {
		this.diemTrungBinh = diemTrungBinh;
	}

	public Lop getLop() {
		return lop;
	}

	public void setLop(Lop lop) {
		this.lop = lop;
	}

	public String layTenKhoa() {
		return this.lop.getTenKhoa();
	}

	public boolean kiemTraSinhVienDau() {
		return this.diemTrungBinh >= 5.0;
	}

	public boolean kiemTraSVCoNgaySinhGiongVoiSVKhac(SinhVien svKhac) {
		return this.ngaySinh.equals(svKhac.ngaySinh);
	}
}
