package OOP_07;
/*
 * Xây dựng chương trình java hỗ trợ quản lý thông tin sinh viên
 * Sinh viên bao gồm mã số sinh viên, họ và tên, ngày tháng năm sinh, điểm trung bình, lớp.
 * Ngày sinh bao gồm ngày tháng năm.
 * Lớp bao gồm tên lớp và tên khoa.
 * -Xây dựng lớp và thực hiện các phướng thức sáu:
 * a. Cho biết tên khoa mà sinh viên đang theo học.
 * b. Cho biết học sinh có đậu hay không (điểm trung bình >= 5.0)
 * c. Kiểm tra sinh viên có ngày sinh giống với ngày sinh của một sinh viên khác hay không
 * 
 */
public class TEST {
	public static void main(String[] args) {
		Ngay ngaySinh1 = new Ngay(20, 04, 2001);
		Ngay ngaySinh2 = new Ngay(25, 11, 2000);
		Ngay ngaySinh3 = new Ngay(20, 04, 2001);

		Lop lop1 = new Lop("CN19CLCA", "Công nghê thông tin");
		Lop lop2 = new Lop("HH19CLCA", "Kinh tế Logistic");
		Lop lop3 = new Lop("QL19CLCA", "Kinh tế vận tải biển");

		SinhVien sv1 = new SinhVien("19H1120035", "Đào Văn Thương", ngaySinh1, 8.2, lop1);
		SinhVien sv2 = new SinhVien("19H1120045", "Nguyễn Ngọc Trung", ngaySinh3, 4.9, lop2);
		SinhVien sv3 = new SinhVien("19H1120005", "Nguyễn Thúy Vy", ngaySinh2, 5.0, lop3);
		
		System.out.println("Khoa mà sinh viên 1 đang theo học: "+ sv1.layTenKhoa());
		System.out.println("Khoa mà sinh viên 2 đang theo học: "+ sv2.layTenKhoa());
		System.out.println("Khoa mà sinh viên 3 đang theo học: "+ sv3.layTenKhoa());
		
		System.out.println("\nKiểm tra sinh viên 1 có đậu : "+ sv1.kiemTraSinhVienDau());
		System.out.println("Kiểm tra sinh viên 2 có đậu : "+ sv2.kiemTraSinhVienDau());
		System.out.println("Kiểm tra sinh viên 3 có đậu : "+ sv3.kiemTraSinhVienDau());
		
		System.out.println("\nKiểm tra sinh viên 1 có trùng ngày sinh với sinh viên 2 : "+ sv1.kiemTraSVCoNgaySinhGiongVoiSVKhac(sv2));
		System.out.println("Kiểm tra sinh viên 1 có trùng ngày sinh với sinh viên 3 : "+ sv1.kiemTraSVCoNgaySinhGiongVoiSVKhac(sv3));
		System.out.println("Kiểm tra sinh viên 2 có trùng ngày sinh với sinh viên 3 : "+ sv2.kiemTraSVCoNgaySinhGiongVoiSVKhac(sv3));
		
		
		
		

	}
}
