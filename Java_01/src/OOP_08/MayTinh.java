package OOP_08;

public class MayTinh {
	private HangSanXuat hangSanXuat;
	private Ngay ngaySanXuat;
	private double giaBan;
	private double thoiGianBaoHanhTheoThang;

	public MayTinh(HangSanXuat hangSanXuat, Ngay ngaySanXuat, double giaBan, double thoiGianBaoHanhTheoThang) {
		this.hangSanXuat = hangSanXuat;
		this.ngaySanXuat = ngaySanXuat;
		this.giaBan = giaBan;
		this.thoiGianBaoHanhTheoThang = thoiGianBaoHanhTheoThang;
	}

	public HangSanXuat getHangSanXuat() {
		return hangSanXuat;
	}

	public void setHangSanXuat(HangSanXuat hangSanXuat) {
		this.hangSanXuat = hangSanXuat;
	}

	public Ngay getNgaySanXuat() {
		return ngaySanXuat;
	}

	public void setNgaySanXuat(Ngay ngaySanXuat) {
		this.ngaySanXuat = ngaySanXuat;
	}

	public double getGiaBan() {
		return giaBan;
	}

	public void setGiaBan(double giaBan) {
		this.giaBan = giaBan;
	}

	public double getThoiGianBaoHanhTheoThang() {
		return thoiGianBaoHanhTheoThang;
	}

	public void setThoiGianBaoHanhTheoThang(double thoiGianBaoHanhTheoThang) {
		this.thoiGianBaoHanhTheoThang = thoiGianBaoHanhTheoThang;
	}

	boolean kiemTraGiaBanThapHon(MayTinh mtKhac) {
		return this.giaBan < mtKhac.giaBan;
	}

	public String layTenQuocGia() {
		return this.hangSanXuat.layTenQuocGia();

	}

	@Override
	public String toString() {
		return hangSanXuat +"\n"+ ngaySanXuat + "\n\t\tGiá bán: " + giaBan + " USD"
				+ "\n\t\tThời gian bảo hành: " + thoiGianBaoHanhTheoThang + " tháng";
	}
}
