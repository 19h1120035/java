package OOP_08;
public class Test {
	public static void main(String[] args) {
		Ngay ngay1 = new Ngay(15, 05, 1992);
		Ngay ngay2 = new Ngay(20, 04, 2000);
		Ngay ngay3 = new Ngay(30, 03, 1995);
		Ngay ngay4 = new Ngay(12, 07, 1989);

		QuocGia qg1 = new QuocGia("Việt Nam", "VN");
		QuocGia qg2 = new QuocGia("Mỹ", "USA");
		QuocGia qg3 = new QuocGia("Thái Lan", "TL");
		QuocGia qg4 = new QuocGia("Trung Quốc", "TQ");

		HangSanXuat hang1 = new HangSanXuat("ASUS", qg1);
		HangSanXuat hang2 = new HangSanXuat("DELL", qg2);
		HangSanXuat hang3 = new HangSanXuat("LENOVO", qg3);
		HangSanXuat hang4 = new HangSanXuat("PHP", qg4);

		MayTinh mt1 = new MayTinh(hang1, ngay1, 1000, 24);
		MayTinh mt2 = new MayTinh(hang2, ngay2, 2000, 12);
		MayTinh mt3 = new MayTinh(hang3, ngay3, 1500, 18);
		MayTinh mt4 = new MayTinh(hang4, ngay4, 1200, 06);
		mt1.toString();
		mt2.toString();
		mt3.toString();
		mt4.toString();
		System.out.println("Thông tin máy tính:");
		System.out.println("\tMáy tính 1:" + mt1);
		System.out.println("\tMáy tính 2:" + mt2);
		System.out.println("\tMáy tính 3:" + mt2);
		System.out.println("\tMáy tính 4:" + mt2);

		System.out.println("So sánh giá bán:");
		System.out.println("\tMT1 < MT2: " + mt1.kiemTraGiaBanThapHon(mt2));
		System.out.println("\tMT1 < MT3: " + mt1.kiemTraGiaBanThapHon(mt3));
		System.out.println("\tMT1 < MT4: " + mt1.kiemTraGiaBanThapHon(mt4));
		System.out.println("\n\tMT2 < MT1: " + mt2.kiemTraGiaBanThapHon(mt1));
		System.out.println("\tMT2 < MT3: " + mt2.kiemTraGiaBanThapHon(mt3));
		System.out.println("\tMT2 < MT4: " + mt2.kiemTraGiaBanThapHon(mt4));
		System.out.println("\n\tMT3 < MT1: " + mt3.kiemTraGiaBanThapHon(mt1));
		System.out.println("\tMT3 < MT2: " + mt3.kiemTraGiaBanThapHon(mt2));
		System.out.println("\tMT3 < MT4: " + mt3.kiemTraGiaBanThapHon(mt4));
		System.out.println("\n\tMT4 < MT1: " + mt4.kiemTraGiaBanThapHon(mt1));
		System.out.println("\tMT4 < MT2: " + mt4.kiemTraGiaBanThapHon(mt2));
		System.out.println("\tMT4 < MT3: " + mt4.kiemTraGiaBanThapHon(mt3));

		System.out.println("Tên quốc gia:");
		System.out.println("\tMáy tính 1: " + mt1.layTenQuocGia());
		System.out.println("\tMáy tính 2: " + mt2.layTenQuocGia());
		System.out.println("\tMáy tính 3: " + mt3.layTenQuocGia());
		System.out.println("\tMáy tính 4: " + mt4.layTenQuocGia());

	}
}
