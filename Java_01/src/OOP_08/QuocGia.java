package OOP_08;

public class QuocGia {
	private String tenQuocGia;
	private String maQuocGia;

	public QuocGia(String tenQuocGia, String maQuocGia) {
		this.tenQuocGia = tenQuocGia;
		this.maQuocGia = maQuocGia;
	}

	public String getTenQuocGia() {
		return tenQuocGia;
	}

	public void setTenQuocGia(String tenQuocGia) {
		this.tenQuocGia = tenQuocGia;
	}

	public String getMaQuocGia() {
		return maQuocGia;
	}

	public void setMaQuocGia(String maQuocGia) {
		this.maQuocGia = maQuocGia;
	}

	@Override
	public String toString() {
		return "Nơi Sản Xuất: " + tenQuocGia + "\n\t\tMã sản xuất: " + maQuocGia;
	}

}
