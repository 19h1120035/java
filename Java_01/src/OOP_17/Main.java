package OOP_17;


public class Main {
	public static void main(String[] args) {
		Ngay ngaysinh1 = new Ngay(20, 6, 2001);
		Ngay ngaysinh2 = new Ngay(10, 2, 2000);
		Ngay ngaysinh3 = new Ngay(25, 11, 2004);
		Ngay ngaysinh4 = new Ngay(5, 8, 2002);

		NgayVaoLam ngayVaoLam1 = new NgayVaoLam(23, 3, 2018);
		NgayVaoLam ngayVaoLam2 = new NgayVaoLam(20, 5, 2019);
		NgayVaoLam ngayVaoLam3 = new NgayVaoLam(10, 12, 2020);
		NgayVaoLam ngayVaoLam4 = new NgayVaoLam(19, 6, 2019);

		CongThang cong1 = new CongThang(2, 27, 3);
		CongThang cong2 = new CongThang(3, 28, 2);
		CongThang cong3 = new CongThang(4, 25, 5);
		CongThang cong4 = new CongThang(3, 29, 1);

		NhanSu ns1 = new NhanSu("34341", "Đào Văn Thương", 3.5, ngayVaoLam1, ngaysinh1, cong1, "Giám Đốc");
		NhanSu ns2 = new NhanSu("57346", "Vũ Thị Hoa", 3.0, ngayVaoLam2, ngaysinh2, cong2, "Trưởng phòng");
		NhanSu ns3 = new NhanSu("43633", "Dương Bình Định", 2.5, ngayVaoLam3, ngaysinh3, cong3, "Nhân viên");
		NhanSu ns4 = new NhanSu("62465", "Lương Cao Bằng", 2.5, ngayVaoLam4, ngaysinh4, cong4, "Nhân viên");
		ns1.toString();
		ns2.toString();
		ns3.toString();
		ns4.toString();
		System.out.println("Danh sách nhân sự:");
		System.out.println("Nhân sự 1: " + ns1);
		System.out.println("Nhân sự 2: " + ns2);
		System.out.println("Nhân sự 3: " + ns3);
		System.out.println("Nhân sự 4: "+ ns4);
		System.out.println("Số lương:");
		System.out.println("\tNhân sự 1: " + ns1.soLuong() + " triệu VND");
		System.out.println("\tNhân sự 2: " + ns2.soLuong() + " triệu VND");
		System.out.println("\tNhân sự 3: " + ns3.soLuong() + " triệu VND");
		System.out.println("\tNhân sự 4: " + ns4.soLuong() + " triệu VND");
		System.out.println("Sinh nhật của các nhân sự trong mỗi quý của năm:");
		System.out.println("\tNhân sự 1: " + ns1.sinhNhatTrongQuy());
		System.out.println("\tNhân sự 2: " + ns2.sinhNhatTrongQuy());
		System.out.println("\tNhân sự 3: " + ns3.sinhNhatTrongQuy());
		System.out.println("\tNhân sự 4: " + ns4.sinhNhatTrongQuy());

	}
}
