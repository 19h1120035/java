package OOP_16;

public class SapXepChon implements SapXepInterface {

	@Override
	public void sapXepTang(double[] arr) {
		int i, j, min_idx;
		for (i = 0; i < arr.length - 1; i++) {
			min_idx = i;
			for (j = i + 1; j < arr.length; j++) {
				if (arr[j] < arr[min_idx]) {
					min_idx = j;
					swap(arr[min_idx], arr[i]);
				}
			}
		}
	}

	private void swap(double a, double b) {
		double temp = a;
		a = b;
		b = temp;
	}

	@Override
	public void sapXepGiam(double[] arr) {
		int i, j, min_idx;
		for (i = 0; i < arr.length - 1; i++) {
			min_idx = i;
			for (j = i + 1; j < arr.length; j++) {
				if (arr[j] < arr[min_idx]) {
					min_idx = j;
					swap(arr[min_idx], arr[i]);
				}
			}
		}
	}

}
