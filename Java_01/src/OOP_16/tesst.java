package OOP_16;
public class tesst {
	public static void main(String[] args) {
		System.out.println("Test câu a: ");
		MayTinhCasioFx500 mfx500 = new MayTinhCasioFx500();
		MayTinhVinacal500 mvn500 = new MayTinhVinacal500();
		System.out.println("10/5 = " +mfx500.chia(10, 5));
		System.out.println("10/0 = " +mvn500.chia(10, 0));
		System.out.println("3+2 = "+ mfx500.cong(3, 2));
		
		System.out.println("Test câu b: ");
		double[] arr1 = new double[] {5,1,3,4,7,9,2,6,0};
		double[] arr2 = new double[] {2,6,1,8,9,4,3,10};
		SapXepChen sxc = new SapXepChen();
		SapXepChon sxch =  new SapXepChon();
		sxc.sapXepGiam(arr2);
		System.out.print("Sắp xếp chèn giảm: arr2 = ");
		for (int i = 0; i < arr2.length; i++) {
			System.out.print(arr2[i]+ "  ");
		}
		System.out.println();
		sxc.sapXepTang(arr2);
		System.out.print("Sắp xếp chèn tăng: arr2 = ");
		for (int i = 0; i < arr2.length; i++) {
			System.out.print(arr2[i]+ "  ");
		}
		System.out.println();
		sxc.sapXepTang(arr1);
		System.out.print("Sắp xếp chèn tăng: arr1 = ");
		for (int i = 0; i < arr1.length; i++) {
			System.out.print(arr1[i]+ "  ");
		}
		System.out.println();
		sxc.sapXepGiam(arr1);
		System.out.print("Sắp xếp chèn giảm: arr2 = ");
		for (int i = 0; i < arr1.length; i++) {
			System.out.print(arr1[i]+ "  ");
		}
		sxch.sapXepGiam(arr2);
		System.out.print("\nSắp xếp chọn giảm: arr2 = ");
		for (int i = 0; i < arr2.length; i++) {
			System.out.print(arr2[i]+ "  ");
		}
		sxch.sapXepTang(arr2);
		System.out.print("\nSắp xếp chọn tăng: arr2 = ");
		for (int i = 0; i < arr2.length; i++) {
			System.out.print(arr2[i]+ "  ");
		}
		sxch.sapXepTang(arr1);
		System.out.print("\nSắp xếp chọn tăng: arr1 = ");
		for (int i = 0; i < arr1.length; i++) {
			System.out.print(arr1[i]+ "  ");
		}
		System.out.println();
		sxch.sapXepGiam(arr1);
		System.out.print("Sắp xếp chọn giảm: arr1 = ");
		for (int i = 0; i < arr1.length; i++) {
			System.out.print(arr1[i]+ "  ");
		}
		
		System.out.println("Test câu c: ");
		PhanMemMayTinh pmmt = new PhanMemMayTinh();
		System.out.println("5+3 = "+pmmt.cong(5, 3));
		
		double[] arr3 = new double[] {2,65,18,29,24,3,10};
		pmmt.sapXepTang(arr3);
		for (int i = 0; i < arr3.length; i++) {
			System.out.print(arr3[i]+"  ");
		}
	}
}
