package OOP_18;

import java.util.Scanner;

public class CongNhan extends CanBo {
	public static final int BAC_MAX = 7;
	int bac; // bậc: 1->7
	public CongNhan() {
	}
	public CongNhan(String fullname, String birthday, String gender, String address, int bac) {
		super(fullname, birthday, gender, address);
		this.bac = bac;
	}
	public int getBac() {
		return bac;
	}
	public void setBac(int bac) {
		this.bac = bac;
	}
	@Override
	public String toString() {
		return super.toString()+ "\n\t\tBậc: " + bac ;
	}
	@Override
	public void input() {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.err.println("Vui lòng nhập thông tin Công nhân !");
		super.input();
		System.out.println("Nhập bậc công nhân: ");
		bac = Integer.parseInt(sc.nextLine());
		
	}
	

}
