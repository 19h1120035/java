package OOP_18;

import java.util.Scanner;

public class CanBo {
	String fullname, birthday, gender, address;
	public CanBo() {
		
	}
	public CanBo(String fullname, String birthday, String gender, String address) {
		this.fullname = fullname;
		this.birthday = birthday;
		this.gender = gender;
		this.address = address;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "\n\t\tHọ và tên : " + fullname + "\n\t\tNgày sinh: " + birthday + "\n\t\tGiới tính: " + gender + "\n\t\tĐịa chỉ: " + address;
	}
	public void display() {
		System.out.println(this);
	}
	public void input () {
		Scanner sc= new Scanner(System.in);
		System.out.println("Nhập họ và tên:");
		fullname = sc.nextLine();
		System.out.println("Nhập ngày sinh: ");
		birthday = sc.nextLine();
		System.out.println("Nhập giới tính: ");
		gender = sc.nextLine();
		System.out.println("Nhập địa chỉ: ");
		address = sc.nextLine();
	}
	
}
