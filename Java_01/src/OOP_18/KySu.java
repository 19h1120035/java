package OOP_18;

import java.util.Scanner;

public class KySu extends CanBo {
	String nganh;

	public KySu() {
	}

	public KySu(String fullname, String birthday, String gender, String address, String nganh) {
		super(fullname, birthday, gender, address);
		this.nganh = nganh;
	}

	public String getNganh() {
		return nganh;
	}

	public void setNganh(String nganh) {
		this.nganh = nganh;
	}

	@Override
	public String toString() {
		return super.toString() + "\n\t\tNgành: "+nganh;
		
	}
	@Override
	public void input() {
		Scanner sc = new Scanner(System.in);
		System.err.println("Vui lòng nhập thông tin Kỹ Sư !");
		super.input();
		System.out.println("Nhập ngành: ");
		nganh = sc.nextLine();
	}
	
 	

}
