package OOP_18;

import java.util.Scanner;

public class NvPhucVu extends CanBo {
	String congViec;

	public NvPhucVu() {
	}

	public NvPhucVu(String fullname, String birthday, String gender, String address, String congViec) {
		super(fullname, birthday, gender, address);
		this.congViec = congViec;
	}

	public String getCongViec() {
		return congViec;
	}

	public void setCongViec(String congViec) {
		this.congViec = congViec;
	}

	@Override
	public String toString() {
		return super.toString()+ "\n\t\tCông việc: "+congViec;
	}
	@Override
	public void input() {
		Scanner sc = new Scanner(System.in);
		System.err.println("Vui lòng nhập thông tin nhân viên phục vụ !");
		super.input();
		System.out.println("Nhập công việc: ");
		congViec = sc.nextLine();
	}
	
	

}
