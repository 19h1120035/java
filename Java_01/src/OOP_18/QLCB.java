package OOP_18;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


public class QLCB {
	static List<CanBo> canBoList = new ArrayList<>();
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int chon;
		do {
			showMenu();
			chon = Integer.parseInt(sc.nextLine());
			switch (chon) {
			case 1: {
				inputData();
				break;
			}
			case 2: {
				searchByName();
				break;
			}
			case 3: {
				System.out.println("\tThông tin Cán Bộ:");
				showData();
				break;
			}
			case 4: {
				save();
				break;
			}
			case 5: {
				read();
				break;
			}
			case 6: {
				System.out.println("Thoát !!!");
				System.exit(0);
			}

			default:
				System.err.println("Nhập sai ! Vui lòng nhập lại.");

			}
		} while (chon != 4);
	}

	private static void read() {
		canBoList = SerializeFileFactory.docFile("D:\\datacanbo.txt");
		System.out.println("Đã đọc file thành công !! ");
	}

	private static void save() {
		boolean kt = SerializeFileFactory.luuFile(canBoList , "D:\\datacanbo.txt");
		if (kt = true) {
			System.out.println("Lưu file thành công !");
		} else {
			System.out.println("Lưu file thất bại !!");
		}
	}

	static void inputData() {
		System.out.println("Nhập số cán bộ cần thêm :");
		int n = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < n; i++) {
			CanBo canBo = createCanBo();
			canBoList.add(canBo);
		}
	}

	static void searchByName() {
		System.out.println("1. Tìm kiếm theo họ và tên.");
		System.out.println("2. Tìm kiếm theo ngày sinh.");
		System.out.println("3. Tìm kiếm theo giới tính.");
		System.out.println("4. Tìm kiếm theo địa chỉ.");
		System.out.println("Chọn :");
		int chon = Integer.parseInt(sc.nextLine());
		switch (chon) {
		case 1: {
			System.out.println("Nhập tên cán bộ cần tìm kiếm: ");
			String fullname = sc.nextLine();
			for (CanBo canBo : canBoList) {
				if (canBo.getFullname().equalsIgnoreCase(fullname)) {
					canBo.display();
				}
			}
			break;
		}
		case 2: {
			System.out.println("Nhập ngày sinh cán bộ cần tìm kiếm: ");
			String birthday = sc.nextLine();
			for (CanBo canBo : canBoList) {
				if (canBo.getBirthday().equalsIgnoreCase(birthday)) {
					canBo.display();
				}
			}
			break;
		}
		case 3: {
			System.out.println("Nhập giới tính cán bộ cần tìm kiếm :");
			String gender = sc.nextLine();
			for (CanBo canBo : canBoList) {
				if (canBo.getGender().equalsIgnoreCase(gender)) {
					canBo.display();
				}
			}
			break;
		}
		case 4: {
			System.out.println("Nhập địa chỉ cán bộ cần tìm kiếm: ");
			String address = sc.nextLine();
			for (CanBo canBo : canBoList) {
				if (canBo.getAddress().equalsIgnoreCase(address)) {
					canBo.display();
				}
			}
			break;
		}
		default:
			System.err.println("Nhập sai ! Vui lòng nhập lại.");
		}

	}

	static void showData() {
		canBoList.forEach(canBo -> {
			canBo.display();
		});
	}

	static CanBo createCanBo() {
		CanBo canBo = null;
		System.out.println("1. Nhập thông tin Công Nhân. ");
		System.out.println("2. Nhập thông tin Kỹ Sư.");
		System.out.println("3. Nhập thông tin Nhân Viên Phục Vụ.");
		System.out.println("Chọn: ");
		int chon = Integer.parseInt(sc.nextLine());
		switch (chon) {
		case 1: {
			canBo = new CongNhan();
			break;
		}
		case 2: {
			canBo = new KySu();
			break;
		}
		case 3: {
			canBo = new NvPhucVu();
			break;
		}
		default:
			System.err.println("Nhập sai ! Vui lòng nhập lại.");
		}
		canBo.input();
		return canBo;
	}

	static void showMenu() {
		System.out.println("1. Nhập thông tin mới cho cán bộ. ");
		System.out.println("2. Tìm kiếm cán bộ. ");
		System.out.println("3. Hiển thị thông tin cán bộ. ");
		System.out.println("4. Lưu thông tin cán bộ.");
		System.out.println("5. Đọc thông tin cán bộ.");
		System.out.println("6. Thoát. ");
		System.out.println("Vui lòng chọn [1..6]: ");
	}
}
