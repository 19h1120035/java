package OOP_18;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SerializeFileFactory {
	public static boolean luuFile(List<CanBo> canBoList, String path) {
		try {
			FileOutputStream fos = new FileOutputStream(path);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(canBoList);
			oos.close();
			fos.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	public static ArrayList<CanBo>docFile(String path) {
		ArrayList<CanBo>canBoList = new ArrayList<CanBo>();
		try {
			FileInputStream fis = new FileInputStream(path);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object data = ois.readObject();
			canBoList = (ArrayList<CanBo>) data;
			ois.close();
			fis.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return canBoList;
	}
}
