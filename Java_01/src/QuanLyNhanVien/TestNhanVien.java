package QuanLyNhanVien;

import java.util.ArrayList;
import java.util.Collections;

public class TestNhanVien {

	public static void main(String[] args) {
		ArrayList<NhanVien>ds = new ArrayList<NhanVien>();
		ds.add(new NhanVien(1,"Thương"));
		ds.add(new NhanVien(4, "Hiếu"));
		ds.add(new NhanVien(3, "Nhiều"));
		ds.add(new NhanVien(2, "Lắm"));
		ds.add(new NhanVien(5, "Hiếu"));
		System.out.println("Danh sách nhân viên của công ty: ");
		for (NhanVien nhanVien : ds) {
			System.out.println(nhanVien.getMa() +" "+nhanVien.getTen());
		}
		Collections.sort(ds);
		System.out.println("Danh sách nhân viên đã sắp xếp: ");
		for (NhanVien nhanVien : ds) {
			System.out.println(nhanVien.getMa()+" "+nhanVien.getTen());
		}
	}

}
