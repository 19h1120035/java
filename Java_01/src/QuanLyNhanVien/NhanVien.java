package QuanLyNhanVien;

public class NhanVien implements Comparable<NhanVien> {
	private int ma;
	private String ten;
	
	public NhanVien(int ma, String ten) {
		this.ma = ma;
		this.ten = ten;
	}
	public NhanVien() {
	}
	public int getMa() {
		return ma;
	}
	public void setMa(int ma) {
		this.ma = ma;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}

//	= 0 bằng nhau
//	> 0 đối tượng 1 lớn hơn đối tượng 2
//	< 0 đối tượng 1 nhỏ hơn đối tượng 2 
	@Override
	public int compareTo(NhanVien o) {
//		return this.ten.compareToIgnoreCase(o.getTen());
		int soSanh = this.ten.compareToIgnoreCase(o.getTen());
		if(soSanh == 0) {
		if (this.ma == o.ma) {
			return 0;
		} else if (this.ma > o.ma){
			return 1;
		}
		return -1;
		} 
		return soSanh;
	}

}
