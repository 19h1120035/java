package com.mysql.jdbc;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Connection {
	public static void main(String[] args) {
		String jdbcURL = "jdbc:postgresql://localhost:2004/Data";
		String username = "root";
		String password = "password";

		try {
			Connection connection = (Connection) DriverManager.getConnection(jdbcURL, username, password);
			System.out.println("Kết nối thành công !!!");
		
		} catch (SQLException e) {
			System.err.println("Kết nối lỗi");
			e.printStackTrace();
		}
	}
}
