package UI;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;

import QuanLyKhachHangUI.KhachHang;
import QuanLyKhachHangUI.NhomKhachHang;

public class KhachHangUI extends JFrame {
	DefaultMutableTreeNode root = null;
	JTree treeNhom;

	DefaultTableModel dtm;
	JTable tblKhach;

	JTextField txtMa, txtTen, txtPhone, txtEmail;
	JButton btnLuu, btnXoa;

	ArrayList<NhomKhachHang> dsNhom;
	ArrayList<KhachHang> dsKhach;

	NhomKhachHang nhomSelected = null;
	public KhachHangUI(String title) {
		super(title);
		addControls();
		addEvents();
		fakeData();
	}

	private void addEvents() {
		treeNhom.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				DefaultMutableTreeNode nodeSelected = (DefaultMutableTreeNode) treeNhom.getLastSelectedPathComponent();
				if (nodeSelected != null &&  nodeSelected.getLevel()==1) {
					nhomSelected = (NhomKhachHang) nodeSelected.getUserObject();
					hienThiDanhSachKhachHangTheoNhom();
					
				}
			}
		});
		tblKhach.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				int rowSelected = tblKhach.getSelectedRow();
				if (rowSelected == -1 ) return ;
				String ma = tblKhach.getValueAt(rowSelected, 0)+ "";
				String ten = tblKhach.getValueAt(rowSelected, 1)+ "";
				String phone = tblKhach.getValueAt(rowSelected,2)+ "";
				String email = tblKhach.getValueAt(rowSelected, 3)+ "";
				txtMa.setText(ma);
				txtTen.setText(ten);
				txtPhone.setText(phone);
				txtEmail.setText(email);
			}
		});
		
		btnLuu.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				KhachHang kh = new KhachHang(
						txtMa.getText(),
						txtTen.getText(),
						txtPhone.getText(),
						txtEmail.getText());
				nhomSelected.themKhachHang(kh);
			}
		});
	}

	protected void hienThiDanhSachKhachHangTheoNhom() {
		dtm.setRowCount(0);
		for (KhachHang kh : nhomSelected.getKhachHangs()) {
			Vector<String>vec = new Vector<String>();
			vec.add(kh.getMa());
			vec.add(kh.getTen());
			vec.add(kh.getPhone());
			vec.add(kh.getEmail());	
			dtm.addRow(vec);
			
		}
	}

	private void addControls() {
		Container con = getContentPane();
		JPanel pnLeft = new JPanel();
		pnLeft.setPreferredSize(new Dimension(300, 0));
		JPanel pnRight = new JPanel();
		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnLeft, pnRight);
		con.setLayout(new BorderLayout());
		con.add(sp, BorderLayout.CENTER);

		pnRight.setLayout(new BorderLayout());
		JPanel pnTopOfRight = new JPanel();
		pnTopOfRight.setPreferredSize(new Dimension(0, 350));
		JPanel pnBottomRight = new JPanel();
		JSplitPane spRight = new JSplitPane(JSplitPane.VERTICAL_SPLIT, pnTopOfRight, pnBottomRight);
		pnRight.add(spRight, BorderLayout.CENTER);

		pnLeft.setLayout(new BorderLayout());
		root = new DefaultMutableTreeNode("Công ty thực phẩm CucuFood");
		treeNhom = new JTree(root);
		JScrollPane scTree = new JScrollPane(treeNhom, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnLeft.add(scTree, BorderLayout.CENTER);
		// ====== table ======
		pnTopOfRight.setLayout(new BorderLayout());
		dtm = new DefaultTableModel();
		dtm.addColumn("Mã KH");
		dtm.addColumn("Tên KH");
		dtm.addColumn("Số điện thoại");
		dtm.addColumn("Email");
		tblKhach = new JTable(dtm);
		JScrollPane scTable = new JScrollPane(tblKhach, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnTopOfRight.add(scTable, BorderLayout.CENTER);
		// ====================================

		// ==============Bottom=============
		pnBottomRight.setLayout(new BoxLayout(pnBottomRight, BoxLayout.Y_AXIS));
		JPanel pnMa = new JPanel();
		pnMa.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblMa = new JLabel("Mã khách hàng: ");
		txtMa = new JTextField(20);
		pnMa.add(lblMa);
		pnMa.add(txtMa);
		pnBottomRight.add(pnMa);

		JPanel pnTen = new JPanel();
		pnTen.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblTen = new JLabel("Tên khách hàng: ");
		txtTen = new JTextField(20);
		pnTen.add(lblTen);
		pnTen.add(txtTen);
		pnBottomRight.add(pnTen);

		JPanel pnPhone = new JPanel();
		pnPhone.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPhone = new JLabel("Số điện thoại: ");
		txtPhone = new JTextField(20);
		pnPhone.add(lblPhone);
		pnPhone.add(txtPhone);
		pnBottomRight.add(pnPhone);

		JPanel pnEmail = new JPanel();
		pnEmail.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblEmail = new JLabel("Email khách hàng: ");
		txtEmail = new JTextField(20);
		pnEmail.add(lblEmail);
		pnEmail.add(txtEmail);
		pnBottomRight.add(pnEmail);

		JPanel pnButton = new JPanel();
		pnButton.setLayout(new FlowLayout(FlowLayout.LEFT));
		btnLuu = new JButton("Lưu khách hàng");
		btnXoa = new JButton("Xóa khách hàng");
		pnButton.add(btnLuu);
		pnButton.add(btnXoa);
		pnBottomRight.add(pnButton);

		// canh chỉnh đều lable
		lblMa.setPreferredSize(lblEmail.getPreferredSize());
		lblTen.setPreferredSize(lblEmail.getPreferredSize());
		lblPhone.setPreferredSize(lblEmail.getPreferredSize());

	}

	public void fakeData() {
		dsNhom = new ArrayList<>();
		NhomKhachHang vip = new NhomKhachHang("n1", "Khách Hàng Vip");
		NhomKhachHang timNang = new NhomKhachHang("n2", "Khách Hàng Tiềm Năng");
		NhomKhachHang khoTinh = new NhomKhachHang("n3", "Khách Hàng Khó Tính Hay Nhăn Nheo");
		dsNhom.add(vip);
		dsNhom.add(timNang);
		dsNhom.add(khoTinh);
		vip.getKhachHangs().add(new KhachHang("K01", "Đào Văn Thương", "0333729170", "chuthuong1080@gmail.com"));
		vip.getKhachHangs().add(new KhachHang("K02", "Nguyễn Minh Phú", "0967720047", "minhphu1222@gmail.com"));
		vip.getKhachHangs().add(new KhachHang("K03", "Nguyễn Minh Phương", "0967723466", "minhphuong22@gmail.com"));
		vip.getKhachHangs().add(new KhachHang("K04", "Nguyễn Minh Nhật", "0967734634", "minhnhat142@gmail.com"));
		timNang.getKhachHangs().add(new KhachHang("K01", "Nguyễn Việt Long", "0982696803", "longlu1201@gmail.com"));
		khoTinh.getKhachHangs().add(new KhachHang("K01", "Nguyễn Tấn Tài", "0392772301", "tantai2511@gmail.com"));

		for (NhomKhachHang nhom : dsNhom) {
			DefaultMutableTreeNode nodeNhom = new DefaultMutableTreeNode(nhom);
			root.add(nodeNhom);
			for (KhachHang khach : nhom.getKhachHangs()) {
				DefaultMutableTreeNode nodeKhach = new DefaultMutableTreeNode(khach);
				nodeNhom.add(khach);
			}
		}
		treeNhom.expandRow(0);
	}

	public void showWindow() {
		this.setSize(800, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
