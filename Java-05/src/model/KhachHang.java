package model;

import java.io.Serializable;

public class KhachHang implements Serializable {
	private String maKh;
	private String tenKH;
	public KhachHang() {
	}
	public KhachHang(String maKh, String tenKH) {
		this.maKh = maKh;
		this.tenKH = tenKH;
	}
	public String getMaKh() {
		return maKh;
	}
	public void setMaKh(String maKh) {
		this.maKh = maKh;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	@Override
	public String toString() {
		return maKh + "\t\t" + tenKH;
	}
	
	
}
