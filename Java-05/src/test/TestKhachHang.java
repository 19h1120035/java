package test;

import java.util.ArrayList;

import io.TextFileFactory;
import model.KhachHang;

public class TestKhachHang {
	public static void testLuuFile() {
		ArrayList<KhachHang> dsKH = new ArrayList<KhachHang>();
		dsKH.add(new KhachHang("KH01", "Đào Văn Thương"));
		dsKH.add(new KhachHang("KH02", "Đào Văn Lời"));
		dsKH.add(new KhachHang("KH03", "Đào Văn Phúc"));
		dsKH.add(new KhachHang("KH04", "Đào Văn Nam"));
		boolean result = TextFileFactory.luuFile(dsKH, "D:\\data.txt");
		if (result == true ) {
			System.out.println("Lưu file thành công !");
		} else {
			System.out.println("Lưu file thất bại !!!");
		}
		
		
	}
	public static void main(String[] args) {
//		testLuuFile();
		ArrayList<KhachHang>dsKH = TextFileFactory.docFile("D:\\data.txt");		
		System.out.println("Danh sách khách hàng nạp vào máy tính là: ");
		System.out.println("Mã KH\t\tTên KH");
		for(KhachHang kh: dsKH) {
			System.out.println(kh);
		}
//		
	}
}
