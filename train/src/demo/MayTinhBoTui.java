package demo;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MayTinhBoTui extends JFrame {

	private JPanel jp;
	private JTextField textField1;
	private JTextField textField2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MayTinhBoTui frame = new MayTinhBoTui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MayTinhBoTui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 200, 500, 350);
		jp = new JPanel();
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(jp);
		jp.setLayout(null);

		JLabel lb1 = new JLabel("M\u00E1y T\u00EDnh B\u1ECF T\u00FAi C\u01A1 B\u1EA3n");
		lb1.setForeground(Color.GREEN);
		lb1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lb1.setHorizontalAlignment(SwingConstants.CENTER);
		lb1.setBounds(0, 11, 484, 33);
		jp.add(lb1);

		JLabel lb2 = new JLabel("S\u1ED1 th\u1EE9 nh\u1EA5t:");
		lb2.setHorizontalAlignment(SwingConstants.CENTER);
		lb2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lb2.setBounds(63, 71, 139, 25);
		jp.add(lb2);

		textField1 = new JTextField();
		textField1.setBounds(278, 71, 110, 35);
		jp.add(textField1);
		textField1.setColumns(10);

		JLabel lb3 = new JLabel("S\u1ED1 th\u1EE9 hai:");
		lb3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lb3.setHorizontalAlignment(SwingConstants.CENTER);
		lb3.setBounds(76, 130, 110, 25);
		jp.add(lb3);

		textField2 = new JTextField();
		textField2.setBounds(278, 132, 110, 35);
		jp.add(textField2);
		textField2.setColumns(10);

		JButton btn1 = new JButton("C\u1ED9ng");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String n1 = textField1.getText();
				String n2 = textField2.getText();
				int s1 = Integer.parseInt(n1);
				int s2 = Integer.parseInt(n2);
				int kq = s1+s2;
				JOptionPane.showMessageDialog(btn1, "Tổng của " + s1 + " và " + s2 + " = " + kq);
			}
		});
		btn1.setFont(new Font("Tahoma", Font.BOLD, 18));
		btn1.setBounds(27, 192, 89, 33);
		jp.add(btn1);

		JButton btn2 = new JButton("Tr\u1EEB");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String n1 = textField1.getText();
				String n2 = textField2.getText();
				int s1 = Integer.parseInt(n1);
				int s2 = Integer.parseInt(n2);
				int kq = s1-s2;
				JOptionPane.showMessageDialog(btn2, "Hiệu của " + s1 + " và " + s2 + " = " + kq);
			}
		});
		btn2.setFont(new Font("Tahoma", Font.BOLD, 18));
		btn2.setBounds(132, 192, 89, 33);
		jp.add(btn2);

		JButton btn3 = new JButton("Nh\u00E2n");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String n1 = textField1.getText();
				String n2 = textField2.getText();
				int s1 = Integer.parseInt(n1);
				int s2 = Integer.parseInt(n2);
				int kq = s1*s2;
				JOptionPane.showMessageDialog(btn3, "Tích của " + s1 + " và " + s2 + " = " + kq);
			}
		});
		btn3.setFont(new Font("Tahoma", Font.BOLD, 18));
		btn3.setBounds(240, 192, 89, 33);
		jp.add(btn3);

		JButton btn4 = new JButton("Chia");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String n1 = textField1.getText();
				String n2 = textField2.getText();
				int s1 = Integer.parseInt(n1);
				int s2 = Integer.parseInt(n2);
				int kq = s1/s2;
				JOptionPane.showMessageDialog(btn4, "Hiệu của " + s1 + " và " + s2 + " = " + kq);
			}
		});
		btn4.setFont(new Font("Tahoma", Font.BOLD, 18));
		btn4.setBounds(339, 192, 89, 33);
		jp.add(btn4);
		
		JButton btn5 = new JButton("Thoát");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btn5.setForeground(Color.RED);
		btn5.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btn5.setBounds(196, 263, 89, 33);
		jp.add(btn5);
	}

}
