package checkpw;

import java.awt.*;
import java.awt.event.*;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class Vidu1 extends JFrame {
	public static final int PASSWORD_LENGTH = 10;
	public static final int MSSV_LENGTH = 10;
	private JPanel jp;
	private JTextField textField1;
	private JTextField textField2;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vidu1 frame = new Vidu1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Vidu1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 200, 500, 300);
		jp = new JPanel();
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(jp);
		jp.setLayout(null);

		JLabel lb1 = new JLabel("ĐĂNG NHẬP TÀI KHOẢN SINH VIÊN");
		lb1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lb1.setForeground(Color.BLUE);
		lb1.setHorizontalAlignment(SwingConstants.CENTER);
		lb1.setBounds(10, 0, 474, 39);
		jp.add(lb1);

		JLabel lb2 = new JLabel("MSSV:");
		lb2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lb2.setForeground(Color.BLACK);
		lb2.setBounds(40, 61, 62, 28);
		jp.add(lb2);

		JLabel lb3 = new JLabel("Mật Khẩu:");
		lb3.setForeground(Color.BLACK);
		lb3.setFont(new Font("Tahoma", Font.BOLD, 16));
		lb3.setBounds(38, 119, 137, 28);
		jp.add(lb3);

		textField1 = new JTextField();
		textField1.setBounds(176, 61, 225, 34);
		jp.add(textField1);
		textField1.setColumns(10);

		textField2 = new JTextField();
		textField2.setBounds(176, 118, 225, 34);
		jp.add(textField2);
		textField2.setColumns(10);

		JButton btn = new JButton("Đăng Nhập");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					String regex1 = "^\\d\\d[h,H]\\d+$";
					String regex2 = "^[a-z0-9]*\\d\\d[a-z0-9]*$";
					String mssv = textField1.getText();
					String pw = textField2.getText();
					Pattern p1 = Pattern.compile(regex1);
					Pattern p2 = Pattern.compile(regex2);
					if (p1.matcher(mssv).find() && (mssv.length() == MSSV_LENGTH) 
							&& (p2.matcher(pw).find() && pw.length() >= PASSWORD_LENGTH)) {
						JOptionPane.showMessageDialog(btn, "Đăng Nhập Thành Công !");
					} else {
						JOptionPane.showMessageDialog(btn, "MSSV hoặc mật khẩu không đúng !");
					}
				}
			
		});
		btn.setFont(new Font("Tahoma", Font.BOLD, 18));
		btn.setForeground(Color.GREEN);
		btn.setBounds(176, 203, 137, 39);
		jp.add(btn);
	}

	
}
