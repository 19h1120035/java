package giaipt;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Window;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Scanner;
import java.awt.event.ActionEvent;

public class GiaiPT extends JFrame {

	private JPanel jp;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GiaiPT frame = new GiaiPT();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public GiaiPT() {
		giaiPt();
	}

	public void giaiPt() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		jp = new JPanel();
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(jp);
		jp.setLayout(null);

		JLabel lb1 = new JLabel("GIẢI PHƯƠNG TRÌNH");
		lb1.setHorizontalAlignment(SwingConstants.CENTER);
		lb1.setForeground(Color.BLUE);
		lb1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lb1.setBounds(10, 11, 414, 27);
		jp.add(lb1);

		JButton btn1 = new JButton("Giải Phương Trình Bậc Nhất");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				giaiPt1();
			}
		});
		btn1.setFont(new Font("Tahoma", Font.BOLD, 16));
		btn1.setBounds(60, 79, 328, 36);
		jp.add(btn1);

		JButton btn2 = new JButton("Giải Phương Trình Bậc Hai");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				giaiPt2();
			}
		});
		btn2.setFont(new Font("Tahoma", Font.BOLD, 16));
		btn2.setBounds(60, 186, 328, 36);
		jp.add(btn2);
	}

	public void giaiPt1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 200, 500, 400);
		jp = new JPanel();
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(jp);
		jp.setLayout(null);

		JLabel lb1_pt1 = new JLabel("GIẢI PHƯƠNG TRÌNH BẬC NHẤT");
		lb1_pt1.setHorizontalAlignment(SwingConstants.CENTER);
		lb1_pt1.setForeground(Color.BLUE);
		lb1_pt1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lb1_pt1.setBounds(10, 11, 456, 27);
		jp.add(lb1_pt1);

		JLabel lb2_pt2 = new JLabel("Nhập vào số thứ nhất:");
		lb2_pt2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lb2_pt2.setForeground(Color.BLACK);
		lb2_pt2.setBounds(32, 63, 179, 27);
		jp.add(lb2_pt2);

		JLabel lb3_pt1 = new JLabel("Nhập vào số thứ hai:");
		lb3_pt1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lb3_pt1.setForeground(Color.BLACK);
		lb3_pt1.setBounds(32, 143, 179, 27);
		jp.add(lb3_pt1);

		JTextField textField1_pt1 = new JTextField();
		textField1_pt1.setBounds(250, 60, 156, 36);
		jp.add(textField1_pt1);
		textField1_pt1.setColumns(10);

		JTextField textField2_pt1 = new JTextField();
		textField2_pt1.setBounds(250, 141, 156, 36);
		jp.add(textField2_pt1);
		textField2_pt1.setColumns(10);

		JButton btn1_pt2 = new JButton("Giải Phương Trình");
		btn1_pt2.setForeground(Color.GREEN);
		btn1_pt2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s1 = textField1_pt1.getText();
				String s2 = textField2_pt1.getText();
				double a = Double.parseDouble(s1);
				double b = Double.parseDouble(s2);
				if (a == 0) {
					if (b == 0)
						JOptionPane.showMessageDialog(btn1_pt2, "Phương trình có vô số nghiệm!!!");
					else
						JOptionPane.showMessageDialog(btn1_pt2, "Phương trình vô nghiệm!!!");
				} else
					JOptionPane.showMessageDialog(btn1_pt2, "Phương trình có nghiệm là : x = " + (- b) / a);

			}
		});
		btn1_pt2.setFont(new Font("Tahoma", Font.BOLD, 16));
		btn1_pt2.setBounds(78, 213, 328, 36);
		jp.add(btn1_pt2);

		JButton btn2_pt1 = new JButton("Previous");
		btn2_pt1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				giaiPt();
			}
		});
		btn2_pt1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn2_pt1.setBounds(33, 307, 125, 33);
		jp.add(btn2_pt1);

		JButton btn3_pt1 = new JButton("EXIT");
		btn3_pt1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btn3_pt1.setForeground(Color.DARK_GRAY);
		btn3_pt1.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn3_pt1.setBounds(341, 309, 125, 33);
		jp.add(btn3_pt1);

	}

	public void giaiPt2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(700, 200, 500, 400);
		jp = new JPanel();
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(jp);
		jp.setLayout(null);

		JLabel lb1_pt2 = new JLabel("GIẢI PHƯƠNG TRÌNH BẬC HAI");
		lb1_pt2.setHorizontalAlignment(SwingConstants.CENTER);
		lb1_pt2.setForeground(Color.BLUE);
		lb1_pt2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lb1_pt2.setBounds(10, 11, 456, 27);
		jp.add(lb1_pt2);

		JLabel lb2_pt2 = new JLabel("Nhập vào số thứ nhất:");
		lb2_pt2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lb2_pt2.setForeground(Color.BLACK);
		lb2_pt2.setBounds(32, 63, 179, 27);
		jp.add(lb2_pt2);

		JLabel lb3_pt2 = new JLabel("Nhập vào số thứ hai:");
		lb3_pt2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lb3_pt2.setForeground(Color.BLACK);
		lb3_pt2.setBounds(32, 109, 179, 27);
		jp.add(lb3_pt2);

		JLabel lb4_pt2 = new JLabel("Nhập vào số thứ ba:");
		lb4_pt2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lb4_pt2.setForeground(Color.BLACK);
		lb4_pt2.setBounds(32, 160, 179, 27);
		jp.add(lb4_pt2);

		JTextField textField1_pt2 = new JTextField();
		textField1_pt2.setBounds(250, 60, 156, 36);
		jp.add(textField1_pt2);
		textField1_pt2.setColumns(10);

		JTextField textField2_pt2 = new JTextField();
		textField2_pt2.setBounds(250, 106, 156, 36);
		jp.add(textField2_pt2);
		textField2_pt2.setColumns(10);

		JTextField textField3_pt2 = new JTextField();
		textField3_pt2.setBounds(250, 153, 156, 34);
		jp.add(textField3_pt2);
		textField3_pt2.setColumns(10);

		JButton btn1_pt2 = new JButton("Giải Phương Trình");
		btn1_pt2.setForeground(Color.GREEN);
		btn1_pt2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String s1 = textField1_pt2.getText();
				String s2 = textField2_pt2.getText();
				String s3 = textField3_pt2.getText();
				double a = Double.parseDouble(s1);
				double b = Double.parseDouble(s2);
				double c = Double.parseDouble(s3);
				double delta = Math.pow(b, 2) - 4 * a * c;
				if (delta < 0) {
					JOptionPane.showMessageDialog(btn1_pt2, "Phương trình vô nghiệm !!!");
				} else if (delta == 0)
					JOptionPane.showMessageDialog(btn1_pt2,
							"Phương trình có nghiệm duy nhất là : x = " + (- b) / 2 * a);
				else
					JOptionPane.showMessageDialog(btn1_pt2, "Phương trình có hai nghiệm là : x1 = "
							+ (- (b) + Math.sqrt(delta)) / 2 * a + " và x2 = " + (- (b) - Math.sqrt(delta)) / 2 * a);
			}
		});
		btn1_pt2.setFont(new Font("Tahoma", Font.BOLD, 16));
		btn1_pt2.setBounds(78, 213, 328, 36);
		jp.add(btn1_pt2);

		JButton btn2_pt2 = new JButton("Previous");
		btn2_pt2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				giaiPt();
			}
		});
		btn2_pt2.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn2_pt2.setBounds(33, 307, 125, 33);
		jp.add(btn2_pt2);

		JButton btn3_pt2 = new JButton("EXIT");
		btn3_pt2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btn3_pt2.setForeground(Color.DARK_GRAY);
		btn3_pt2.setFont(new Font("Tahoma", Font.BOLD, 14));
		btn3_pt2.setBounds(341, 309, 125, 33);
		jp.add(btn3_pt2);
	}
}
