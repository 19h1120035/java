package bh3;

import java.util.Scanner;

// tính tổng các số nguyên từ a đến b
public class FuncDemo {
	public static void main(String[] args) {
		int ketQua = tinhTong(5, 10);
		var n =5;
//		ketQua = giaThua(n);
		System.out.println(ketQua);
	}
	private static int giaThua(int n) {
		var t = 1;
		while (n>0) {
			t= t*n--;
		}
		return t;
	}
	private static int tinhTong(int a, int b) {
		var ketQua = 0;
		if (a>b) {
			var t =a;
			a=b;
			b=t;
		}
		for (int i = 5;i <=10; i++) {
			ketQua++;
		}
		return ketQua;
	}
}
