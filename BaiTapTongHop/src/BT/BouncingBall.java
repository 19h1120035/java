package BT;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Ball {
	float X, Y; // Vá»‹ trÃ­
	float radius = 10; // kÃ­ch thÆ°á»›c
	float dx = 3, dy = 3; // hÆ°á»›ng Ä‘i

	public Ball() {
		X = 50 + (float) (Math.random() * 100);
		Y = 50 + (float) (Math.random() * 100);

		if (Math.random() > 0.5)
			dx = -dx;
		if (Math.random() > 0.5)
			dy = -dy;
	}

	public Ball(int x, int y, int dx, int dy) {
		this.X = x;
		this.Y = y;
		this.dx = dx;
		this.dy = dy;
	}

	public void drawBall(Graphics g) {
		if (g != null) {
			g.setColor(Color.blue);
			g.fillOval((int) (X - radius), (int) (Y - radius), (int) radius * 2, (int) radius * 2);

		}
	}

	public void updatePosition(float width, float height) {
		X = X + dx;
		Y = Y + dy;
		if (X - radius < 0) {
			dx = -dx;
			X = radius;
		} else if (X + radius > width) {
			dx = -dx;
			X = width - radius;
		}
		if (Y - radius < 0) {
			dy = -dy;
			Y = radius;
		} else if (Y + 60 > height) {
			dy = -dy;
			Y = height - 60;
		}
	}
}
class HinhChuNhat {
	int id;
	float X, Y;
	int height = 50;
	int width = 200;
	float dx, dy;
	Color color = Color.red;

	public HinhChuNhat() {
		X = (float) (Math.random() * 100);
		Y = (float) (Math.random() * 100);
		if (Math.random() > 0.5) {
			dx = -dx;
		}
		if (Math.random() > 0.5) {
			dy = -dy;
		}
	}

	public HinhChuNhat(int X, int Y, int dx, int dy) {
		this.X = X;
		this.Y = Y;
		this.dx = dx;
		this.dy = dy;
	}

	public void drawNhat(Graphics g) {
		if (g != null) {
			g.setColor(Color.GRAY);
			g.fillRect((int) (X - height), (int) (Y - width), (int) width, (int) height);
		}
	}

	public void updatePosition(float Width, float Height) {
		X = X + dx;
		Y = Y + dy;
		if (X - height < 0) {
			dx = -dx;
			X = height;
		} else if (X + height > Height) {
			dx = -dx;
			X = Height - 3*height/ 2;
		}
	}
}

class BallPanel extends JPanel implements Runnable {

	ArrayList<Ball> listOfBalls = new ArrayList<Ball>();
	ArrayList<HinhChuNhat> listOfNhats = new ArrayList<HinhChuNhat>();
	

	public void addBall(int X, int Y, int dx, int dy) {
		listOfBalls.add(new Ball(X, Y, dx, dy));
	}
	public void addNhats(int X, int Y, int dx, int dy) {
		listOfNhats.add(new HinhChuNhat(X, Y, dx, dy));
	}

	public void addBall() {
		listOfBalls.add(new Ball());
	}

	@Override
	public void run() {
		while (true) {
			for (Ball b : listOfBalls) {
				b.updatePosition(this.getWidth(), this.getHeight());
			}
			for(HinhChuNhat n: listOfNhats) {
				n.updatePosition(this.getWidth(), this.getHeight());
			}
			this.repaint();
			try {
				Thread.sleep(30);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (Ball b : listOfBalls) {
			b.drawBall(g);
		}
		for(HinhChuNhat n: listOfNhats) {
			n.drawNhat(g);
		}
	}

	public void start() {
		Thread th = new Thread(this);
		th.start();
	}

}

public class BouncingBall {
	public static void main(String[] args) {
		JFrame f = new JFrame("Bouncing Ball");
		f.setSize(600, 500);
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		BallPanel bpn = new BallPanel();
		f.setContentPane(bpn);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		bpn.start();
		Random ran = new Random();
		bpn.addBall();
		bpn.addBall(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10), ran.nextInt(10));
		bpn.addBall(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10), ran.nextInt(10));
		bpn.addBall(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10), ran.nextInt(10));
		bpn.addBall(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10), ran.nextInt(10));
		bpn.addBall(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10), ran.nextInt(10));
		bpn.addBall(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10), ran.nextInt(10));

//		bpn.addBall(100, 50, 3, 3);
//		bpn.addBall(200, 80, -3, -3);
//		bpn.addBall(400, 60, -3, -3);
//		bpn.addNhats(0, 610, 0, 0);

	}
}