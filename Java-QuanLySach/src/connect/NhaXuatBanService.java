package connect;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.NhaXuatBan;

public class NhaXuatBanService extends SQLService {
	public ArrayList<NhaXuatBan> layToanBoNhaXuatBan(){
		ArrayList<NhaXuatBan> dsNhaXuatBan = new ArrayList<NhaXuatBan>();
		try {
			String sql = "select * from tblnxb order by idnxb";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				NhaXuatBan nxb = new NhaXuatBan();
				nxb.setMaNXB(result.getString(1));
				nxb.setTenNXB(result.getString(2));
				nxb.setAddress(result.getString(3));
				nxb.setPhone(result.getString(4));
				dsNhaXuatBan.add(nxb);
			}
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dsNhaXuatBan;
	}
}
