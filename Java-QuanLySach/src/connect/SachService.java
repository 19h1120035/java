package connect;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.NhaXuatBan;
import model.Sach;

public class SachService extends SQLService {
	public ArrayList<Sach> timSachTheoNhaXuatBan(String maNxb){
		ArrayList<Sach> dsSach = new ArrayList<Sach>();
		try {
			String sql = "select * from tblsach where idnxb =?";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			preStatement.setString(1, maNxb);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				Sach sach = new Sach();
				sach.setMaSach(result.getString(1));
				sach.setTenSach(result.getString(2));
				sach.setMaNXB(result.getString(3));
				sach.setSoTrang(result.getInt(4));
				dsSach.add(sach);
			}
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dsSach;
	}
}
