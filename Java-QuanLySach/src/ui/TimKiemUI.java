package ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import connect.SachService;
import model.Sach;

public class TimKiemUI extends JFrame {
	JTextField txtTim;
	JButton btnTim;
	DefaultTableModel dtmSach;
	JTable tblSach;

	public TimKiemUI(String title) {
		super(title);
		addControls();
		addEvents();

	}

	private void addEvents() {
		btnTim.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyTimKiem();
				
			}
		});
	}

	protected void xuLyTimKiem() {
		SachService sachsv = new SachService();
		ArrayList<Sach>dsSach = sachsv.timSachTheoNhaXuatBan(txtTim.getText());
		dtmSach.setRowCount(0);
		for (Sach sach : dsSach) {
			Vector<Object>vec = new Vector<Object>();
			vec.add(sach.getMaSach());
			vec.add(sach.getTenSach());
			vec.add(sach.getMaNXB());
			vec.add(sach.getSoTrang());
			dtmSach.addRow(vec);
		}
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BorderLayout());
		JPanel pnNorth = new JPanel();
		pnNorth.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblNhap = new JLabel("Nhập sách cần tìm: ");
		txtTim = new JTextField(25);
		btnTim = new JButton("Tìm kiếm");
		pnNorth.add(lblNhap);
		pnNorth.add(txtTim);
		pnNorth.add(btnTim);
		con.add(pnNorth, BorderLayout.NORTH);

		JPanel pnCenter = new JPanel();
		pnCenter.setLayout(new BorderLayout());
		dtmSach = new DefaultTableModel();
		dtmSach.addColumn("Mã Sách");
		dtmSach.addColumn("Tên Sách");
		dtmSach.addColumn("Nhà Xuất Bản");
		dtmSach.addColumn("Số Trang");
		tblSach = new JTable(dtmSach);
		JScrollPane scTable = new JScrollPane(tblSach, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnCenter.add(scTable,BorderLayout.CENTER);
		con.add(pnCenter,BorderLayout.CENTER);
		
		
		
	}
	

	public void showWindow() {
		this.setSize(550, 550);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

}
