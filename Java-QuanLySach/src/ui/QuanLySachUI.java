package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import connect.NhaXuatBanService;
import connect.SQLService;
import connect.SachService;
import model.NhaXuatBan;

public class QuanLySachUI extends JFrame {
	JTextField txtIDNXB, txtNameNXB, txtAddress, txtPhone;
	JButton btnVeTruoc, btnVeSau;
	JLabel lblStep;

	JButton btnThem, btnLuu, btnSua, btnXoa;

	DefaultTableModel dtmNXB;
	JTable tblNXB;

	JButton btnTimKiem;
	Connection conn = null;
	ArrayList<NhaXuatBan> dsNhaXuatBan = null;

	public QuanLySachUI(String title) {
		super(title);
		addControls();
		ketNoi();
		addEvents();
		hienThiToanBoNhaXuatBan();

	}

	private void hienThiToanBoNhaXuatBan() {
		NhaXuatBanService nxbService = new NhaXuatBanService();
		dsNhaXuatBan = nxbService.layToanBoNhaXuatBan();
		dtmNXB.setRowCount(0);
		for (NhaXuatBan nxb : dsNhaXuatBan) {
			Vector<Object> vec = new Vector<Object>();
			vec.add(nxb.getMaNXB());
			vec.add(nxb.getTenNXB());
			vec.add(nxb.getAddress());
			vec.add(nxb.getPhone());
			dtmNXB.addRow(vec);
		}
	}

	private void addEvents() {
		btnTimKiem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TimKiemUI ui = new TimKiemUI("Tìm Kiếm Sách");
				ui.showWindow();
			}
		});
		btnThem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyThemNXB();

			}
		});
		tblNXB.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int rowSelected = tblNXB.getSelectedRow();
				if (rowSelected == -1)
					return;
				String id = tblNXB.getValueAt(rowSelected, 0) + "";
				String name = tblNXB.getValueAt(rowSelected, 1) + "";
				String address= tblNXB.getValueAt(rowSelected, 2) + "";
				String phone = tblNXB.getValueAt(rowSelected, 3) + "";
				txtIDNXB.setText(id);
				txtNameNXB.setText(name);
				txtAddress.setText(address);
				txtPhone.setText(phone);
			}
		});
		btnSua.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				xulySua();
			}
		});
		btnXoa.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyXoa();
				
			}
		});
	}

	protected void xuLyXoa() {
		try {
			String sql = "DELETE FROM public.tblnxb WHERE idnxb = ?";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			preStatement.setString(1, txtIDNXB.getText());
			int x = preStatement.executeUpdate();
			if (x > 0) {
				JOptionPane.showMessageDialog(null, "Xóa Nhà Xuất Bản thành công !");
				hienThiToanBoNhaXuatBan();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void xulySua() {
		try {
			String sql = "UPDATE tblnxb SET namenxb=?, address=?, phone=? WHERE idnxb=?";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			preStatement.setString(1, txtNameNXB.getText());
			preStatement.setString(2, txtAddress.getText());
			preStatement.setString(3, txtPhone.getText());
			preStatement.setString(4, txtIDNXB.getText());
			int x = preStatement.executeUpdate();
			if (x > 0) {
				JOptionPane.showMessageDialog(null, "Sửa Nhà Xuất Bản thành công !");
				hienThiToanBoNhaXuatBan();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void xuLyThemNXB() {

		try {
			String sql = "insert into tblnxb values (?,?,?,?)";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			preStatement.setString(1, txtIDNXB.getText());
			preStatement.setString(2, txtNameNXB.getText());
			preStatement.setString(3, txtAddress.getText());
			preStatement.setString(4, txtPhone.getText());
			int x = preStatement.executeUpdate();
			if (x > 0) {
				JOptionPane.showMessageDialog(null, "Thêm Nhà Xuất Bản thành công !");
				hienThiToanBoNhaXuatBan();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BorderLayout());
		JPanel pnNorth = new JPanel();
		JPanel pnCenter = new JPanel();
		JPanel pnSouth = new JPanel();
		pnSouth.setLayout(new FlowLayout(FlowLayout.LEFT));

		con.add(pnNorth, BorderLayout.NORTH);
		con.add(pnCenter, BorderLayout.CENTER);
		con.add(pnSouth, BorderLayout.SOUTH);

		pnNorth.setLayout(new BorderLayout());
		JPanel pnDetail = new JPanel(); // chi tiết
		pnNorth.add(pnDetail, BorderLayout.CENTER);
		JPanel pnPerform = new JPanel(); // thực hiện
		pnNorth.add(pnPerform, BorderLayout.EAST);

		pnDetail.setLayout(new BoxLayout(pnDetail, BoxLayout.Y_AXIS));
		JPanel pnNXB = new JPanel();
		JLabel lblNXB = new JLabel("Thông tin nhà xuất bản");
		lblNXB.setForeground(Color.BLUE);
		Font font = new Font("arial", Font.BOLD, 22);
		lblNXB.setFont(font);
		pnNXB.add(lblNXB);
		pnDetail.add(pnNXB);

		JPanel pnMaNXB = new JPanel();
		JLabel lblMaNXB = new JLabel("Mã nhà xuất bản: ");
		txtIDNXB = new JTextField(25);
		pnMaNXB.add(lblMaNXB);
		pnMaNXB.add(txtIDNXB);
		pnDetail.add(pnMaNXB);

		JPanel pnNameNXB = new JPanel();
		JLabel lblNameNXB = new JLabel("Tên nhà xuất bản: ");
		txtNameNXB = new JTextField(25);
		pnNameNXB.add(lblNameNXB);
		pnNameNXB.add(txtNameNXB);
		pnDetail.add(pnNameNXB);

		JPanel pnAddress = new JPanel();
		JLabel lblAddress = new JLabel("Địa chỉ: ");
		txtAddress = new JTextField(25);
		pnAddress.add(lblAddress);
		pnAddress.add(txtAddress);
		pnDetail.add(pnAddress);

		JPanel pnPhone = new JPanel();
		JLabel lblPhone = new JLabel("Điện thoại: ");
		txtPhone = new JTextField(25);
		pnPhone.add(lblPhone);
		pnPhone.add(txtPhone);
		pnDetail.add(pnPhone);

		JPanel pnButtonDetail = new JPanel();
		btnVeTruoc = new JButton("Về trước");
		lblStep = new JLabel("1/10");
		btnVeSau = new JButton("Về sau");
		pnButtonDetail.add(btnVeTruoc);
		pnButtonDetail.add(lblStep);
		pnButtonDetail.add(btnVeSau);
		pnDetail.add(pnButtonDetail);

		// chỉnh độ rộng
		lblMaNXB.setPreferredSize(lblNameNXB.getPreferredSize());
		lblAddress.setPreferredSize(lblNameNXB.getPreferredSize());
		lblPhone.setPreferredSize(lblNameNXB.getPreferredSize());

		pnPerform.setLayout(new BoxLayout(pnPerform, BoxLayout.Y_AXIS));

		JPanel pnButtonThem = new JPanel();
		btnThem = new JButton("Thêm");
		pnButtonThem.add(btnThem);
		pnPerform.add(pnButtonThem);

		JPanel pnButtonLuu = new JPanel();
		btnLuu = new JButton("Lưu");
		pnButtonLuu.add(btnLuu);
		pnPerform.add(pnButtonLuu);

		JPanel pnButtonSua = new JPanel();
		btnSua = new JButton("Sửa");
		pnButtonSua.add(btnSua);
		pnPerform.add(pnButtonSua);

		JPanel pnButtonXoa = new JPanel();
		btnXoa = new JButton("Xóa");
		pnButtonXoa.add(btnXoa);
		pnPerform.add(pnButtonXoa);

		pnCenter.setLayout(new BorderLayout());
		dtmNXB = new DefaultTableModel();
		dtmNXB.addColumn("Mã nhà xuất bản");
		dtmNXB.addColumn("Tên nhà xuất bản");
		dtmNXB.addColumn("Địa chỉ");
		dtmNXB.addColumn("Điện thoại");
		tblNXB = new JTable(dtmNXB);
		JScrollPane scTable = new JScrollPane(tblNXB, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnCenter.add(scTable, BorderLayout.CENTER);

		JPanel pnButtonOfSouth = new JPanel();
		pnButtonOfSouth.setLayout(new FlowLayout(FlowLayout.LEFT));
		btnTimKiem = new JButton("Tìm kiếm");
		pnButtonOfSouth.add(btnTimKiem);
		pnSouth.add(pnButtonOfSouth);

		// kẽ khung
		TitledBorder borderThongTinChiTiet = new TitledBorder(BorderFactory.createLineBorder(Color.green),
				"Thông Tin Chi Tiết");
		pnDetail.setBorder(borderThongTinChiTiet);

		// kẽ khung
		TitledBorder borderThucHien = new TitledBorder(BorderFactory.createLineBorder(Color.blue), "Thực Hiện");
		pnPerform.setBorder(borderThucHien);

		btnThem.setIcon(new ImageIcon("img/add.png"));
		btnLuu.setIcon(new ImageIcon("img/save.png"));
		btnSua.setIcon(new ImageIcon("img/fix.png"));
		btnXoa.setIcon(new ImageIcon("img/delete.png"));
		btnVeTruoc.setIcon(new ImageIcon("img/prev.png"));
		btnVeSau.setIcon(new ImageIcon("img/next.png"));
		btnLuu.setPreferredSize(btnThem.getPreferredSize());
		btnSua.setPreferredSize(btnThem.getPreferredSize());
		btnXoa.setPreferredSize(btnThem.getPreferredSize());
		btnTimKiem.setIcon(new ImageIcon("img/search.png"));

		TitledBorder borderDanhSachNXB = new TitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Danh Sách Nhà Xuất Bản");
		pnCenter.setBorder(borderDanhSachNXB);
	}

	private void ketNoi() {
		try {
			String url = "jdbc:postgresql://localhost:2004/dbQuanLySach";
			Properties pro = new Properties();
			pro.put("user", "postgres");
			pro.put("password", "20042001");
			conn = DriverManager.getConnection(url, pro);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showWindow() {
		this.setSize(700, 700);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
