package ui;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ThemUI extends JFrame {
	JTextField txtIDNXB, txtNameNXB, txtAddressNXB, txtPhone;
	JButton btnLuu;
	Connection conn = null;

	public ThemUI(String title) {
		super(title);
		ketNoi();
		addControls();
		addEvents();
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BoxLayout(con, BoxLayout.Y_AXIS));

		JPanel pnID = new JPanel();
		pnID.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblID = new JLabel("Mã NXB : ");
		txtIDNXB = new JTextField(20);
		pnID.add(lblID);
		pnID.add(txtIDNXB);
		con.add(pnID);

		JPanel pnTen = new JPanel();
		pnTen.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblTen = new JLabel("Tên Nhà Xuất Bản: ");
		txtNameNXB = new JTextField(20);
		pnTen.add(lblTen);
		pnTen.add(txtNameNXB);
		con.add(pnTen);

		JPanel pnAddress = new JPanel();
		pnAddress.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblAddress = new JLabel("Địa chỉ: ");
		txtAddressNXB = new JTextField(20);
		pnAddress.add(lblAddress);
		pnAddress.add(txtAddressNXB);
		con.add(pnAddress);

		JPanel pnPhone = new JPanel();
		pnPhone.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPhone = new JLabel("Số điện thoại: ");
		txtPhone = new JTextField(20);
		pnPhone.add(lblPhone);
		pnPhone.add(txtPhone);
		con.add(pnPhone);

		JPanel pnButton = new JPanel();
		pnButton.setLayout(new FlowLayout(FlowLayout.CENTER));
		btnLuu = new JButton("Lưu");
		pnButton.add(btnLuu);
		con.add(pnButton);

//		lblID.setPreferredSize(lblPhone2.getPreferredSize());
//		lblTen.setPreferredSize(lblPhone2.getPreferredSize());
//		lblAddress.setPreferredSize(lblPhone2.getPreferredSize());
//		lblPhone.setPreferredSize(lblPhone2.getPreferredSize());
	}

	private void addEvents() {
		btnLuu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyLuu();

			}
		});
	}

	protected void xuLyLuu() {
		try {
			String sql = "insert into tblnxb values (?,?,?,?)";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			preStatement.setString(1,txtIDNXB.getText());
			preStatement.setString(2, txtNameNXB.getText());
			preStatement.setString(3, txtAddressNXB.getText());
			preStatement.setString(4, txtPhone.getText());
			int x = preStatement.executeUpdate();
			if (x > 0) {
				JOptionPane.showMessageDialog(null, "Thêm Nhà Xuất Bản thành công !");
			}
			dispose(); // ẩn màn hình này
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ketNoi() {
		try {
			String url = "jdbc:postgresql://localhost:2004/dbQuanLySach";
			Properties pro = new Properties();
			pro.put("user", "postgres");
			pro.put("password", "20042001");
			conn = DriverManager.getConnection(url, pro);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showWindow() {
		this.setSize(600, 500);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

}
