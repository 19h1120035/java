package DanhBaUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DanhBa extends JFrame {
	DefaultTableModel dtmDanhBa;
	JTable tblDanhBa;
	public static Connection conn = null;
	public static Statement statement = null;

	JButton btnThem, btnSua;
	JMenuItem mnuEdit, mnuDelete;
	JPopupMenu popupMenu;

	public DanhBa(String title) {
		super(title);
		addControls();
		addEvents();
		ketNoi();
		hienThiDanhBa();
	}

	private void hienThiDanhBa() {
		try {
			String sql = "SELECT * FROM qldanhba order by id";
			statement = conn.createStatement();
			ResultSet result = statement.executeQuery(sql);
			dtmDanhBa.setRowCount(0);
			while (result.next()) {
				Vector<Object> vec = new Vector<Object>();
				vec.add(result.getInt(5));
				vec.add(result.getString(1));
				vec.add(result.getString(2));
				vec.add(result.getString(3));
				vec.add(result.getString(4));
				dtmDanhBa.addRow(vec);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ketNoi() {
		try {
			String url = "jdbc:postgresql://localhost:2004/danhba";
			Properties pro = new Properties();
			pro.put("user", "postgres");
			pro.put("password", "20042001");
			conn = DriverManager.getConnection(url, pro);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addEvents() {
		btnThem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ChiTietDanhBa ui = new ChiTietDanhBa("Thông tin danh bạ chi tiết");
				ui.showWindow();
				if (ChiTietDanhBa.ketqua > 0)
					hienThiDanhBa();
			}
		});
		btnSua.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int row = tblDanhBa.getSelectedRow();
				if (row == -1)
					return;
				String ma = tblDanhBa.getValueAt(row, 0) + "";

				ChiTietDanhBa ui = new ChiTietDanhBa("Thông tin danh bạ chi tiết");
				ui.maChon = ma;
				ui.hienThiThongTinChiTiet();
				ui.showWindow();
				hienThiDanhBa();
			}
		});

		tblDanhBa.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) { // nhấn chuột phải
					int row = tblDanhBa.rowAtPoint(e.getPoint());
					int column = tblDanhBa.columnAtPoint(e.getPoint());
					if (!tblDanhBa.isRowSelected(row)) {
						tblDanhBa.changeSelection(row, column, false, false);
					}

					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
		mnuEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnSua.doClick();
			}
		});
		mnuDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyXoa();
			}
		});
	}

	protected void xuLyXoa() {
		int ret = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn xóa danh bạ này không ?",
				"Xác nhận Xóa danh bạ", JOptionPane.YES_NO_OPTION);
		int rowSelected = tblDanhBa.getSelectedRow();
		if (rowSelected == -1)
			return;
		String id = tblDanhBa.getValueAt(rowSelected, 0) + "";
		String ma = tblDanhBa.getValueAt(tblDanhBa.getSelectedRow(), 0) + "";
		if (ret == JOptionPane.YES_OPTION) {
			try {
				String sql = "delete from qldanhba where id = '" + ma + "'";
				statement = conn.createStatement();
				int x = statement.executeUpdate(sql);
				if (x > 0) {
					JOptionPane.showMessageDialog(null, "Xóa danh bạ thành công !");
					hienThiDanhBa();
				}

			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BorderLayout());
		JPanel pnNorth = new JPanel();
		JLabel lblTitle = new JLabel("Chương trình quản lý danh bạ");
		lblTitle.setFont(new Font("arial", Font.BOLD, 20));
		lblTitle.setForeground(Color.BLUE);
		pnNorth.add(lblTitle);
		con.add(pnNorth, BorderLayout.NORTH);
		dtmDanhBa = new DefaultTableModel();
		dtmDanhBa.addColumn("ID");
		dtmDanhBa.addColumn("Tên danh bạ");
		dtmDanhBa.addColumn("Địa chỉ");
		dtmDanhBa.addColumn("Số điện thoại");
		dtmDanhBa.addColumn("Số điện thoại 2");
		tblDanhBa = new JTable(dtmDanhBa);
		JScrollPane scTable = new JScrollPane(tblDanhBa, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		con.add(scTable, BorderLayout.CENTER);

		JPanel pnButton = new JPanel();
		pnButton.setLayout(new FlowLayout(FlowLayout.LEFT));
		btnThem = new JButton("Thêm Danh Bạ");
		btnSua = new JButton("Sửa Danh Bạ");
		pnButton.add(btnThem);
		pnButton.add(btnSua);
		con.add(pnButton, BorderLayout.SOUTH);

		popupMenu = new JPopupMenu();
		mnuEdit = new JMenuItem("Chỉnh sửa");
		mnuDelete = new JMenuItem("Xóa");
		popupMenu.add(mnuEdit);
		popupMenu.addSeparator();
		popupMenu.add(mnuDelete);
	}

	public void showWindow() {
		this.setSize(800, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
