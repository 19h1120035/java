package DanhBaUI;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ChiTietDanhBa extends JDialog {
	JTextField txtID, txtTen, txtAddres, txtPhone, txtPhone1,txtMa;
	JButton btnAdd;

	Connection conn = DanhBa.conn;
	Statement statement = DanhBa.statement;

	public static int ketqua = -1;
	public String maChon = "";
	public ChiTietDanhBa(String title) {
		this.setTitle(title);
		addControls();
		addEvents();
		if (maChon.length() != 0) {
			hienThiThongTinChiTiet();
		}
	}

	public void hienThiThongTinChiTiet() {
		try {
			String sql = "select * from qldanhba where id = '" + maChon+ "'";
			statement = conn.createStatement();
			ResultSet result = statement.executeQuery(sql);
			if (result.next()) {
				txtMa.setText(result.getInt(5)+"");
				txtTen.setText(result.getString(1));
				txtAddres.setText(result.getString(2));
				txtPhone.setText(result.getString(3));
				txtPhone1.setText(result.getString(4));
			}
			txtMa.setEditable(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addEvents() {
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyLuu();
			}
		});
	}

	protected void xuLyLuu() {
		String ma = txtMa.getText();
		String ten = txtTen.getText();
		String address = txtAddres.getText();
		String phone = txtPhone.getText();
		String phone1 = txtPhone1.getText();
		if (maChon.length() == 0) {
			try {
//			String sql = "insert into qldanhba values('" +ten+ "','"
//					+ address+ "','" + phone+ "','" + phone1+"',)";
				String sql = "insert into qldanhba values('" + ten + "','" + address + "','" + phone + "','" + phone1
						+ "','"+ma+"')";
				statement = conn.createStatement();
				int x = statement.executeUpdate(sql);
				ketqua = x;
				JOptionPane.showMessageDialog(btnAdd, "Thêm thông tin danh bạ thành công !");
				dispose(); // ẩn màn hình này

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				String sql = "update qldanhba set name = '"+ten+"',address = '"+address+"',phone = '"+phone+"',phone1 = '"+phone1+"' where id = '"+maChon+"'";
				statement=conn.createStatement();
				int x = statement.executeUpdate(sql);
				ketqua = x;
				JOptionPane.showMessageDialog(btnAdd, "Sửa thông tin danh bạ thành công !");
				dispose();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BoxLayout(con, BoxLayout.Y_AXIS));
		
		JPanel pnMa= new JPanel();
		pnMa.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblMa = new JLabel("ID: ");
		txtMa = new JTextField(20);
		pnMa.add(lblMa);
		pnMa.add(txtMa);
		con.add(pnMa);
		
		JPanel pnTen = new JPanel();
		pnTen.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblTen = new JLabel("Tên Danh Bạ: ");
		txtTen = new JTextField(20);
		pnTen.add(lblTen);
		pnTen.add(txtTen);
		con.add(pnTen);

		JPanel pnAddress = new JPanel();
		pnAddress.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblAddress = new JLabel("Địa chỉ: ");
		txtAddres = new JTextField(20);
		pnAddress.add(lblAddress);
		pnAddress.add(txtAddres);
		con.add(pnAddress);

		JPanel pnPhone = new JPanel();
		pnPhone.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPhone = new JLabel("Số điện thoại: ");
		txtPhone = new JTextField(20);
		pnPhone.add(lblPhone);
		pnPhone.add(txtPhone);
		con.add(pnPhone);

		JPanel pnPhone1 = new JPanel();
		pnPhone1.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPhone1 = new JLabel("Số điện thoại khác (nếu có): ");
		txtPhone1 = new JTextField(20);
		pnPhone1.add(lblPhone1);
		pnPhone1.add(txtPhone1);
		con.add(pnPhone1);

		lblMa.setPreferredSize(lblPhone1.getPreferredSize());
		lblTen.setPreferredSize(lblPhone1.getPreferredSize());
		lblAddress.setPreferredSize(lblPhone1.getPreferredSize());
		lblPhone.setPreferredSize(lblPhone1.getPreferredSize());
		lblPhone1.setPreferredSize(lblPhone1.getPreferredSize());

		JPanel pnButton = new JPanel();
		pnButton.setLayout(new FlowLayout(FlowLayout.LEFT));
		btnAdd = new JButton("Lưu Danh Bạ");
		pnButton.add(btnAdd);
		con.add(pnButton);
	}

	public void showWindow() {
		this.setSize(500, 400);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
