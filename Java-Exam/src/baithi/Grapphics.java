package baithi;

import java.awt.*;
import javax.swing.*;

import java.util.ArrayList;
import java.util.Random;

class Ball {
	String id;
	float X, Y;
	int radius = 100;
	float dx, dy;
	Color color = Color.blue;

	public Ball() {
		X = (float) (Math.random() * 100);
		Y = (float) (Math.random() * 100);
		if (Math.random() > 0.5) {
			dx = -dx;
		}
		if (Math.random() > 0.5) {
			dy = -dy;
		}
	}

	public Ball(int X, int Y, int dx, int dy) {
		this.X = X;
		this.Y = Y;
		this.dx = dx;
		this.dy = dy;
	}

	public void drawBall(Graphics g) {
		if (g != null) {
			g.setColor(color);
			g.fillOval((int) (X - radius), (int) (Y - radius), (int) radius * 2, (int) radius * 2);
			g.setColor(Color.red);
			g.drawOval((int) (X - radius), (int) (Y - radius), (int) radius * 2, (int) radius * 2);
			Font f = new Font("arial", Font.BOLD, 20);
			g.setFont(f);
			g.drawString("1", (int) (X - 5), (int) (Y + 5));
		}
	}

	public void updatePosition(float width, float height) {
		X = X + dx;
		Y = Y + dy;

		if (X - radius < 0) {
			dx = -dx;
			X = radius;
			radius = radius - (radius * 10 / 100);
		} else if (X + radius > width) {
			dx = -dx;
			X = width - radius;
			radius = radius - (radius * 10 / 100);
		}
		if (Y - radius < 0) {
			dy = -dy;
			Y = radius;
			radius = radius - (radius * 10 / 100);
		} else if (Y + radius > height) {
			dy = -dy;
			Y = height - radius;
			radius = radius - (radius * 10 / 100);
		}
	}
}

class HinhVuong {
	int id;
	float X, Y;
	int height = 100;
	int width = 100;
	float dx, dy;
	Color colorNen = Color.red;
	Color colorVien = Color.black;

	public HinhVuong() {
		X = (float) (Math.random() * 100);
		Y = (float) (Math.random() * 100);
		if (Math.random() > 0.5) {
			dx = -dx;
		}
		if (Math.random() > 0.5) {
			dy = -dy;
		}
	}

	public HinhVuong(int X, int Y, int dx, int dy) {
		this.X = X;
		this.Y = Y;
		this.dx = dx;
		this.dy = dy;
	}

	public void drawVuong(Graphics g) {
		if (g != null) {
			g.setColor(colorNen);
			g.fillRect((int) (X - width), (int) (Y - height), (int) width, (int) height);
			g.setColor(Color.WHITE);
			g.drawString("2", (int) (X - (width / 2) - 5), (int) (Y - (width / 2) + 5));
			Font f = new Font("arial", Font.BOLD, 20);
			g.setFont(f);
			g.setColor(colorVien);
			g.drawRect((int) (X - width), (int) (Y - height), (int) width, (int) height);
		}
	}

	public void updatePosition(float Width, float Height) {
		X = X + dx;
		Y = Y + dy;
		if (X - width < 0) {
			dx = -dx;
			X = width;
			if (colorNen == Color.red && colorVien == Color.black) {
				colorNen = Color.black;
				colorVien = Color.red;
			} else {
				colorNen = Color.red;
				colorVien = Color.black;
			}
		} else if (X > Width) {
			dx = -dx;
			X = Width;
			if (colorNen == Color.red && colorVien == Color.black) {
				colorNen = Color.black;
				colorVien = Color.red;
			} else {
				colorNen = Color.red;
				colorVien = Color.black;
			}
		}
		if (Y - width < 0) {
			dy = -dy;
			Y = height;
			if (colorNen == Color.red && colorVien == Color.black) {
				colorNen = Color.black;
				colorVien = Color.red;
			} else {
				colorNen = Color.red;
				colorVien = Color.black;
			}
		} else if (Y > Height) {
			dy = -dy;
			Y = Height;
			if (colorNen == Color.red && colorVien == Color.black) {
				colorNen = Color.black;
				colorVien = Color.red;
			} else {
				colorNen = Color.red;
				colorVien = Color.black;
			}
		}
	}
}

class HinhChuNhat {
	int id;
	float X, Y;
	int height = 100;
	int width = 50;
	float dx, dy;
	Color color = Color.green;

	public HinhChuNhat() {
		X = (float) (Math.random() * 100);
		Y = (float) (Math.random() * 100);
		if (Math.random() > 0.5) {
			dx = -dx;
		}
		if (Math.random() > 0.5) {
			dy = -dy;
		}
	}

	public HinhChuNhat(int X, int Y, int dx, int dy) {
		this.X = X;
		this.Y = Y;
		this.dx = dx;
		this.dy = dy;
	}

	public void drawNhat(Graphics g) {
		if (g != null) {
			g.setColor(color);
			g.fillRect((int) (X - width), (int) (Y - height), (int) height, (int) width);
			g.setColor(Color.black);
			g.drawRect((int) (X - width), (int) (Y - height), (int) height, (int) width);
			Font f = new Font("arial", Font.BOLD, 20);
			g.setFont(f);
			g.drawString("3", (int) (X - (width / 2) + 20), (int) (Y - (height / 2) - 15));
		}
	}

	public void updatePosition(float Width, float Height) {
		X = X + dx;
		Y = Y + dy;
		if (X - width < 0) {
			dx = -dx;
			X = width;
		} else if (X + width > Width) {
			dx = -dx;
			X = Width - width;
		}
		if (Y - height < 0) {
			dy = -dy;
			Y = height;
		} else if (Y - width > Height) {
			dy = -dy;
		}
	}
}

class BallPanel extends JPanel implements Runnable {
	ArrayList<Ball> listOfBalls = new ArrayList<Ball>();
	ArrayList<HinhVuong> listOfVuongs = new ArrayList<HinhVuong>();
	ArrayList<HinhChuNhat> listOfNhats = new ArrayList<HinhChuNhat>();

	public void addBall(int X, int Y, int dx, int dy) {
		listOfBalls.add(new Ball(X, Y, dx, dy));
	}

	public void addVuong(int X, int Y, int dx, int dy) {
		listOfVuongs.add(new HinhVuong(X, Y, dx, dy));
	}

	public void addNhat(int X, int Y, int dx, int dy) {
		listOfNhats.add(new HinhChuNhat(X, Y, dx, dy));
	}

	public void addBall() {
		listOfBalls.add(new Ball());
	}

	public void addVuong() {
		listOfVuongs.add(new HinhVuong());
	}

	public void addnhat() {
		listOfNhats.add(new HinhChuNhat());
	}

	@Override
	public void run() {
		while (true) {
			for (Ball b : listOfBalls) {
				b.updatePosition(this.getWidth(), this.getHeight());
			}
			for (HinhChuNhat n : listOfNhats) {
				n.updatePosition(this.getWidth(), this.getHeight());
			}
			for (HinhVuong v : listOfVuongs) {
				v.updatePosition(this.getWidth(), this.getHeight());
			}
			this.repaint();
			try {
				Thread.sleep(50);
			} catch (InterruptedException ex) {
			}
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (Ball b : listOfBalls) {
			b.drawBall(g);
		}
		for (HinhVuong v : listOfVuongs) {
			v.drawVuong(g);
		}
		for (HinhChuNhat n : listOfNhats) {
			n.drawNhat(g);
		}
	}

	public void start() {
		Thread th = new Thread(this);
		th.start();
	}
}

public class Grapphics {
	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("19H1120035 - Đào Văn Thương");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		BallPanel panel = new BallPanel();
		frame.setContentPane(panel);
		frame.setVisible(true);
		Random ran = new Random();
		panel.start();
		panel.addBall(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10)+2, ran.nextInt(10)+-1);
		panel.addNhat(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10)+1, ran.nextInt(10)+3);
		panel.addVuong(ran.nextInt(600), ran.nextInt(500), ran.nextInt(10)+3, ran.nextInt(10)+1);

	}

}