
public class NhanSu {
	private String maNhanSu;
	private String hoVaTen;
	private double heSoLuong;
	private NgayVaoLam ngayVaoLam;
	private Ngay ngaySinh;
	private CongThang congThang;
	private String chucVu;

	public NhanSu(String maNhanSu, String hoVaTen, double heSoLuong, NgayVaoLam ngayVaoLam, Ngay ngaySinh,
			CongThang congThang, String chucVu) {
		this.maNhanSu = maNhanSu;
		this.hoVaTen = hoVaTen;
		this.heSoLuong = heSoLuong;
		this.ngayVaoLam = ngayVaoLam;
		this.ngaySinh = ngaySinh;
		this.congThang = congThang;
		this.chucVu = chucVu;
	}

	public String getMaNhanSu() {
		return maNhanSu;
	}

	public void setMaNhanSu(String maNhanSu) {
		this.maNhanSu = maNhanSu;
	}

	public String getHoVaTen() {
		return hoVaTen;
	}

	public void setHoVaTen(String hoVaTen) {
		this.hoVaTen = hoVaTen;
	}

	public double getHeSoLuong() {
		return heSoLuong;
	}

	public void setHeSoLuong(double heSoLuong) {
		this.heSoLuong = heSoLuong;
	}

	public NgayVaoLam getNgayVaoLam() {
		return ngayVaoLam;
	}

	public void setNgayVaoLam(NgayVaoLam ngayVaoLam) {
		this.ngayVaoLam = ngayVaoLam;
	}

	public Ngay getNgaySinh() {
		return ngaySinh;
	}

	public void setNgaySinh(Ngay ngaySinh) {
		this.ngaySinh = ngaySinh;
	}

	public CongThang getCongThang() {
		return congThang;
	}

	public void setCongThang(CongThang congThang) {
		this.congThang = congThang;
	}

	public String getChucVu() {
		return chucVu;
	}

	public void setChucVu(String chucVu) {
		this.chucVu = chucVu;
	}

	@Override
	public String toString() {
		return "\n\tMã nhân sự: " + maNhanSu + "\n\tHọ và tên: " + hoVaTen + "\n\tHệ số lương: " + heSoLuong
				+ ngayVaoLam + ngaySinh + congThang + "\n\tChức vụ: " + chucVu;
	}

	public int luongCoBan() {
		int l = 0;
		if (chucVu == "Giám Đốc") {
			return 35;
		} else if (chucVu == "Trưởng phòng") {
			return 25;
		} else if (chucVu == "Nhân viên") {
			return 15;
		}
		return l;
	}

	public double soLuong() {
		double soNgayTinhLuong = this.congThang.getSoNgayCong() + this.congThang.getSoNgayNghiPhep();
		double luong = (this.heSoLuong * this.luongCoBan() * soNgayTinhLuong) / 24;
		return luong;
	}

	public String sinhNhatTrongQuy() {
		if (this.ngaySinh.getThang() >= 1 && this.ngaySinh.getThang() <= 3) {
			return "Quý 1";
		}
		if (this.ngaySinh.getThang() >= 4 && this.ngaySinh.getThang() <= 6) {
			return "Quý 2";
		}
		if (this.ngaySinh.getThang() >= 7 && this.ngaySinh.getThang() <= 9) {
			return "Quý 3";
		}
		if (this.ngaySinh.getThang() >= 10 && this.ngaySinh.getThang() <= 12) {
			return "Quý 4";
		}
		return null;

	}
}
