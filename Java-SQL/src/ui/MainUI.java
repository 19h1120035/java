package ui;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainUI extends JFrame {
	
	JButton btnSelect, btnInsert, btnUpdate, btnDelete;
	
	
	public MainUI(String title) {
		super(title);
		addControls();
		addEvents();

	}

	private void addEvents() {
		btnSelect.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SelectUI ui = new SelectUI("M�n h�nh Select cho PrepareStatement");
				ui.showWindow();
				
			}
		});
		btnInsert.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				InsertUI ui = new InsertUI("M�n h�nh Insert cho PrepareStatement");
				ui.showWindow();
			}
		});
		btnUpdate.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				UpdateUI ui = new UpdateUI("M�n h�nh Update cho PrepareStatement");
				ui.showWindow();
			}
		});
		btnDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SelectToDeleteUI ui = new SelectToDeleteUI("M�n h�nh X�a cho PrepareStatement");
				ui.showWindow();
			}
		});
	}

	private void addControls() {
		Container con = getContentPane();
		JPanel pnMain = new JPanel();
		pnMain.setLayout(new FlowLayout());
		btnSelect = new JButton("PrepareStatement - Select");
		btnInsert = new JButton("PrepareStatement - Insert");
		btnUpdate= new JButton("PrepareStatement - Update");
		btnDelete= new JButton("PrepareStatement - Delete");
		pnMain.add(btnSelect);
		pnMain.add(btnInsert);
		pnMain.add(btnUpdate);
		pnMain.add(btnDelete);
		con.add(pnMain);
	}

	public void showWindow() {
		this.setSize(300, 400);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
