package ui;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class InsertUI extends JFrame {
	JTextField txtID, txtTen, txtAddress, txtPhone, txtPhone2;
	JButton btnLuu;
	Connection conn = null;
	
	public InsertUI(String title) {
		super(title);
		addControls();
		addEvents();
		ketNoi();
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BoxLayout(con, BoxLayout.Y_AXIS));
		
		JPanel pnID = new JPanel();
		pnID.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblID = new JLabel("ID: ");
		txtID = new JTextField(20);
		pnID.add(lblID);
		pnID.add(txtID);
		con.add(pnID);
		
		JPanel pnTen = new JPanel();
		pnTen.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblTen = new JLabel("Tên danh bạ: ");
		txtTen = new JTextField(20);
		pnTen.add(lblTen);
		pnTen.add(txtTen);
		con.add(pnTen);
		
		JPanel pnAddress = new JPanel();
		pnAddress.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblAddress = new JLabel("Địa chỉ: ");
		txtAddress = new JTextField(20);
		pnAddress.add(lblAddress);
		pnAddress.add(txtAddress);
		con.add(pnAddress);
		
		JPanel pnPhone = new JPanel();
		pnPhone.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPhone = new JLabel("Số điện thoại: ");
		txtPhone = new JTextField(20);
		pnPhone.add(lblPhone);
		pnPhone.add(txtPhone);
		con.add(pnPhone);

		JPanel pnPhone2 = new JPanel();
		pnPhone2.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPhone2 = new JLabel("Số điện thoại khác: ");
		txtPhone2 = new JTextField(20);
		pnPhone2.add(lblPhone2);
		pnPhone2.add(txtPhone2);
		con.add(pnPhone2);
		
		JPanel pnButton = new JPanel();
		pnButton.setLayout(new FlowLayout(FlowLayout.CENTER));
		btnLuu = new JButton("Lưu");
		pnButton.add(btnLuu);
		con.add(pnButton);
		
		lblID.setPreferredSize(lblPhone2.getPreferredSize());
		lblTen.setPreferredSize(lblPhone2.getPreferredSize());
		lblAddress.setPreferredSize(lblPhone2.getPreferredSize());
		lblPhone.setPreferredSize(lblPhone2.getPreferredSize());
	}

	private void addEvents() {
		btnLuu.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyLuu();
				
			}
		});
	}

	protected void xuLyLuu() {
		try {
			String sql = "insert into qldanhba values (?,?,?,?,?)";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			preStatement.setInt(5, Integer.parseInt(txtID.getText()));
			preStatement.setString(1,txtTen.getText());
			preStatement.setString(2,txtAddress.getText());
			preStatement.setString(3,txtPhone.getText());
			preStatement.setString(4,txtPhone2.getText());
			int x = preStatement.executeUpdate();
			if (x > 0) {
				JOptionPane.showMessageDialog(null, "Thêm Danh Bạ mới thành công !");
			}
			dispose(); // ẩn màn hình này
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ketNoi() {
		try {
			String url = "jdbc:postgresql://localhost:2004/danhba";
			Properties pro = new Properties();
			pro.put("user", "postgres");
			pro.put("password", "20042001");
			conn = DriverManager.getConnection(url, pro);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void showWindow() {
		this.setSize(400, 300);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
