package ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.DefaultTableModel;

public class SelectToDeleteUI extends JFrame {
	DefaultTableModel dtmDanhBa;
	JTable tblDanhBa;
	JMenuItem mnuDelete;
	JPopupMenu popup;

	public static Connection conn = null;
	public static Statement statement = null;

	public SelectToDeleteUI(String title) {
		super(title);
		addControls();
		addEvents();
		ketNoi();
		hienThiDanhBa();
	}

	private void addEvents() {
		tblDanhBa.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					int row = tblDanhBa.rowAtPoint(e.getPoint());
					int column = tblDanhBa.columnAtPoint(e.getPoint());
					if (!tblDanhBa.isRowSelected(row)) {
						tblDanhBa.changeSelection(row, column, false, false);
					}
					popup.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
		mnuDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyXoa();
			}
		});
	}

	protected void xuLyXoa() {
		// Xóa theo cách thông thường
		/*
		int ret = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn xóa danh bạ này không ?",
				"Xác nhận Xóa danh bạ", JOptionPane.YES_NO_OPTION);
		int rowSelected = tblDanhBa.getSelectedRow();
		if (rowSelected == -1)
			return;
		String id = tblDanhBa.getValueAt(rowSelected, 0) + "";
		String ma = tblDanhBa.getValueAt(tblDanhBa.getSelectedRow(), 0) + "";
		if (ret == JOptionPane.YES_OPTION) {
			try {
				String sql = "delete from qldanhba where id = '" + ma + "'";
				statement = conn.createStatement();
				int x = statement.executeUpdate(sql);
				if (x > 0) {
					JOptionPane.showMessageDialog(null, "Xóa danh bạ thành công !");
					hienThiDanhBa();
				}

			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
		*/
		// Xóa theo cách PreparedStatement
		int ret = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn xóa danh bạ này không ?",
				"Xác nhận Xóa danh bạ", JOptionPane.YES_NO_OPTION);
		int rowSelected = tblDanhBa.getSelectedRow();
		if (rowSelected == -1)
			return;
		if (ret == JOptionPane.YES_OPTION) {
			try {
				String ma = tblDanhBa.getValueAt(tblDanhBa.getSelectedRow(), 0) + "";
				int id = Integer.parseInt(ma);
				String sql = "delete from qldanhba where id=?";
				PreparedStatement preStatement = conn.prepareStatement(sql);
				preStatement.setInt(1, id);
				int x = preStatement.executeUpdate();
				if (x > 0) {
					JOptionPane.showMessageDialog(null, "Xóa danh bạ thành công !");
					hienThiDanhBa();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void hienThiDanhBa() {
		try {
			String sql = "SELECT * FROM qldanhba order by id ";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			dtmDanhBa.setRowCount(0);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				Object[] arr = { result.getInt(5), result.getString(1), result.getString(2), result.getString(3),
						result.getString(4) };
				dtmDanhBa.addRow(arr);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ketNoi() {
		try {
			String url = "jdbc:postgresql://localhost:2004/danhba";
			Properties pro = new Properties();
			pro.put("user", "postgres");
			pro.put("password", "20042001");
			conn = DriverManager.getConnection(url, pro);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BorderLayout());
		dtmDanhBa = new DefaultTableModel();
		dtmDanhBa.addColumn("ID");
		dtmDanhBa.addColumn("Tên danh bạ");
		dtmDanhBa.addColumn("Địa chỉ");
		dtmDanhBa.addColumn("Số điện thoại");
		dtmDanhBa.addColumn("Số điện thoại 2");
		tblDanhBa = new JTable(dtmDanhBa);
		JScrollPane sc = new JScrollPane(tblDanhBa, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		con.add(sc, BorderLayout.CENTER);

		mnuDelete = new JMenuItem("Xóa");
		popup = new JPopupMenu();
		popup.add(mnuDelete);
	}

	public void showWindow() {
		this.setSize(600, 400);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
