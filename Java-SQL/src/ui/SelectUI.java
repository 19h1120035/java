package ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class SelectUI extends JFrame {
	DefaultTableModel dtmDanhBa;
	JTable tblDanhBa;

	public static Connection conn = null;
	public static Statement statement = null;

	public SelectUI(String title) {
		super(title);
		addControls();
		ketNoi();
		hienThiDanhBa();
	}

	private void hienThiDanhBa() {
		try {
			String sql = "SELECT * FROM qldanhba where id >? order by id";
			PreparedStatement preStatement = conn.prepareStatement(sql);
			preStatement.setInt(1, 0);
			dtmDanhBa.setRowCount(0);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				Object []arr= {
						result.getInt(5),
						result.getString(1),
						result.getString(2),
						result.getString(3),
						result.getString(4)
				};	
				dtmDanhBa.addRow(arr);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ketNoi() {
		try {
			String url = "jdbc:postgresql://localhost:2004/danhba";
			Properties pro = new Properties();
			pro.put("user", "postgres");
			pro.put("password", "20042001");
			conn = DriverManager.getConnection(url, pro);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BorderLayout());
		dtmDanhBa = new DefaultTableModel();
		dtmDanhBa.addColumn("ID");
		dtmDanhBa.addColumn("Tên danh bạ");
		dtmDanhBa.addColumn("Địa chỉ");
		dtmDanhBa.addColumn("Số điện thoại");
		dtmDanhBa.addColumn("Số điện thoại 2");
		tblDanhBa = new JTable(dtmDanhBa);
		JScrollPane sc = new JScrollPane(tblDanhBa, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		con.add(sc, BorderLayout.CENTER);
	}

	public void showWindow() {
		this.setSize(600, 400);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
