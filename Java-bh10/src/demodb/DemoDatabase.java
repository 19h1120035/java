package demodb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

public class DemoDatabase {

	public static void main(String[] args) throws SQLException {
		String url = "jdbc:postgresql://localhost:2004/demodata";
		Properties props = new Properties();
		props.setProperty("user", "postgres");
		props.setProperty("password", "20042001");
		Connection conn = DriverManager.getConnection(url, props);
		if (conn != null) {
			System.out.println("Kết nối thành công");
		} else {
			System.err.println("Kết nối thất bại");
		}
		Statement stmt = conn.createStatement();
		
		Random rand = new Random();
		for(int i=1; i<=100; i++) {
			String B = UUID.randomUUID().toString();
			float C = rand.nextFloat();
			String sql = "Insert into data values("+ i+",'"+ B+"', "+ C+")";
			stmt.executeUpdate(sql);
		}
		ResultSet rs = stmt.executeQuery("select * from data");
		while (rs.next())
			System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getFloat(3));

	}

}