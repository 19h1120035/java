package train_03;

import java.util.Random;

public class Thread1 extends Thread {
	int n;

	public Thread1(int n) {
		this.n = n;
	}

	@Override
	public void run() {
		try {
			for (int i = 1; i <= n; i += 2) {
				System.out.println("Số chẵn của Thread 1: " + i);
				Thread.sleep(100);
			}
		} catch (Exception e) {
		}
	}
}
