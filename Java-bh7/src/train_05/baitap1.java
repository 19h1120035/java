package train_05;

import java.util.Scanner;

public class baitap1 {
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Start");
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập số nguyên n: ");
		int n = sc.nextInt();
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					for (int i = 1; i <= n; i += 2) {
						System.out.println("Số lẻ của Thread 1 : " + i);
						Thread.sleep(100);
					}
				} catch (Exception e) {
				}
			}
		});
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					for (int i = 0; i <= n; i += 2) {
						System.out.println("\t\t\t\tSố chẵn của Thread 2 : " + i);
						Thread.sleep(100);
					}
				} catch (Exception e) {
				}
			}
		});
		t2.start();
		t1.start();
		t1.join();
		t2.join();
		System.out.println("Ends");
	}
}
