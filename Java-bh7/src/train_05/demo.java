package train_05;

public class demo {
	public static void main(String[] args) {
		System.out.println("15 số nguyên tố đầu tiên :");
		int dem = 0;
		for (int i = 0; i <= 100; i++) {
			if (isPrime(i) == 1) {
				System.out.println(i + " ");
				dem++;
			}
		}
	}

	public static int isPrime(int n) {
		int dem = 0;
		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				dem++;
			}
		}
		if (dem == 2) {
			return 1;
		} else
			return 0;

	}
}
