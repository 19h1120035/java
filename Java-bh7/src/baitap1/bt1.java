package baitap1;

import java.util.Scanner;

public class bt1{
	public static void main(String[] args) throws InterruptedException {
		Scanner sc =new Scanner(System.in);
		System.out.println("====MutilThread Exercies 1====");
		System.out.println("19H1120035 - Đào Văn Thương");
		System.out.println("Nhập số nguyên n: ");
		int n = sc.nextInt();
		Run1 r1 = new Run1(n, "Thread 1");
//		Thread th1 = new Thread(r1);
		Thread th1 = new Thread(new Run1(n, "Thread 1"));
		Thread th2 = new Thread(new Run2(n, "Thread 2"));
		System.out.println("Start!!!");
		th2.start();
		th1.start();
		th2.join();
		th1.join();
		System.out.println("Ends!");
		
	}
}
class Run1 implements Runnable{
	int n;
	String threadName;
	public Run1(int n, String threadName) {
		this.n = n;
		this.threadName = threadName;
	}
	@Override
	public void run() {
		for (int i = 1; i <= n; i+=2) {
			System.out.println("Số lẻ của "+threadName+" là: "+i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
class Run2 implements Runnable{
	int n;
	String threadName;
	
	public Run2(int n, String threadName) {
		this.n = n;
		this.threadName = threadName;
	}

	@Override
	public void run() {
		for (int i = 0; i <= n; i+=2) {
			System.out.println("\t\t\t\tSố chẵn của "+threadName+" là: "+i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}