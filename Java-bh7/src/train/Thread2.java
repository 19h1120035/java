package train;

import java.util.Scanner;

public class Thread2 extends Thread {
	SharedData sharedData;

	public Thread2(SharedData sharedData) {
		this.sharedData = sharedData;
	}

	@Override
	public void run() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập số nguyên n: ");
		int n = sc.nextInt();
		for (int i = 0; i <= n; i+=2) {
			synchronized (sharedData) {
//				System.out.println("T2 > i >  " + i);
//				sharedData.notifyAll();
//				if (i == 9) {
//					System.out.println("Stop t2!");
//					stop();
//				}
				try {
					sharedData.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
//				int result = sharedData.rad * sharedData.rad;
				System.out.println("\t\t\t\tSố nguyên chẵn của Thread 2: " + i);
			}
		}
	}

}
