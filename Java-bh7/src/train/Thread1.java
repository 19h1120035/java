package train;

import java.util.Scanner;

public class Thread1 extends Thread {
	SharedData sharedData;

	public Thread1(SharedData sharedData) {
		this.sharedData = sharedData;
	}

	@Override
	public void run() {
//		Random random = new Random();
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập vào số nguyên n: ");
		int n = sc.nextInt();
		for (int i = 1; i <= n; i+=2) {
			synchronized (sharedData) {
//				System.out.println("T1 > i >  " + i);
//				try {
//					sharedData.wait();
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				int rad = random.nextInt(100);
//				sharedData.rad = rad;
				System.out.println("Số nguyên lẻ của Thread 1: " + i);
				sharedData.notifyAll();
//				if (i == 9) {
//					System.out.println("Stop t1");
//					stop();
//				}
				try {
					sharedData.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}
}
