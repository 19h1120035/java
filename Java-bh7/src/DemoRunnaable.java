
public class DemoRunnaable {
	public static void main(String[] args) {
		// Tạo 1 thread chạy song song với thread main
		HelloRunnable runnable = new HelloRunnable("Thread 1");
		Thread th1 = new Thread(runnable);
		th1.start();
		Thread th2 = new Thread(new HelloRunnable("Thread 2"));
		th2.start();

//		Tạo liên lục 10 thread
//		for (int i = 1; i <= 10; i++) {
//			(new Thread(new HelloRunnable("Thread " + i + "/10"))).start();
//
//		}
		for (int i = 1; i <= 10; i++) {
			Thread th =  new Thread(new HelloRunnable("Thread " + i + "/10"));
//			th.setPriority(10-i);
			th.start();
			
		}
	}

}

class HelloRunnable implements Runnable {
	String threadName;

	public HelloRunnable(String threadName) {
		this.threadName = threadName;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < 5; i++) {
				System.out.println(i+". Hello from a " + threadName + " !");
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
