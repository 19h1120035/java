package train_04;

public class Thread2 extends Thread {
	int n;
	public Thread2(int n) {
		this.n = n;
	}
	@Override
	public void run() {
		try {
			for (int i = 0; i <= n; i += 2) {
				System.out.println("\t\t\t\tSố chẵn của Thread 2: " + i);
				Thread.sleep(100);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
