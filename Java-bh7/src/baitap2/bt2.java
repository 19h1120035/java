package baitap2;

import java.util.Scanner;

public class bt2{
	public static void main(String[] args) throws InterruptedException {
		Scanner sc =new Scanner(System.in);
		System.out.println("====MutilThread Exercies 2====");
		System.out.println("19H1120035 - Đào Văn Thương");
		System.out.println("Nhập số nguyên n: ");
		int n = sc.nextInt();
		Thread th1 = new Thread(new Thread1(n, "Thread 1"));
		Thread th2 = new Thread(new Thread2(n, "Thread 2"));
		System.out.println("Start!!!");
		th2.start();
		th1.start();
		th2.join();
		th1.join();
		System.out.println("Ends!");
		
	}
}
class Thread1 extends Thread {
	int n;
	String threadName;

	public Thread1(int n, String threadName) {
		this.n = n;
		this.threadName = threadName;
	}

	@Override
	public void run() {
		try {
			for (int i = 1; i <= n; i += 2) {
				System.out.println("Số lẻ của "+threadName+" là: "+ i);
				Thread.sleep(100);
			}
		} catch (Exception e) {
		}
	}
}

class Thread2 extends Thread {
	int n;
	String threadName;

	public Thread2(int n, String threadName) {
		this.n = n;
		this.threadName = threadName;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i <= n; i += 2) {
				System.out.println("\t\t\t\tSố chẵn của "+threadName+" là: "+ i);
				Thread.sleep(100);
			}
		} catch (Exception e) {
		}
	}
}