package baitap3;
import java.util.Scanner;

public class bt3 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("+++ MutilThread Bai tap 3 +++");
		System.out.println("19H1120054 - Nguyen Minh Nhat");
		System.out.print("Nhập số lượng thread muốn tạo: ");
		int n = in.nextInt();
		System.out.print("Nhap giới hạn số nguyên tố cần tìm ");
		int m = in.nextInt();
		int fist = 0;
		int last = 0;
		for (int i = 0; i < n; i++) {
			fist = last;
			last = last + (m / n);
			(new Thread(new Prime(fist, last))).start();
		}
	}
}

class Prime implements Runnable {
	int a = 0;
	int b = 0;

	public Prime(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public void run() {
		for (int i = a; i < b; i++) {

			if (IsPrime(i)) {

				System.out.print(i + ", ");
			}
		}
	}

	boolean IsPrime(int n) {
		if (n < 2)
			return false;
		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
}
