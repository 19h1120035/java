package baitap3;

public class ThreadPrime extends Thread {
	private int n;
	private int m;
	private int t;

	public ThreadPrime(int n, int m, int t) {
		this.n = n;
		this.m = m;
		this.t = t;
	}
	public static int isPrime(int m) {
		int dem = 0;
		for (int i = 1; i <= m; i++) {
			if (m % i == 0) {
				dem++;
			}
		}
		if (dem == 2) {
			return 1;
		} else
			return 0;
	}

	@Override
	public void run() {
		try {
			int dem = 0;
			for (int i = (t - 1) * (m / n); i <=t*(m / n); i++) {
				if (isPrime(i) == 1) {
					System.out.println("Số nguyên tố Thread " + t + " là: " + i);
					dem++;
				}

				Thread.sleep(100);
			}
		} catch (Exception e) {
		}
	}

}
