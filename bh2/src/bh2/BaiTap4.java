
//TÌM SỐ NGUYÊN ÂM TRONG 1 CHUỖI

package bh2;

import java.util.Scanner;

public class BaiTap4 {
	public static void main(String[] args) {
		String str;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập vào một chuỗi: ");
		str = sc.next();
		int count = 0;
		str = str.toLowerCase();
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if ( ch == 'e' || ch == 'u' || ch == 'o' || ch == 'a' || ch == 'i')
				count++;
		}
		System.out.println("Số nguyên âm là: "+ count);
		
	}
}
