//HIỂN THỊ SỐ NGŨ GIÁC
package bh2;

public class Baitap7 {
	public static void main(String[] args) {
		int count = 1;
		for (int i = 1; i <= 50; i++) {
			System.out.printf("%-6d", soNguGiac(i));
			if (count % 10 == 0)
				System.out.println();
			count++;
		}
	}

	public static int soNguGiac(int i) {
		return (i * (3 * i - 1)) / 2;
	}
}
