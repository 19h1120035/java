//TỔNG CÁC CHỮ SỐ TRONG 1 SỐ NGUYÊN
package bh2;

import java.util.Scanner;

public class BaiTap6 {
public static void main(String[] args) {
	int n;
	Scanner sc = new Scanner(System.in);
	System.out.println("Nhập vào số nguyên n: ");
	n = sc.nextInt();
	int tong = 0;
	while (n > 0) {
		int soDu = n % 10;
		n /= 10;
		tong += soDu;
	}
	System.out.println("Tổng các chữ số trong một số nguyên là: "+ tong);
}
}
