//KIỂM TRA MẬT KHẨU

package bh2;

import java.util.Scanner;
import java.util.regex.*;

public class BaiTap11 {
	public static final int PASSWORD_LENGTH = 10;

	public static void main(String[] args) {
		System.out.println("===============QUT TẮC MẬT KHẨU================");
		System.out.println("1. Mật khẩu phải có ít nhất 10 kí tự.          ");
		System.out.println("2. Mật khẩu chỉ bao gồm các chữ số và chữ cái. ");
		System.out.println("3. Mật khẩu phải chứa ít nhất hai chữ số.      \n");
		String pass = passWord();
		System.out.println("Mật khẩu của bạn là: " + pass);
	}

	public static String passWord() {
		Scanner sc = new Scanner(System.in);
		String regex = "^[a-z0-9]*\\d\\d[a-z0-9]*$";
		String password = "";
		Pattern p = Pattern.compile(regex);
		while (true) {
			System.out.println("Nhập mật khẩu của bạn (Đồng ý với các điều kiện trên): ");
			password = sc.nextLine();
			if (p.matcher(password).find() && password.length() >= PASSWORD_LENGTH) {
				System.out.println("Mật khẩu hợp lệ!");
				break;
			} else
				System.err.println("Mật khẩu không hợp lệ!");
		}
		return password;
	}
}
