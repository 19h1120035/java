
//TÍNH DIỆN TÍCH HÌNH TAM GIÁC

package bh2;
import java.util.Scanner;
public class BaiTap13 {
	public static void main(String[] args) {
		/*
		 * Áp dụng công thức heron: S = sqrt(p*(p-a)*(p-b)*(p-c)) với p là chu vi của nửa tam giác
		 * chu vi nửa tam giác: p = (a+b+c)/2
		 * 
		 */
		Scanner sc = new Scanner(System.in);
		double s1, s2, s3, p, s;
		System.out.println("Nhập cạnh thứ nhất của tam giác: ");
		s1 = sc.nextDouble();
		System.out.println("Nhập cạnh thứ hai của tam giác: ");
		s2 = sc.nextDouble();
		System.out.println("Nhập cạnh thứ ba của tam giác: ");
		s3 = sc.nextDouble();
		p = (s1 + s2 + s3) / 2;
		s = Math.sqrt(p * (p - s1) * (p - s2) * (p - s3));
		System.out.println("Chu vi của nửa tam giác: P = "+ p);
		System.out.println("Diện tích của tam giác là: S =  "+ s );
	}
}
