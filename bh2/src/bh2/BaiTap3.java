
//TÌM KÍ TỰ GIỮA TRONG MỘT CHUỖI

package bh2;

import java.util.Scanner;

public class BaiTap3 {
	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	System.out.println("Nhập vào một chuỗi: ");
	String str = sc.nextLine();
	System.out.println("Kí tự giữa trong chuỗi: "+ middle(str));
	}
	public static String middle(String str) {
		int position;
		int length;
		if (str.length() % 2 == 0) {
			position = str.length() / 2 - 1;
			length = 2;
		}
		else {
			position = str.length() / 2;
			length = 1;
		}
		return str.substring(position, position + length);
	}
}
