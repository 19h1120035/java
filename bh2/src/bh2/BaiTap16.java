//TÌM CẶP SỐ NGUYÊN TỐ SINH ĐÔI 
package bh2;

public class BaiTap16 {
	public static void main(String[] args) {
		System.out.println("Số các nguyên tố sinh đôi nhỏ hơn 100: ");
		for (int i = 2; i < 100; i++) {
			if (soNguyenTo(i) && soNguyenTo(i + 2))
				System.out.printf("(%d, %d)\n", i, i + 2);
		}
	}

	public static boolean soNguyenTo(long n) {
		if (n < 2)
			return false;
		for (int i = 2; i <= n / 2; i++)
			if (n % i == 0)
				return false;
		return true;
	}
}
