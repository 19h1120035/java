// TÍNH NĂM NHUẬN
package bh2;

import java.util.Scanner;

public class Baitap10 {
	public static void main(String[] args) {
		int nam;
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập vào 1 năm: ");
		nam = sc.nextInt();
		if ((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0)
			System.out.println("Năm " + nam + " là một năm nhuận !!");
		else
			System.out.println("Năm " + nam + " không phải là một năm nhuận !!");
	}
}
