//TÍNH GIÁ TRỊ ĐẦU TƯ TRONG TƯƠNG LAI
package bh2;

import java.util.Scanner;

public class BaiTap8 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Nhập số tiền cần đầu tư: ");
		double dauTu = in.nextDouble();
		System.out.print("Nhập lãi suất: ");
		double laiSuat = in.nextDouble();
		System.out.print("Nhập số năm đầu vào: ");
		int nam = in.nextInt();
		laiSuat *= 0.01;
		System.out.println("Năm         Tương lai");
		for (int i = 1; i <= nam; i++) {
			int format = 19;
			if (i >= 10)
				format= 18;
			System.out.printf(i + "%" + format + ".2f\n", giaTriDauTu(dauTu, laiSuat / 12, i));
		}
	}

	public static double giaTriDauTu(double soTienDauTu, double laiSuatHangThang, int nam) {
		return soTienDauTu * Math.pow(1 + laiSuatHangThang, nam * 12);
	}
}