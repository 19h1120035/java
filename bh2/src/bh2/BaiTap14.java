//TÍNH DIỆN TÍCH HÌNH NGŨ GIÁC

package bh2;

import java.util.Scanner;

public class BaiTap14 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhập vào số cạnh: ");
		int n = sc.nextInt();
		System.out.print("Nhập cạnh a: ");
		double a = sc.nextDouble();

		System.out.println("Diện tích của hình ngũ giác là:  " + area(n, a));
	}

	public static double area(int n, double a) {
		return (n * a * a) / (4 * Math.tan(Math.PI / n));
	}
}