package demo;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ViDu2 extends JFrame{
	public ViDu2 (String title) {
		super(title);
		initUI();
	}
	private void initUI() {
		this.setSize(500, 400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel pn1 = new JPanel();
		pn1.setBackground(new Color(21, 0, 97));
		JButton btn1 = new JButton("FlowLayout");
		JButton btn2 = new JButton("Add các controls");
		JButton btn3 = new JButton("Trên một dòng");
		JButton btn4 = new JButton("Hết chỗ chứa");
		JButton btn5 = new JButton("Thì xuống dòng");
		pn1.add(btn1);
		pn1.add(btn2);
		pn1.add(btn3);
		pn1.add(btn4);
		pn1.add(btn5);
		this.add(pn1);
	}
	public static void main(String[] args) {
		new ViDu2("Ví Dụ 2").setVisible(true);
	}
}
