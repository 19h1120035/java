package demo;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Hello extends JFrame {
	public Hello() {
		JLabel lable = new JLabel("Hello Swing!", SwingConstants.CENTER);
		lable.setForeground(Color.GREEN);
		lable.setFont(new Font(Font.SERIF,Font.BOLD, 60));
		this.setSize(400,400);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.add(lable);
	}
	public static void main(String[] args) {
		new Hello().setVisible(true);
	}
}
