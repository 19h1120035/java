package demo;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class ViDu1 extends JFrame {
	public ViDu1(String title) {
		super(title);
		this.setSize(500, 400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton btn1 = new JButton("Center");
		JButton btn2 = new JButton("Left");
		JButton btn3 = new JButton("Right");
		JButton btn4 = new JButton("Top");
		JButton btn5 = new JButton("Bottom");
		this.add(btn1);
		this.add(btn2, BorderLayout.WEST);
		this.add(btn3, BorderLayout.EAST);
		this.add(btn4, BorderLayout.NORTH);
		this.add(btn5, BorderLayout.SOUTH);
	}

	public static void main(String[] args) {
		new ViDu1("Hello Swing").setVisible(true);
		
	}
}
