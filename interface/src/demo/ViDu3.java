package demo;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.LayoutManager;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class ViDu3 extends JFrame {
	String title;

	public ViDu3(String title) throws HeadlessException {
		super();
		initUI();
	}
	private void initUI() {
		this.setSize(500,300);
		this.setAlwaysOnTop(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(true);
		JPanel pnBoxLayout = new JPanel();
		pnBoxLayout.setLayout(new BoxLayout(pnBoxLayout, BoxLayout.X_AXIS));
		JButton btn1 = new JButton("BoxLayOut");
		Font fbtn1 = new Font("Tahoma",Font.BOLD, 24);
		btn1.setFont(fbtn1);
		btn1.setForeground(Color.red);
		JButton btn2 = new JButton("X_AXIS");
		btn2.setFont(fbtn1);
		btn2.setForeground(Color.blue);;
		JButton btn3 = new JButton("Y_AXIS");
		btn3.setFont(fbtn1);
		btn3.setForeground(Color.green);
		pnBoxLayout.add(btn1);
		pnBoxLayout.add(btn2);
		pnBoxLayout.add(btn3);
		this.add(pnBoxLayout);
	}
	public static void main(String[] args) {
		ViDu3 viDu3 = new ViDu3("Ví Dụ 3");
		viDu3.setVisible(true);
	}
	
}
