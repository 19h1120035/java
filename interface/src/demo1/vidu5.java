package demo1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JTable;

public class vidu5 extends JFrame {

	private JPanel dsg;
	private JButton btn1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vidu5 frame = new vidu5();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public vidu5() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		dsg = new JPanel();
		dsg.setBorder(new EmptyBorder(5, 5, 5, 5));
		dsg.setLayout(new BorderLayout(0, 0));
		setContentPane(dsg);
		
		btn1 = new JButton("OK");
		btn1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btn1.setBackground(Color.green);
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(rootPane, "Hello Swing");
			}
		});
		dsg.add(btn1, BorderLayout.CENTER);
	}

}
