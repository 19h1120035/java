package demo1;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Vidu1 extends JFrame {
	JButton button;

	public Vidu1() {
		createAndShow();
	}

	public void createAndShow() {
		button = new JButton("OK");
//		button = new JFrame("Titile");
		setSize(400,300);
		setBounds(550, 200, 400, 300);
		setTitle("Bài tập 1");
		this.setLocationRelativeTo(null); // hiển thị giữa màn hình
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // dừng khi tắt cửa sổ
		this.add(button); // thêm button vào cửa sổ
		this.setLayout(new FlowLayout());
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(rootPane, "Hello Swing !");
			}
		});
//		frame.pack(); // hiển thị cửa sổ vừa đủ với botton
		
		
	}

	public static void main(String[] args) {
		new Vidu1();
	}
}
