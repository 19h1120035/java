package model;

import java.io.Serializable;
import java.util.Scanner;

public class KhachHang implements Serializable, Comparable<KhachHang> {
	private int maKH;
	private String tenKH;
	private String phone;
	public KhachHang() {
	}
	public KhachHang(int maKH, String tenKH, String phone) {
		this.maKH = maKH;
		this.tenKH = tenKH;
		this.phone = phone;
	}
	public int getMaKH() {
		return maKH;
	}
	public void setMaKH(int maKH) {
		this.maKH = maKH;
	}
	public String getTenKH() {
		return tenKH;
	}
	public void setTenKH(String tenKH) {
		this.tenKH = tenKH;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return this.maKH + "\t\t" + this.tenKH + "\t\t" + this.phone;
	}
	public void display() {
		System.out.println(this);
	}
	public void input () {
		Scanner sc= new Scanner(System.in);
		System.out.println("Nhập mã khách hàng: ");
		maKH = Integer.parseInt(sc.nextLine());
		System.out.println("Nhập họ và tên khách hàng:");
		tenKH = sc.nextLine();
		System.out.println("Nhập số điện thoại: ");
		phone = sc.nextLine();
	}
	public int compareTo(KhachHang o) {
		return this.phone.compareToIgnoreCase(o.phone);
	}
	
	
}
