package test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import io.SerializeFileFactory;
import model.KhachHang;

public class TestKH {
	static Scanner sc = new Scanner(System.in);
	static ArrayList<KhachHang> dsKH = new ArrayList<KhachHang>();

	public static void showMenu() {
		System.out.println("1. Nhập thông tin khách hàng.");
		System.out.println("2. Xuất thông tin khách hàng.");
		System.out.println("3. Tìm kiếm thông tin khách hàng.");
		System.out.println("4. Sắp xếp.");
		System.out.println("5. Lưu khách hàng.");
		System.out.println("6. Đọc khách hàng.");
		System.out.println("7. Thống kê theo nhà mạng.");
		System.out.println("8. Thoát.");
		System.out.println("Bạn chọn chức năng [1..8]: ");
		int chon = sc.nextInt();
		switch (chon) {
		case 1: {
			input();
			break;
		}
		case 2: {
			display();
			break;
		}
		case 3: {
			search();
			break;
		}
		case 4: {
			sort();
			break;
		}
		case 5: {
			save();
			break;
		}
		case 6: {
			read();
			break;
		}
		case 7: {
			thongKe();
			break;
		}
		case 8: {
			System.out.println("Tạm biệt !!");
			System.exit(0);
		}
		default:
			System.err.println("Nhập sai ! Vui lòng nhập lại.");
		}
	}

	private static void thongKe() {
		int n = 0;
		for (KhachHang kh : dsKH) {
			if (kh.getPhone().startsWith("03")) {
				n++;
			}
		}
		System.out.println("Có " + n + " khách hàng Viettel");
	}

	private static void read() {
		dsKH = SerializeFileFactory.docFile("D:\\datakhachhang.txt");
		System.out.println("Đã đọc file thành công !! ");
	}

	private static void save() {
		boolean kt = SerializeFileFactory.luuFile(dsKH, "D:\\datakhachhang.txt");
		if (kt = true) {
			System.out.println("Lưu file thành công !");
		} else {
			System.err.println("Lưu file thất bại !!");
		}
	}

	private static void sort() {
		Collections.sort(dsKH);
		System.out.println("Đã sắp xếp số điện thoại !");

	}

	static void search() {
		System.out.println("Vui lòng nhập đầu số điện thoại cần tìm: ");
		String phone = sc.next();
		System.out.println("================================================");
		System.out.println("Khách hàng có đầu số " + phone + " :");
		System.out.println("Mã KH\t\tTên Khách Hàng\t\tSố điện thoại");
		for (KhachHang kh : dsKH) {
			if (kh.getPhone().startsWith(phone)) {
				System.out.println(kh);
			}
		}
		System.out.println("================================================");
	}

	static void display() {
		System.out.println("================================================");
		System.out.println("Mã KH\t\tTên Khách Hàng\t\tSố điện thoại");
		for (KhachHang kh : dsKH) {
			System.out.println(kh);

		}
		System.out.println("=================================================");
	}

	static void input() {
		KhachHang kh = new KhachHang();
		kh.input();
		dsKH.add(kh);
	}

	public static void main(String[] args) {
		while (true) {
			showMenu();
		}
	}
}
