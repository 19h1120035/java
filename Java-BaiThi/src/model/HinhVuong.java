package model;

import java.awt.Color;
import java.awt.Graphics;

public class HinhVuong {
	int x, y, width, height;
	int radius = 100; // kích thước
	int dx = 3, dy = 3; // hướng đi
	Color color = Color.RED;

	// Constructor
	public HinhVuong(int x, int y,int dx , int dy, int width, int height, Color color) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
	}

	public void updatePosition(float width, float height) {
		x = x + dx;
		y = y + dy;
		if (x - radius < 0) {
			dx = -dx;
			x = radius;
		} else if (x + radius > width) {
			dx = -dx;
			x = (int) (width - radius);
		}
		if (y - radius < 0) {
			dy = -dy;
			y = radius;
		} else if (y + radius > height) {
			dy = -dy;
			y = (int) (height - radius);
		}
	}

	// Paint itself given the Graphics context
	public void paint(Graphics g) {
		g.setColor(Color.blue);
		g.fillRoundRect(250,20,70,60,25,25);
	}
}
