package model;

import java.awt.Color;
import java.awt.Graphics;

public class HinhChuNhat {
	int x, y, width, height; // Use an rectangle for illustration
	Color color = Color.RED; // Color of the object
	int radius = 10; // kích thước
	int dx = 3, dy = 3; // hướng đi

	// Constructor
	public HinhChuNhat(int x, int y, int width, int height, Color color) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
	}

	public void updatePosition(int width, int height) {
		x = x + dx;
		y = y + dy;
		if (x - radius < 0) {
			dx = -dx;
			x = radius;
		} else if (x + radius > width) {
			dx = -dx;
			x = width - radius;
		}
		if (y - radius < 0) {
			dy = -dy;
			y = radius;
		} else if (y + radius > height) {
			dy = -dy;
			y = height - radius;
		}
	}

	// Paint itself given the Graphics context
	public void paint(Graphics g) {
		g.setColor(color);
		g.fillRect(x, y, width, height); // Fill a rectangle
	}
}
