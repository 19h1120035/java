package model;

import java.awt.Color;
import java.awt.Graphics;

public class HinhTron {
	float X, Y; // Vị trí
	float radius = 100; // kích thước
	float dx = 3, dy = 3; // hướng đi

	public HinhTron() {
		X = 50 + (float) (Math.random() * 100);
		Y = 50 + (float) (Math.random() * 100);

		if (Math.random() > 0.5)
			dx = -dx;
		if (Math.random() > 0.5)
			dy = -dy;
	}

	public HinhTron(int x, int y, int dx, int dy) {
		this.X = x;
		this.Y = y;
		this.dx = dx;
		this.dy = dy;
	}

	public void drawBall(Graphics g) {
		if (g != null) {
			g.setColor(Color.blue);
			g.fillOval((int) (X - radius), (int) (Y - radius), (int) radius * 2, (int) radius * 2);

		}
	}

	public void updatePosition(float width, float height) {
		X = X + dx;
		Y = Y + dy;
		if (X - radius < 0) {
			radius -= 0.1 * radius;
			dx = -dx;
			X = radius;
		} else if (X + radius > width) {
			radius -= 0.1 * radius;
			dx = -dx;
			X = width - radius;
		}
		if (Y - radius < 0) {
			radius -= 0.1 * radius;
			dy = -dy;
			Y = radius;
		} else if (Y + radius > height) {
			radius -= 0.1 * radius;
			dy = -dy;
			Y = height - radius;
		}
	}
}
