package ui;

import java.awt.Color;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.HinhChuNhat;
import model.HinhTron;
import model.HinhVuong;

class BallPanel extends JPanel implements Runnable {

	ArrayList<HinhTron> listOfBalls = new ArrayList<HinhTron>();
	ArrayList<HinhVuong> listOfVuong = new ArrayList<HinhVuong>();
	
	public void addBall(int X, int Y, int dx, int dy) {
		listOfBalls.add(new HinhTron(X, Y, dx, dy));
	}
	public void addVuong(int X, int Y, int dx, int dy,int width, int height, Color color) {
		listOfVuong.add(new HinhVuong(X, Y, dx, dy, width, height, color));
	}

	@Override
	public void run() {
		while (true) {
			for (HinhTron b : listOfBalls) {
				b.updatePosition(this.getWidth(), this.getHeight());
			}
			for(HinhVuong v: listOfVuong) {
				v.updatePosition(this.getWidth(), this.getHeight());
			}
			this.repaint();
			try {
				Thread.sleep(30);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (HinhTron b : listOfBalls) {
			b.drawBall(g);
		}
	}

	public void start() {
		Thread th = new Thread(this);
		th.start();
	}

}

public class BouncingBall {
	public static void main(String[] args) {
		JFrame f = new JFrame("Bouncing Ball");
		f.setSize(600, 500);
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		JPanel btnPanel = new JPanel(new FlowLayout());
		BallPanel bpn = new BallPanel();
		f.setContentPane(bpn);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		bpn.start();
		bpn.addBall(10, 10, 10, 5);
		bpn.addVuong(100, 200, 14, 10, 100, 100, Color.RED);
		bpn.addVuong(100, 200, 6, 10, 100, 50, Color.RED);
//		bpn.addBall(100, 0, 50, 25);
//		bpn.addBall(200, 80, 80, 30);
//		bpn.addBall(400, 60, -3, -3);
//		bpn.addBall(0, 500, 10, 10);
	}
}
