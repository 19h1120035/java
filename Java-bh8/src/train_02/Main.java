package train_02;

public class Main {
	public static void main(String[] args) {
		Print pr = new Print();
		for (int i = 0; i <50; i++) {
			ThreadDemo t1 = new ThreadDemo(pr, i+1);
			t1.start();
		}

	}
}


class ThreadDemo extends Thread {
	Print pR;
	int t;

	public ThreadDemo(Print pR, int t) {
		this.pR = pR;
		this.t = t;
	}
	@Override
	public void run() {
		synchronized (pR) {
			pR.print(t);
			
		}
	}
	
}


class Print {
	public void print(int t) {
		System.out.println("Hello from Thread "+ t+" !");
	}
}
