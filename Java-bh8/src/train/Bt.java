package train;

public class Bt {
	public static void main(String[] args) {
		System.out.println("====MutilThread Exercies 3====");
		System.out.println("19H1120035 - Đào Văn Thương");
		for (int i = 50; i > 0; i--) {
			int t = i;
			PrintMessage pm = new PrintMessage(t);
			CreateThread th = new CreateThread(pm);
			th.start();
			try {
				th.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}

class PrintMessage {
	int t;

	public PrintMessage(int t) {
		this.t = t;
	}

	public void printMessage() {
		System.out.println("Hello from Thread " + t + " !");
	}
}

class CreateThread extends Thread {
	PrintMessage PM;

	public CreateThread(PrintMessage pm) {
		PM = pm;
	}

	@Override
	public void run() {
		synchronized (PM) {
			PM.printMessage();
		}
	}

}
