package train_01;

public class Main implements Runnable {
	public int tong;
	public Main() {
		this.tong = 1000;
	}


	public synchronized void rutTien() {
		if (tong > 0) {
			tong = tong - 1000;
			System.out.println("Tổng tiền của Thread :"+tong);
		} else {
			System.out.println("Thread "+"Không còn tiền !!");
		}
	}

	@Override
	public void run() {
		rutTien();
	}
	
	public static void main(String[] args) {
		Main t00 = new Main();
		Thread t1 = new Thread(t00);
		Thread t2 = new Thread(t00);
		t1.start();
		t2.start();
	}
}
