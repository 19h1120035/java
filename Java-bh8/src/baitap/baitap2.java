package baitap;

public class baitap2 {
	public static void main(String[] args) {
		System.out.println("====MutilThread Exercise 2====");
		System.out.println("19H1120035 - Đào Văn Thương");
		BanVeXeBuyt bus = new BanVeXeBuyt();
		KhachThread nam = new KhachThread(2, bus, "Nam");
		KhachThread binh = new KhachThread(2, bus, "Binh");
		nam.start();
		binh.start();
	}
}

class KhachThread extends Thread {
	private int soGheDat;

	public KhachThread(int gh, Runnable daiLy, String hoTen) {
		super(daiLy, hoTen);
		this.soGheDat = gh;
	}

	public int laySoGheDat() {
		return soGheDat;
	}
}

class BanVeXeBuyt implements Runnable {
	private int soGheTrong = 2;

	@Override
	public void run() {
		KhachThread khach = (KhachThread) Thread.currentThread();
		boolean datDuoc = this.banVe(khach.laySoGheDat(), khach.getName());
		if (datDuoc == true) {
			System.out.println(
					"Chuc mung " + Thread.currentThread().getName() + ", " + khach.laySoGheDat() + " ghe da duoc dat.");
		} else {
			System.out.println("Xin loi " + Thread.currentThread().getName() + " khong du so ghe yeu cau ("
					+ khach.laySoGheDat() + ")");
		}
	}

	private synchronized boolean banVe(int soGheDat, String hoTen) {
		System.out.println("Chao mung " + hoTen + " so ghe trong la: " + this.laySoGheTrong());
		if (soGheDat > this.laySoGheTrong()) {
			return false;
		} else {
			soGheTrong = soGheTrong - soGheDat;
			return true;
		}
	}

	private int laySoGheTrong() {
		return soGheTrong;
	}
}
