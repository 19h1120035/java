package baitap;

public class baitap3 {
	public static void main(String[] args) {
		System.out.println("====MutilThread Excersice 3====");
		System.out.println("19H1120035 - Đào Văn Thương");
		HelloClass.startThread(1, 50);
	}
}

class HelloClass implements Runnable {
	int threadNum, maxThread;

	public HelloClass(int threadNum, int maxThread) {
		this.threadNum = threadNum;
		this.maxThread = maxThread;
	}

	@Override
	public void run() {
		if (threadNum < maxThread) {
			Thread th = startThread(threadNum + 1, maxThread);
			try {
				th.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Hello from Thread " + threadNum + " !");
	}

	public static Thread startThread(int threadNum, int maxThread) {
		Thread th = new Thread(new HelloClass(threadNum, maxThread));
		th.start();
		return th;
	}
}
