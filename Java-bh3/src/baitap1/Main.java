package baitap1;

public class Main {
	public static void main(String[] args) {
		PhanSo p1 = new PhanSo(6, 8);
		PhanSo p2 = new PhanSo(2, -3);
		p1.toString();
		p2.toString();
		System.out.println("Phân số thứ 1: " + p1);
		System.out.println("Phân số thứ 2: " + p2);
		System.out.println("\nPhân số 1 + phân số 2: " + p1.congPhanSo(p2));
		System.out.println("Phân số 1 - phân số 2: " + p1.truPhanSo(p2));
		System.out.println("Phân số 1 x phân số 2: " + p1.nhanPhanSo(p2));
		System.out.println("Phân số 1 : phân số 2: " + p1.chiaPhanSo(p2));
		p1.toiGian();
		System.out.println("Tối giản phân số 1: " + p1);
	}
}
