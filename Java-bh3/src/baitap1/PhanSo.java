package baitap1;

public class PhanSo {
	private int tuSo;
	private int mauSo;

	public PhanSo() {
		tuSo = 0;
		mauSo = 1;
	}

	public PhanSo(int tuSo, int mauSo) {
		this.tuSo = tuSo;
		if (mauSo != 0) {
			this.mauSo = mauSo;
		} else
			this.mauSo = 1;
	}

	public int getTuSo() {
		return tuSo;
	}

	public void setTuSo(int tuSo) {
		this.tuSo = tuSo;
	}

	public int getMauSo() {
		return mauSo;
	}

	public void setMauSo(int mauSo) {
		if (mauSo > 0) {
			this.mauSo = mauSo;
		} else
			this.mauSo = 1;
	}

	@Override
	public String toString() {
		if (tuSo * mauSo < 0) {
			return "-" + Math.abs(tuSo) + "/" + Math.abs(mauSo);
		}
		return Math.abs(tuSo) + "/" + Math.abs(mauSo);
	}

	public PhanSo congPhanSo(PhanSo psKhac) {
		PhanSo kq = new PhanSo();
		kq.mauSo = this.mauSo * psKhac.mauSo;
		kq.tuSo = this.tuSo * psKhac.mauSo + this.mauSo * psKhac.tuSo;
		return kq;
	}

	public PhanSo truPhanSo(PhanSo psKhac) {
		PhanSo kq = new PhanSo();
		kq.mauSo = this.mauSo * psKhac.mauSo;
		kq.tuSo = this.tuSo * psKhac.mauSo - this.mauSo * psKhac.tuSo;
		return kq;
	}

	public PhanSo nhanPhanSo(PhanSo psKhac) {
		PhanSo kq = new PhanSo();
		kq.mauSo = this.mauSo * psKhac.mauSo;
		kq.tuSo = this.tuSo * psKhac.tuSo;
		return kq;
	}

	public PhanSo chiaPhanSo(PhanSo psKhac) {
		PhanSo kq = new PhanSo();
		kq.mauSo = this.mauSo * psKhac.tuSo;
		kq.tuSo = this.tuSo * psKhac.mauSo;
		return kq;
	}

	public int timUCLN(int a, int b) {
		while (a != b) {
			if (a > b)
				a = a - b;
			else
				b = b - a;
		}
		return a;
	}

	public void toiGian() {
		int x = timUCLN(this.getTuSo(), this.getMauSo());
		this.setMauSo(this.getMauSo() / x);
		this.setTuSo(this.getTuSo() / x);
	}
}
