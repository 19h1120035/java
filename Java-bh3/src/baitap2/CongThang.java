package baitap2;

public class CongThang {
	private int thang;
	private double soNgayCong;
	private double soNgayNghiPhep;

	public CongThang(int thang, double soNgayCong, double soNgayNghiPhep) {
		this.thang = thang;
		this.soNgayCong = soNgayCong;
		this.soNgayNghiPhep = soNgayNghiPhep;
	}

	public int getThang() {
		return thang;
	}

	public void setThang(int thang) {
		this.thang = thang;
	}

	public double getSoNgayCong() {
		return soNgayCong;
	}

	public void setSoNgayCong(double soNgayCong) {
		this.soNgayCong = soNgayCong;
	}

	public double getSoNgayNghiPhep() {
		return soNgayNghiPhep;
	}

	public void setSoNgayNghiPhep(double soNgayNghiPhep) {
		this.soNgayNghiPhep = soNgayNghiPhep;
	}

	@Override
	public String toString() {
		return "\n\tCông tháng: " + thang + "\n\tSố ngày công: " + soNgayCong + "\n\tSố ngày nghỉ phép: "
				+ soNgayNghiPhep;
	}

}
