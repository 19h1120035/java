package QuanLySach;

import java.util.ArrayList;
import java.util.Scanner;

public class TacGia {
	String name;
	int age;
	String butDanh;
	String date;
	String address;

	// alt shift S O : Tạo constructor
	public TacGia() {
		super();
		this.name = name;
		this.age = age;
		this.butDanh = butDanh;
		this.date = date;
		this.address = address;
	}

	public TacGia(String butDanh2) {
		// TODO Auto-generated constructor stub
	}

	public void input(ArrayList<TacGia> listTacGia) {
		Scanner sc = new Scanner(System.in);
		input();
		System.out.println("\tNhập bút danh: ");
		while(true) {
			butDanh = sc.nextLine();
			boolean isFind = false;
			for (int i = 0; i < listTacGia.size(); i++) {
				if(listTacGia.get(i).getButDanh().equalsIgnoreCase(butDanh)){
					isFind = true;
				}
			}
			if(!isFind) {
				break;
			} else {
				System.err.println("Nhập bút danh khác!");
			} 
		}
	}
	public void input() {
		Scanner sc = new Scanner(System.in);
		System.out.println("\tNhập tên tác giả: ");
		name = sc.nextLine();
		System.out.println("\tNhập tuổi : ");
		age = Integer.parseInt(sc.nextLine());
		System.out.println("\tNgày sinh: ");
		date = sc.nextLine();
		System.out.println("\tNhập địa chỉ: ");
		address = sc.nextLine();
	}

	public void display() {
		System.out.println(toString());
	}

	// alt shift S R: Tạo getter và setter
	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getButDanh() {
		return butDanh;
	}

	public void setButDanh(String butDanh) {
		this.butDanh = butDanh;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	// alt shift S S: tạo tostring
	@Override
	public String toString() {
		return "\tTên tác giả : " + name + "\n\tTuổi: " + age + "\n\tBút danh: " + butDanh + "\n\tNgày sinh: " + date
				+ "\n\tĐịa chỉ: " + address + "\n";
	}

}
