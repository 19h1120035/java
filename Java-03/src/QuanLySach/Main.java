package QuanLySach;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		int choose, n;
		// Quản lý danh sách tác giả
		ArrayList<TacGia> listTacGia = new ArrayList<>();

		// Quản lý danh sách cuốn sách
		ArrayList<Book> listBook = new ArrayList<>();
		Scanner sc = new Scanner(System.in);
		do {
			showMenu();
			System.out.println("Nhập lựa chon: ");
			choose = Integer.parseInt(sc.nextLine());
			switch (choose) {
			case 1: {
				System.out.println("Nhập số sách cần thêm :");
				n = Integer.parseInt(sc.nextLine());
				for (int i = 0; i < n; i++) {
					System.out.println("Nhập thông tin sách "+(i+1)+" : ");
					Book book = new Book();
					book.input();
					boolean isFind = false;
					for (int j = 0; j < listTacGia.size(); j++) {
						if (listTacGia.get(j).getButDanh().equalsIgnoreCase(book.getButDanh())) {
							isFind = true;
							break;
						}
					}
					if (!isFind) {
						TacGia tacgia = new TacGia(book.getButDanh());
						tacgia.input();
					}

					// Lưu đối tượng book vào mảng listBook
					listBook.add(book);

				}
				break;
			}
			case 2: {
				System.out.println("Thông tin của sách :");
				for (Book book : listBook) {
					book.display();
				}
				break;
			}
			case 3: {
				// Nhập thông tin tác giả.
				System.out.println("Nhập số tác giả cần thêm: ");
				n = Integer.parseInt(sc.nextLine());
				for (int i = 0; i < n; i++) {
					System.out.println("Nhập tên tác giả thứ " + (i + 1) + " : ");
					TacGia tacgia = new TacGia();
					tacgia.input(listTacGia);
					// Lưu đối tượng tác giả vào mảng listTacGia
					listTacGia.add(tacgia);

				}
				break;
			}
			case 4: {
				System.out.println("Nhập tên bút danh cần tìm kiếm: ");
				String butDanh = sc.nextLine();
				for (int i = 0; i < listBook.size(); i++) {
					if (listBook.get(i).getButDanh().equalsIgnoreCase(butDanh)) {
						listBook.get(i).display();

					}

				}
				break;
			}
			case 5: {
				System.out.println("Good bye !");
				break;
			}
			default:
				System.err.println("Nhập sai ! Vui lòng nhập lại.");

			}
		} while (choose != 5);
	}

	public static void showMenu() {
		System.out.println("================MENU=================");
		System.out.println("1. Nhập thông tin sách.");
		System.out.println("2. Hiển thị tất cả sách ra màn hình.");
		System.out.println("3. Nhập thông tin tác giả.");
		System.out.println("4. Tìm kiếm sách theo bút danh.");
		System.out.println("5. Thoát.");
		System.out.println("=====================================");
	}
}
