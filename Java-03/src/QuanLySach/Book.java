package QuanLySach;

import java.util.Scanner;

public class Book {
	String bookName;
	String pubdate;
	String butDanh;

	public Book() {
		this.bookName = bookName;
		this.pubdate = pubdate;
		this.butDanh = butDanh;
	}

	public void input() {
		Scanner sc = new Scanner(System.in);
		System.out.println("\tNhập tên sách: ");
		bookName = sc.nextLine();
		System.out.println("\tNhập ngày xuất bản : ");
		pubdate = sc.nextLine();
		System.out.println("\tNhập bút danh: ");
		butDanh = sc.nextLine();
	}

	public void display() {
		System.out.println(toString());
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getPubdate() {
		return pubdate;
	}

	public void setPubdate(String pubdate) {
		this.pubdate = pubdate;
	}

	public String getButDanh() {
		return butDanh;
	}

	public void setButDanh(String butDanh) {
		this.butDanh = butDanh;
	}

	@Override
	public String toString() {
		return "\tTên sách: " + bookName + "\n\tNgày xuất bản: " + pubdate + "\n\tBút danh: " + butDanh+"\n";
	}

}
