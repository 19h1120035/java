package bt;

public class Static {
	int F[];
	String Input;

	Static(String InputStrinng) {
		F = new int[26];
		Input = InputStrinng;
	}

	void run() {
		for (int i = 0; i < Input.length(); i++) {
			char kt = Input.charAt(i);
			if (!(kt >= 'a' && kt <= 'z')) {
				continue;
			}
			int num = kt - 'a';
			F[num]++;
		}
	}

	void display() {
		for (int i = 0; i < F.length; i++) {
			char kt = (char) (i + 'a');
//			System.out.print(" | " + kt + " : " + F[i] + "   ");
			if (F[i] >= 5) {
				System.out.print(kt + " ");
			}
		}
	}
}
