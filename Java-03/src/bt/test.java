package bt;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class test {
	private final static String FILE_URL = "D:\\Code\\Java\\Eclipse_Workspace\\FileStatistic\\src\\bh\\SMSSpamCollection.txt";
	public static final char SPACE = ' ';
	public static final char TAB = '\t';
	public static final char BREAK_LINE = '\n';

	public static void main(String[] args) {
		String str = "";
		BufferedReader br = null;
		try {

			br = new BufferedReader(new FileReader(FILE_URL));

			String textInALine;

			while ((textInALine = br.readLine()) != null) {
				System.out.println(textInALine);
				str = str.concat(textInALine);
				textInALine = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Tree map

		Map<String, Integer> wordMap = countWords(str);

		Map sortedMap = valueSort(wordMap);

		// Get a set of the entries on the sorted map
		Set set = sortedMap.entrySet();

		// Get an iterator
		Iterator i = set.iterator();
		Iterator j = set.iterator();
		System.out.println("10 phần tử xuất hiện nhi�?u nhất là:  ");
		int a = 0, b = 0;
		// Display elements
		while (i.hasNext()) {
			Map.Entry mp = (Map.Entry) i.next();
			a++;
		}
		while (j.hasNext()) {
			b++;

			Map.Entry mp = (Map.Entry) j.next();
			if (b > (a - 10)) {
				System.out.print(mp.getKey() + ": ");
				System.out.println(mp.getValue());
			}
		}

	}

	public static Map<String, Integer> countWords(String input) {
		// WordMap
		Map<String, Integer> wordMap = new TreeMap<String, Integer>();
		if (input == null) {
			return wordMap;
		}
		int size = input.length();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			if (input.charAt(i) != SPACE && input.charAt(i) != TAB && input.charAt(i) != BREAK_LINE) {
				sb.append(input.charAt(i));
			} else {
				addWord(wordMap, sb);
				sb = new StringBuilder();
			}
		}
		addWord(wordMap, sb);
		return wordMap;
	}


	public static void addWord(Map<String, Integer> wordMap, StringBuilder sb) {
		String word = sb.toString();
		if (word.length() == 0) {
			return;
		}
		if (wordMap.containsKey(word)) {
			int count = wordMap.get(word) + 1;
			wordMap.put(word, count);
		} else {
			wordMap.put(word, 1);
		}
	}

	public static <K, V extends Comparable<V>> Map<K, V> valueSort(final Map<K, V> map) {

		Comparator<K> valueComparator = new Comparator<K>() {

			public int compare(K k1, K k2) {
				int comp = map.get(k1).compareTo(map.get(k2));
				if (comp == 0)
					return 1;
				else
					return comp;
			}

		};

		// SortedMap created using the comparator
		Map<K, V> sorted = new TreeMap<K, V>(valueComparator);

		sorted.putAll(map);

		return sorted;
	}

}
