package menubar_contentmenu_toolbar;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

public class HocMenu extends JFrame {
	JMenuBar menuBar; // nơi đựng chứa các menu
	JMenu mnuFile;
	JMenu mnuEdit;
	JMenu mnuHelp;

	JMenuItem mnuFileNew;
	JMenuItem mnuFileOpen;
	JMenuItem mnuFileExit;

	JMenuItem mnuEditCopy;
	JMenuItem mnuEditPast;
	JMenuItem mnuEditDelete;
 
	JMenuItem mnuHelpGui;
	JMenuItem mnuHelpAbout;

	JButton btn1, btn2;
	
	JMenuItem mnuPopupBlue, mnuPopupRed;
	JButton btnLastedChoose = null;
	
	JToolBar toolBar;
	JButton btnLenh1, btnLenh2;
	JCheckBox chk1, chk2;
	public HocMenu(String title) {
		super(title);
		addControls();
		addEvents();
	}

	private void addControls() {
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnuFile = new JMenu("File");
//		menuBar.add(mnuFile);
		mnuFileNew = new JMenuItem("New");
		// tạo phím tắt cho new
		mnuFileNew.setAccelerator(KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
		mnuFileOpen = new JMenuItem("Open");
		mnuFileExit = new JMenuItem("Exit");
//		mnuFileExit.setIcon(new ImageIcon("D:\\exit1.png"));
		mnuFile.add(mnuFileNew);
		mnuFile.addSeparator();
		mnuFile.add(mnuFileOpen);
		mnuFile.addSeparator();
		mnuFile.add(mnuFileExit);
		menuBar.add(mnuFile);

		mnuEdit = new JMenu("Edit");
//		menuBar.add(mnuEdit);
		mnuEditCopy = new JMenuItem("Copy");
		mnuEditPast = new JMenuItem("Past");
		mnuEditDelete = new JMenuItem("Delete");
		mnuEdit.add(mnuEditCopy);
		mnuEdit.addSeparator();
		mnuEdit.add(mnuEditPast);
		mnuEdit.addSeparator();
		mnuEdit.add(mnuEditDelete);
		menuBar.add(mnuEdit);

		mnuHelp = new JMenu("Help");
//		menuBar.add(mnuHelp);
		mnuHelpGui = new JMenuItem("Giới thiệu");
		mnuHelpAbout = new JMenuItem("About");
		mnuHelp.add(mnuHelpGui);
		mnuHelp.addSeparator();
		mnuHelp.add(mnuHelpAbout);
		menuBar.add(mnuHelp);

		Container con = getContentPane();
		con.setLayout(new BorderLayout());
		
		toolBar = new JToolBar();
		btnLenh1 = new JButton("Lệnh 1");
		btnLenh2 = new JButton("Lệnh 2");
		toolBar.add(btnLenh1);
		toolBar.add(btnLenh2);
		chk1 = new JCheckBox("Tự động lưu dữ liệu sau 10 phút");
		toolBar.add(chk1);
		con.add(toolBar, BorderLayout.NORTH);
		
		
		JPanel pnMain = new JPanel();
		pnMain.setLayout(new FlowLayout());
		btn1 = new JButton("Button 1");
		btn2 = new JButton("Button 2");
		pnMain.add(btn1);
		pnMain.add(btn2);
		con.add(pnMain,BorderLayout.CENTER);
		
		btn1.addMouseListener(new PopClickListener());
		btn2.addMouseListener(new PopClickListener());
		
		
	}

	// Bước 1: tạo 1 lớp PopUp để kế thừa từ lớp JPopUp
	// Bước 2: tạo 1 lớp để lắng nghe sự kiện nhấn chuột
	class PopUpDemo extends JPopupMenu {
		public PopUpDemo() {
			mnuPopupRed = new JMenuItem("Tô màu đỏ !");
			add(mnuPopupRed);
			mnuPopupBlue = new JMenuItem("Tô màu xanh !");
			add(mnuPopupBlue);
			mnuPopupRed.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if (btnLastedChoose != null) {
						btnLastedChoose.setBackground(Color.RED);
					}
				}
			});
			mnuPopupBlue.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if (btnLastedChoose != null) {
						btnLastedChoose.setBackground(Color.BLUE);
					}
				}
			});
		}
	}
	class PopClickListener extends MouseAdapter{
//		lắng nghe sự kiện của chuột
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) { // chuột phải
				doPop(e);
			}
		}
		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				doPop(e);
			}
		}

		private void doPop(MouseEvent e) {
			PopUpDemo menu = new PopUpDemo();
			btnLastedChoose = (JButton) e.getComponent();
			menu.show(e.getComponent(),e.getX(),e.getY());
		}
	}

	private void addEvents() {
		mnuFileNew.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Bạn vừa gõ ctrl N");
			}
		});
		mnuFileExit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnLenh1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Bạn chọn lệnh 1 trên tool Bar");
			}
		});
	}


	public void showWindow() {
		this.setSize(600, 400);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setState(MAXIMIZED_BOTH);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
