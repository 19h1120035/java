package bodertextarea;

import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class vidu2 extends JFrame {
	JTextField txtName;
	JTextArea txtAddress;
	Button btnOk ;
	
	JCheckBox chkDiBoi, chkDiXemPhim;
	JRadioButton radNam, radNu;
	ButtonGroup groupGender;
	public vidu2 (String title) {
		super(title);
		addControls();
		addEvents();
	}
	public void addControls() {
		Container con = getContentPane();
		JPanel pnMain = new JPanel();
		pnMain.setLayout(new BoxLayout(pnMain, BoxLayout.Y_AXIS));
		con.add(pnMain);
		
		JPanel pnInfo = new JPanel();
		pnMain.add(pnInfo);
		pnInfo.setLayout(new BoxLayout(pnInfo, BoxLayout.Y_AXIS));
		
		JPanel pnName = new JPanel();
		pnName.setLayout(new FlowLayout());
		JLabel lblName = new JLabel("Nhập tên: ");
		txtName = new JTextField(20);
		pnName.add(lblName);
		pnName.add(txtName);
		pnInfo.add(pnName);
		
		JPanel pnAddress = new JPanel();
		pnAddress.setLayout(new FlowLayout());
		JLabel lblAddress = new JLabel("Địa chỉ : ");
		txtAddress = new JTextArea(5, 19);
		
		txtAddress.setWrapStyleWord(true);
		txtAddress.setLineWrap(true);
		JScrollPane sc = new JScrollPane(txtAddress,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnAddress.add(lblAddress);
		pnAddress.add(sc);
		pnInfo.add(pnAddress);
		
		Border borderInfo = BorderFactory.createLineBorder(Color.blue);
		TitledBorder borderTitleInfo = new TitledBorder(borderInfo, "Thông tin");
		pnInfo.setBorder(borderTitleInfo);
		borderTitleInfo.setTitleColor(Color.red);
		borderTitleInfo.setTitleJustification(TitledBorder.CENTER);
		
		JPanel pnSoThichVaGender = new JPanel();
//		pnSoThichVaGender.setLayout(new BoxLayout(pnSoThichVaGender, BoxLayout.X_AXIS));
		pnSoThichVaGender.setLayout(new GridLayout(1,2));
		JPanel pnSoThich = new JPanel();
		pnSoThich.setPreferredSize(new Dimension(200,100));
		Border borderSoThich = BorderFactory.createLineBorder(Color.GREEN);
		TitledBorder titleBorderSoThich = new TitledBorder(borderSoThich, "Sở thích");
		pnSoThich.setBorder(titleBorderSoThich);
		pnSoThich.setLayout(new BoxLayout(pnSoThich,BoxLayout.Y_AXIS));
		chkDiBoi = new JCheckBox("Đi bơi");
		chkDiXemPhim = new JCheckBox("Đi xem phim");
		pnSoThich.add(chkDiBoi);
		pnSoThich.add(chkDiXemPhim);
		pnSoThichVaGender.add(pnSoThich);
		
		JPanel pnGender = new JPanel();
		pnGender.setPreferredSize(new Dimension(400,100));
		Border borderGender = BorderFactory.createLineBorder(Color.GREEN);
		TitledBorder titleBorderGender = new TitledBorder(borderGender, "Giới tính");
		pnGender.setBorder(titleBorderGender);
		pnGender.setLayout(new BoxLayout(pnGender, BoxLayout.Y_AXIS));
		radNam = new JRadioButton("Nam");
		radNu = new JRadioButton("Nữ");
		groupGender = new ButtonGroup();
		groupGender.add(radNam);
		groupGender.add(radNu);
		pnGender.add(radNam);
		pnGender.add(radNu);
		pnSoThichVaGender.add(pnGender);
		pnMain.add(pnSoThichVaGender);
		
		
		JPanel pnOk = new JPanel();
		pnOk.setLayout(new FlowLayout(FlowLayout.RIGHT));
		btnOk = new Button("OK");
		pnOk.add(btnOk);
		pnMain.add(pnOk);
		
		
	}
	public void addEvents() {
		btnOk.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyLayThongTin();
			}
		});
	}
	protected void xuLyLayThongTin() {
		String msg ="Họ và tên: "+ txtName.getText() +"\n";
		msg+="Địa chỉ: "+ txtAddress.getText()+"\n";
		String sothich = "Sở thích: ";
		if (chkDiBoi.isSelected()) {
			sothich +="\t"+chkDiBoi.getText();
		}
		if(chkDiXemPhim.isSelected()) {
			sothich+="\t"+chkDiXemPhim.getText();
		}
		msg += sothich;
		String gender = "";
		if (radNam.isSelected()) {
			gender ="\nGiới tính: "+ radNam.getText()+"\n";
		} else {
			gender = "\nGiới tính: "+ radNu.getText()+"\n";
		}
		msg+=gender;
		JOptionPane.showMessageDialog(btnOk, msg);
	}
	public void showWindow() {
		this.setSize(400, 400);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	

}
