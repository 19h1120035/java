package jtee;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.plaf.basic.BasicScrollBarUI;
import javax.swing.tree.DefaultMutableTreeNode;

public class HocJTree extends JFrame {
	DefaultMutableTreeNode root = null;
	JTree tree;
	
	public HocJTree(String title) {
		super(title);
		addControls();
		addEvents();
	}
	public void addControls() {
		Container con = getContentPane();
		JPanel pnTree = new JPanel();
		pnTree.setLayout(new BorderLayout());
		root = new DefaultMutableTreeNode("Chu Thương");
		tree = new JTree(root);
		DefaultMutableTreeNode node1 = new DefaultMutableTreeNode("Lập trình Java");
		root.add(node1);
		DefaultMutableTreeNode node1_unit1= new DefaultMutableTreeNode("Chương 1: Lập trình Java cơ bản");
		node1.add(node1_unit1);
		DefaultMutableTreeNode node1_unit2= new DefaultMutableTreeNode("Chương 2: Lập trình hướng đối tượng");
		node1.add(node1_unit2);
		DefaultMutableTreeNode node1_unit3= new DefaultMutableTreeNode("Chương 3: Lập trình đa luồng");
		node1.add(node1_unit3);
		DefaultMutableTreeNode node1_unit4= new DefaultMutableTreeNode("Chương 4: Lập trình giao diện UI");
		node1.add(node1_unit4);
		DefaultMutableTreeNode node1_unit5= new DefaultMutableTreeNode("Chương 5: Lập trình cơ sở dữ liệu");
		node1.add(node1_unit5);
		DefaultMutableTreeNode node2 = new DefaultMutableTreeNode("Lập trình Web");
		root.add(node2);
		DefaultMutableTreeNode node2_unit1= new DefaultMutableTreeNode("Chương 1: HTML");
		node2.add(node2_unit1);
		DefaultMutableTreeNode node2_unit2= new DefaultMutableTreeNode("Chương 2: CSS");
		node2.add(node2_unit2);
		DefaultMutableTreeNode node2_unit3= new DefaultMutableTreeNode("Chương 3: PHP");
		node2.add(node2_unit3);
		DefaultMutableTreeNode node2_unit4= new DefaultMutableTreeNode("Chương 4: MySQL");
		node2.add(node2_unit4);
		DefaultMutableTreeNode node2_unit5= new DefaultMutableTreeNode("Chương 5: JavaScript");
		node2.add(node2_unit5);
		
		DefaultMutableTreeNode node3 = new DefaultMutableTreeNode("Lập trình C#");
		root.add(node3);
		tree.expandRow(0);
		JScrollPane sc = new JScrollPane(tree, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnTree.add(sc,BorderLayout.CENTER);
		con.setLayout(new BorderLayout());
		pnTree.setPreferredSize(new Dimension(300,0));
		
		JPanel pnRight = new JPanel();
		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,pnTree,pnRight);
		con.add(sp,BorderLayout.CENTER);
		
	}
	public void addEvents() {
		tree.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				Object obj = tree.getLastSelectedPathComponent(); // chính là cái node trên giao diện được chọn
				DefaultMutableTreeNode nodeSelected = (DefaultMutableTreeNode) obj;
				JOptionPane.showMessageDialog(null, nodeSelected.getUserObject()+"");
				
			}
		});
	}
	
	public void showWindow() {
		this.setSize(800, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setState(MAXIMIZED_BOTH);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
