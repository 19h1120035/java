package giaipt;
 
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class PTB1UI extends JFrame {
	JTextField txtA, txtB, txtResult;
	JButton btnGiai, btnExit, btnHelp;

	ActionListener eventGiai = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			// Xử lý giải pt
			giaiPT();
		}
	};

	public PTB1UI(String title) {
		super(title);
		addControls();
		addEvents();
	}

	protected void giaiPT() {
		String Sa = txtA.getText();
		String sB = txtB.getText();
		double a = Double.parseDouble(Sa);
		double b = Double.parseDouble(sB);
		if (a == 0 && b == 0) {
			// gán giá trị lên kết quả
			txtResult.setText("Phương trình vô số nghiệm");
		} else if (a == 0 && b != 0) {
			txtResult.setText("Phương trình vô nghiệm !!");
		} else {
			txtResult.setText("x = " + (-b / a));
		}
	}

	public void addEvents() {
		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnGiai.addActionListener(eventGiai);
		btnHelp.addActionListener(new HelpEvents());
	}

	public void addControls() {
		Container con = getContentPane();
		JPanel pnMain = new JPanel();
		pnMain.setLayout(new BoxLayout(pnMain, BoxLayout.Y_AXIS));
		con.add(pnMain);

		JPanel pnTitle = new JPanel();
		pnTitle.setLayout(new FlowLayout());
		JLabel lblTieuDe = new JLabel("Giải phương trình bậc 1");
		pnTitle.add(lblTieuDe);
		pnMain.add(pnTitle);
		lblTieuDe.setForeground(Color.BLUE);
		Font fontTieuDe = new Font("arial", Font.BOLD, 20);
		lblTieuDe.setFont(fontTieuDe);

		JPanel pnA = new JPanel();
		pnA.setLayout(new FlowLayout());
		JLabel lblA = new JLabel("Hệ số a: ");
		txtA = new JTextField(15);
		pnA.add(lblA);
		pnA.add(txtA);
		pnMain.add(pnA);

		JPanel pnB = new JPanel();
		pnB.setLayout(new FlowLayout());
		JLabel lblB = new JLabel("Hệ số b: ");
		txtB = new JTextField(15);
		pnB.add(lblB);
		pnB.add(txtB);
		pnMain.add(pnB);

		JPanel pnBtn = new JPanel();
		pnBtn.setLayout(new FlowLayout());
		btnGiai = new JButton("Giải");
		btnExit = new JButton("EXIT");
		btnExit.setIcon(new ImageIcon("img/exit.jpg"));
		btnHelp = new JButton("HELP");
		pnBtn.add(btnGiai);
		pnBtn.add(btnExit);
		pnBtn.add(btnHelp);
		pnMain.add(pnBtn);

		JPanel pnRes = new JPanel();
		pnRes.setLayout(new FlowLayout());
		JLabel lblRes = new JLabel("Kết quả: ");
		txtResult = new JTextField(15);
		pnRes.add(lblRes);
		pnRes.add(txtResult);
		pnMain.add(pnRes);
	}
	
	class  HelpEvents implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(null, "Chi tiết xin liên hệ sau");
		} 
		
	}
	public void showWindow() {
		this.setSize(400, 250);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

}
