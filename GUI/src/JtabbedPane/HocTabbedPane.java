package JtabbedPane;

import java.awt.Color;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class HocTabbedPane extends JFrame {
	JTabbedPane tab;
	public HocTabbedPane(String title) {
		super(title);
		addControls();
		addEvents();
	}
	private void addEvents() {
		
	}
	private void addControls() {
		Container con = getContentPane();
		tab = new JTabbedPane();
		con.add(tab);
		JPanel pnTab1 = new JPanel();
		JPanel pnTab2 = new JPanel();
		JPanel pnTab3 = new JPanel();
		tab.add(pnTab1,"Cấu Hình");
		tab.add(pnTab2,"Bảo Mật");
		tab.add(pnTab3,"Phân Quyền");
		
		pnTab1.setBackground(Color.RED);
		pnTab2.setBackground(Color.BLUE);
		pnTab3.setBackground(Color.YELLOW);
		
		pnTab1.add(new JButton("Đây là Button"));
		pnTab2.add(new JTextField("hello"));
		}
	public void showWindow() {
		this.setSize(600, 400);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setState(MAXIMIZED_BOTH);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}