package split_panes_jtable;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class HocTable extends JFrame {
	DefaultTableModel dtm;
	JTable tblSinhVien;

	JButton btnAdd, btnDel;

	public HocTable(String title) {
		super(title);
		addControls();
		addEvents();
	}
 
	private void addControls() {
		Container con = getContentPane();
		JPanel pnLeft = new JPanel();
		JPanel pnRight = new JPanel();
		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, pnLeft, pnRight);
		con.add(sp);

		dtm = new DefaultTableModel(); // tạo bảng
		dtm.addColumn("Mã"); // tạo cột cho bảng
		dtm.addColumn("Tên");
		dtm.addColumn("Địa chỉ");
		// tạo hàng cho bảng
		String row1[] = { "SV1", "Đào Văn Thương", "Biên Hòa" };
		dtm.addRow(row1);
		String row2[] = { "SV2", "Nguyễn Thị Hiếu", "Bình Định" };
		dtm.addRow(row2);

		Vector<String> vec3 = new Vector<String>();
		vec3.add("SV3");
		vec3.add("Nguyễn Việt Long");
		vec3.add("Phù Cát");
		dtm.addRow(vec3);

		tblSinhVien = new JTable(dtm);
		JScrollPane scTable = new JScrollPane(tblSinhVien, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnLeft.setLayout(new BorderLayout());
		pnLeft.add(scTable, BorderLayout.CENTER);
		pnLeft.setLayout(new BorderLayout());
		pnLeft.add(scTable, BorderLayout.CENTER);

		JPanel pnButton = new JPanel();
		pnButton.setLayout(new FlowLayout());
		btnAdd = new JButton("Thêm Mới");
		btnDel = new JButton("Xóa");
		pnButton.add(btnAdd);
		pnButton.add(btnDel);
		pnLeft.add(pnButton, BorderLayout.SOUTH);

	}

	public void addEvents() {
		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Vector<String> vec = new Vector<String>();
				vec.add("SV4");
				vec.add("Ngô Lê Hiếu");
				vec.add("Kiều Huyên");
				dtm.addRow(vec);
			}
		});
		btnDel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int row = tblSinhVien.getSelectedRow();
				if (row == -1) return ;
				dtm.removeRow(row);// xóa dòng đang chọn
			}
		});
		tblSinhVien.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = tblSinhVien.getSelectedRow();
				if (row == -1) return;
				String ma = tblSinhVien.getValueAt(row, 0) +"";
				String ten = tblSinhVien.getValueAt(row, 1) +"";
				String address = tblSinhVien.getValueAt(row, 2)+"";
				JOptionPane.showMessageDialog(null, "Mã SV: "+ma+"\nHọ và tên: "+ten+"\nĐịa chỉ: "+address);
			}
		});

	}

	public void showWindow() {
		this.setSize(600, 400);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setState(MAXIMIZED_BOTH);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
