import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
 
public class PreparedStatementExample {
    private static String DB_URL = "jdbc:mysql://localhost:2004/danhba";
    private static String USER_NAME = "root";
    private static String PASSWORD = "20042001";
     
    public static void main(String[] args) {
        String sqlInsert = "INSERT INTO qldanhba VALUES(?, ?, ?)";
        String selectAll = "SELECT * FROM qldanhba";
        try {
            // connect to database
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(DB_URL, 
                    USER_NAME, PASSWORD);
             
            // crate statement to insert student
            PreparedStatement stmt = conn.prepareStatement(sqlInsert);
            stmt.setString(1, "Vinh");
            stmt.setString(2, "Hanoi");
            stmt.setString(3, "298475293745");
            stmt.setString(4, "983456456456");
            stmt.setInt(5, 6);
            stmt.execute();
             
            // select all student
            stmt = conn.prepareStatement(selectAll);
            // get data from table 'student'
            ResultSet rs = stmt.executeQuery();
            // show data
            while (rs.next()) {
                System.out.println(rs.getInt(5) + "  " + rs.getString(1) 
                        + "  " + rs.getString(2)+ "  " + rs.getString(3)+ "  " + rs.getString(4));
            }
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}