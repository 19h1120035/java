public class Edge {

    Node start;
    Node end;
    int w;
    boolean visited;

    Edge(Node pstart,Node pend,int w){
        start = pstart;
        end = pend;
        this.w = w;
    }
}