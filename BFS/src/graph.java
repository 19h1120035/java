import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class graph {
	
	
	ArrayList<String> names=new ArrayList<String>();
	ArrayList<Node> nodes=new ArrayList<Node>();
	boolean visited = true;

	void add(String a,String b,int w)
	{
		if(!names.contains(a))
		{
			Node temp=new Node(a);
			nodes.add(temp);
			names.add(a);
		}
		if(!names.contains(b))
		{
			Node temp1=new Node(b);
			nodes.add(temp1);
			names.add(b);
		}
		makeEdge(a, b, w);
		
	}
	
	void makeEdge(String a,String b, int w)
	{
		Node temp1=search(a);
		Node temp2=search(b);
		temp1.makeedge(temp2, w);
	}
	
	Node search(String a)
	{
		for (Node  n : nodes) 
		{
			if(n.name.equals(a))
			{
				return n;
			}
		}
		return null;
	}
	
	void DFS(String a)
	{
		Node temp=search(a);
		if(!temp.visited)
		{
			//temp.visited=true;
			rec(temp);
			
		}
	}
	void rec(Node a){
		a.visited=true;
		
		for (Edge  e : a.edges) 
		{
			if(!e.end.visited)
			{
				
				rec(e.end);
				System.out.println(e.start.name+"-->"+e.end.name+" "+e.w);
			}
		}
	}
	
	void BFS(String a)
	{
		Queue<Node> q = new LinkedList<Node>();
		Node temp = search(a);
		q.add(temp);
		temp.visited = true;
		while(!q.isEmpty())
		{
			Node n = (Node)q.poll();
			for(Edge e: n.edges)
			{
				if(!e.visited)
				{
					e.visited = true;
					((List<String>) e).add(a);
				}
			}
		}
	}

}