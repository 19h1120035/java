import java.util.ArrayList;


public class Node {
	String name;
	ArrayList<Edge> edges=new ArrayList<Edge>();
	boolean visited;
	
	Node(String pname){
		name=pname;
		visited=false;
	}
	void makeedge(Node pend,int w){
		Edge temp=new Edge(this,pend,w);
		edges.add(temp);
	}
	

}