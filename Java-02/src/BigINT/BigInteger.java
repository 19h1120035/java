package BigINT;

import java.util.Scanner;
import java.util.regex.Pattern;

public class BigInteger {
	public static String inputNumber(String mess) {
		Scanner sc = new Scanner(System.in);
		String number = "";
		Pattern p = Pattern.compile("^[0-9]+$");
		while (true) {
			System.out.println(mess);
			number = sc.nextLine();
			if (p.matcher(number).find()) {
				break;
			} else {
				System.err.println("Invalid Number ! Please Input Again.");
			}
		}
		return number;
	}

	public static void main(String[] args) {
		String a = inputNumber("Input Number a: ");
		java.math.BigInteger number1 = new java.math.BigInteger(a);
		String b = inputNumber("Input Number b: ");
		java.math.BigInteger number2 = new java.math.BigInteger(b);
		java.math.BigInteger result = number1.multiply(number2);
		System.out.println("Result: " + result);
	}
}
