package CheckPW;
import java.util.Scanner;
import java.util.regex.Pattern;
//19H1120035

public class CheckAccount {
	public static final int MSSV_LENGTH = 10;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String regex = "^\\d\\dH\\d+$";
		System.out.println("======QUY TAC DANG NHAP MSSV======");
		System.out.println("1. Mssv phai co dung 10 ki tu.   ");
		System.out.println("2. Mssv phai co it nhat 1 chu cai in hoa.");
		System.out.println("----------------------------------\n");
//		Pattern p = Pattern.compile("^[A-Z]?$");
//		Pattern p1 = Pattern.compile("^[^0-9]?.{10}[A-Z]?$");
		Pattern p2 = Pattern.compile(regex);
		while (true) {
			System.out.println("Nhap MSSV cua ban: ");
			String mssv = sc.nextLine();
			if (p2.matcher(mssv).find()&& (mssv.length() == MSSV_LENGTH)) {
				System.out.println("MSSV cua ban hop le !");
				break;
			} else
				System.err.println("MSSV khong hop le !\nVui long nhap lai !!!");

		}
	}
}
