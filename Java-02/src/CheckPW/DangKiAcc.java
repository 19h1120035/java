package CheckPW;
import java.util.Scanner;
import java.util.regex.Pattern;

public class DangKiAcc {
	public static final int PASSWORD_LENGTH = 10;
	public static final int MSSV_LENGTH = 10;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("VUI LONG DANG NHAP TAI KHOAN SINH VIEN CUA BAN :");
		System.err.println(
				"CHU Y: La sinh vien cua Vien CLC UTransport.\nMat khau phai chua it nhat hai chu so; it nhat 10 ki tu; chi bao gom ki tu chu va ki tu so.");
		accMSSV();
	}

	public static String accMSSV() {
		Scanner sc = new Scanner(System.in);
		String regex = "^\\d\\dH\\d+$";
		String mssv = "";
		Pattern p2 = Pattern.compile(regex);
		while (true) {
			System.out.println("NHAP MSSV CUA BAN: ");
			mssv = sc.nextLine();
			if (p2.matcher(mssv).find() && (mssv.length() == MSSV_LENGTH)) {
				passWord();
				System.out.println("MSSV: " +mssv);
				break;
			} else
				System.err.println("MSSV khong hop le !\nVui long nhap lai MSSV !!!");
		}
		return mssv;
	}

	public static String passWord() {
		Scanner sc = new Scanner(System.in);
		String regex = "^[a-z0-9]*\\d\\d[a-z0-9]*$";
		String password = "";
		Pattern p = Pattern.compile(regex);
		while (true) {
			System.out.println("NHAP MAT KHAU CUA BAN (Dong y voi cac dieu kien tren): ");
			password = sc.nextLine();
			if (p.matcher(password).find() && password.length() >= PASSWORD_LENGTH) {
				System.out.println("BAN DA DANG NHAP THANH CONG!");
				System.out.println("\n-----------------------------------");
				System.out.println("\tTAI KHOAN CUA BAN: \n");
				System.out.println("PW: "+password);
				break;
			} else
				System.err.println("Mat khau khong hop le!\nVui long nhap lai mat khau!");
		}
		return password;
	}
}
