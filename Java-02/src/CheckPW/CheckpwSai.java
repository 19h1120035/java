package CheckPW;
import java.util.Scanner;
import java.util.regex.Pattern;

public class CheckpwSai {
	public static void main(String[] args) {
		System.out.println("===============QUY TAC MAT KHAU================");
		System.out.println("1. Mat khau phai co it nhat 10 ki tu.          ");
		System.out.println("2. Mat khau chi bao gom cac chu so va chu cai. ");
		System.out.println("3. Mat khau phai chua it nhat hai chu so.      \n");
		String pass = passWord();
		System.out.println("Mat khau cua ban la: " + pass);
	}

	public static String passWord() {
		Scanner sc = new Scanner(System.in);
		String password = "";
		Pattern p1 = Pattern.compile("^[A-Za-z0-9].{10,}$");// .{10,} : quy tắc 1;  [A-Za-z0-9]: quy tắc 2;
		Pattern p2 = Pattern.compile("^[0-9A-Za-z]*[0-9]+[0-9A-Za-z]*$"); //quy tắc 3;
		while (true) {
			System.out.println("Nhap mat khau cua ban (Dong y voi cac dieu kien tren): ");
			password = sc.nextLine();
			if (p1.matcher(password).find() && p2.matcher(password).find()) {
				System.out.println("Mat khau hop le!");
				break;
			} else
				System.err.println("Mat khau khong hop le!");
		}
		return password;
	}
}
