package CheckPW;
import java.util.Scanner;
import java.util.regex.Pattern;

/*
 * định dạng mail : sdfg345@gmail.com
 * 					fghdf45@hotmail.com
 * 					sdfs356@email.com
 */
public class CheckMail {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String regex = "^\\w+\\d+@\\w+mail.com$";
//		String regex = "^\\w+\\d+@\\w+mail.com$";
//		\\w+ : So sánh với 1 hoặc nhiều kí tự chữ.
//		\\d+ : So sánh với 1 hoặc nhiều kí tự số trong chuỗi.
//		String regex = "^\\w+\\d*@\\w+mail.com$";
//		\\d* : So sánh với 0 hoặc nhiều lần xuất hiện của kí tự số.
		String mail = "";
		Pattern p = Pattern.compile(regex);
		while (true) {
			System.out.println("Nhập mail của bạn: ");
			mail = sc.nextLine();
			if (p.matcher(mail).find()) {
				System.out.println("Mail của bạn hợp lệ !");
				break;
			} else
				System.err.println("Mail của bạn không hợp lệ !\nVui lòng nhập lại mail !!!");
		}
	}
}
