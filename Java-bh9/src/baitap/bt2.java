package baitap;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;

public class bt2 extends JFrame {

	private JPanel jp;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					bt2 frame = new bt2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public bt2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 150, 450, 300);
		setTitle("Bài Tập 2");
		jp = new JPanel();
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(jp);
		jp.setLayout(null);
		
		JLabel lb1 = new JLabel("MSSV: 19H1120035");
		lb1.setForeground(Color.GREEN);
		lb1.setHorizontalAlignment(SwingConstants.CENTER);
		lb1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lb1.setBounds(93, 11, 231, 27);
		jp.add(lb1);
		
		JLabel lb2 = new JLabel("Họ và tên: Đào Văn Thương");
		lb2.setForeground(Color.GREEN);
		lb2.setHorizontalAlignment(SwingConstants.CENTER);
		lb2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lb2.setBounds(49, 43, 327, 27);
		jp.add(lb2);
		
		JButton btn = new JButton("EXIT");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btn.setFont(new Font("Tahoma", Font.BOLD, 18));
		btn.setBounds(167, 213, 89, 37);
		jp.add(btn);
		
		JLabel lb3 = new JLabel("Ảnh chân dung:");
		lb3.setFont(new Font("Tahoma", Font.ITALIC, 14));
		lb3.setBounds(22, 93, 115, 27);
		jp.add(lb3);
		
		JLabel lb4 = new JLabel("");
		lb4.setIcon(new ImageIcon("E:\\anhchandung.png"));
		lb4.setBounds(153, 81, 185, 121);
		jp.add(lb4);
	}
}
