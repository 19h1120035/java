package baitap;

import java.awt.*;
import java.awt.event.*;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class baitap3 extends JFrame {

	private JPanel jp;

	private final JLabel label = new JLabel("New label");
	private JTextField textField2;
	private JTextField textField1;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					baitap3 frame = new baitap3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public baitap3() {
		setTitle("Bài Tập 3");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		jp = new JPanel();
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(jp);
		jp.setLayout(null);
		
		JLabel lbl1 = new JLabel("Tìm Ước Số Chung Lớn Nhất");
		lbl1.setForeground(Color.GREEN);
		lbl1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		lbl1.setBounds(40, 11, 334, 14);
		jp.add(lbl1);
		
		JLabel lbl2 = new JLabel("Nhập số nguyên thứ nhất:");
		lbl2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl2.setBounds(25, 57, 190, 29);
		jp.add(lbl2);
		
		textField1 = new JTextField();
		textField1.setBounds(242, 57, 109, 26);
		jp.add(textField1);
		textField1.setColumns(10);
		
		JLabel lbl3 = new JLabel("Nhập số nguyên thứ hai:");
		lbl3.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl3.setBounds(25, 106, 179, 29);
		jp.add(lbl3);
		
		textField2 = new JTextField();
		textField2.setBounds(242, 106, 109, 26);
		jp.add(textField2);
		textField2.setColumns(10);
		
		JButton btn = new JButton("USCLN");
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String n1 = textField1.getText();
				String n2 = textField2.getText();
				int s1 = Integer.parseInt(n1);
				int s2 = Integer.parseInt(n2);
				int kq = uscln(s1, s2);
				JOptionPane.showMessageDialog(btn, "Ước chung lớn nhất của "+ s1+" và "+s2 +" = "+kq);
			}
		});
		btn.setForeground(Color.BLUE);
		btn.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		btn.setBounds(159, 175, 116, 31);
		jp.add(btn);
	}
	public int uscln(int n1, int n2) {
    // tìm ước số chung lớn nhất
    while (n1 != n2) {
        if (n1 > n2) {
            n1-= n2;
        } else {
            n2 -= n1;
        }
    }
    return n1;
	}
}
