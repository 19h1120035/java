package baitap;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class vidu1 extends JFrame implements ActionListener {
	JButton okButton, exitButton, cancelButton;
	
	public vidu1 () {
		createAndShow();
	}
	public void createAndShow() {
		okButton = new JButton("OK");
		exitButton = new JButton("EXIT");
		cancelButton = new JButton("CANCEL");
		setTitle("Ví dụ");
		setSize(400, 300);
		setBounds(500,200,400,300);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.add(okButton);
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		
		
		this.add(exitButton);
		exitButton.addActionListener(this);
		exitButton.setActionCommand("exit");
		
		this.add(cancelButton);
		cancelButton.addActionListener(this);
		cancelButton.setActionCommand("cancel");
		
		this.setLayout(new FlowLayout());
		setVisible(true);
		
		
	}
	public static void main(String[] args) {
		new vidu1();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if ("ok".equals(e.getActionCommand())) {
			JOptionPane.showMessageDialog(okButton, "Hello Swing !");
		}
		if ("exit".equals(e.getActionCommand())) {
			System.exit(0);
		}
		if ("cancel".equals(e.getActionCommand())) {
			JOptionPane.showConfirmDialog(cancelButton, "Bạn có chắc chắn hủy bỏ không ?");
			
		}
	}

}
