package baitap;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class baitap1 extends JFrame {
	JButton button;

	public baitap1() {
		createAndShow();
	}

	public void createAndShow() {
		button = new JButton("OK");
		setSize(400,300);
		setBounds(200, 200, 400, 300);
		setTitle("Bài tập 1");
		this.setLocationRelativeTo(null); // hiển thị giữa màn hình
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // dừng khi tắt cửa sổ
		this.add(button); // thêm button vào cửa sổ
		this.setLayout(new FlowLayout());
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(rootPane, "Đào Văn Thương \nHello Swing !");
			}
		});
//		this.pack(); // hiển thị kích thước cỡ vừa với cửa sổ
	}

	public static void main(String[] args) {
		new baitap1();
	}
}
