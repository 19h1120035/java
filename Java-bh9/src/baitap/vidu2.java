package baitap;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class vidu2 extends JFrame implements ActionListener {
	JButton okButton, exitButton, cancelButton;
	
	public vidu2 () {
		createAndShow();
	}
	public void createAndShow() {
		okButton = new JButton("OK");
		exitButton = new JButton("EXIT");
		cancelButton = new JButton("CANCEL");
		setTitle("Ví dụ");
		setSize(400, 300);
		setBounds(500,200,400,300);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.add(okButton);
		okButton.setActionCommand("ok");
		okButton.addActionListener(this);
		okButton.setMnemonic(KeyEvent.VK_O); 
		okButton.setToolTipText("Click vào nút này để hiển thị !");
		
		this.add(exitButton);
		exitButton.addActionListener(this);
		exitButton.setActionCommand("exit");
		exitButton.setMnemonic(KeyEvent.VK_E);
		exitButton.setToolTipText("Click vào nút này để thoát !");
		this.add(cancelButton);
		cancelButton.addActionListener(this);
		cancelButton.setActionCommand("cancel");
		cancelButton.setMnemonic(KeyEvent.VK_C);
		cancelButton.setToolTipText("Click vào nút này để hủy thao tác!");
		
		this.setLayout(new FlowLayout());
		setVisible(true);
		
		
	}
	public static void main(String[] args) {
		new vidu2();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if ("ok".equals(e.getActionCommand())) {
			JOptionPane.showMessageDialog(okButton, "Hello Swing !");
		}
		if ("exit".equals(e.getActionCommand())) {
			System.exit(0);
		}
		if ("cancel".equals(e.getActionCommand())) {
			JOptionPane.showConfirmDialog(cancelButton, "Bạn có chắc chắn hủy bỏ không ?");
			
		}
	}

}
