package baitap;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import javax.swing.JTextField;

public class baitap2 extends JFrame {

	private JPanel jp;

	public static void main(String[] args) {
		baitap2 frame = new baitap2();
		frame.setVisible(true);
	}

	public baitap2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Bài tập 2");
		setBounds(500, 150, 500, 400);
		jp = new JPanel();
		jp.setBorder(new EmptyBorder(5, 5, 5, 5));
		jp.setLayout(new BorderLayout(0, 0));
		setContentPane(jp);
		
		JLabel lbl1 = new JLabel("MSSV: 19H1120035");
		lbl1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lbl1.setBackground(Color.LIGHT_GRAY);
		jp.add(lbl1, BorderLayout.NORTH);
		
		JLabel lbl2 = new JLabel("Họ và tên: Đào Văn Thương");
		lbl2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lbl2.setHorizontalAlignment(SwingConstants.CENTER);
		jp.add(lbl2, BorderLayout.SOUTH);
		
		JButton btn1 = new JButton("EXIT");
		btn1.setActionCommand("Yes");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					System.exit(0);
			}
		});
		btn1.setForeground(Color.RED);
		btn1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 17));
		btn1.setBackground(Color.LIGHT_GRAY);
		jp.add(btn1, BorderLayout.EAST);
		
		JButton btn2 = new JButton("Ảnh Chân Dung");
		btn2.setFont(new Font("Tahoma", Font.BOLD, 15));
		btn2.setToolTipText("Đây là ảnh chân dung!");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn2.setIcon(new ImageIcon("E:\\anhchandung.png"));
		jp.add(btn2, BorderLayout.CENTER);
	}

}
