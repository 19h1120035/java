package BT_MultiThread_01;

import java.util.Random;

public class Thread1 extends Thread {
	SharedData sharedData;

	public Thread1(SharedData sharedData) {
		this.sharedData = sharedData;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int minCode = (int) 'a';// mã của các kí tự
		int maxCode = (int) 'z';
		int limit = maxCode - minCode;

		Random random = new Random();
		while (sharedData.checkAvaiable()) {
			synchronized (sharedData) {
				int rad = random.nextInt(limit) + minCode;
				char c = (char) rad;
				System.out.println(c);

				sharedData.setRadC(c);
				sharedData.notifyAll();
				try {
					sharedData.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// delay
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// thay đổi thời gian lên hệ thống
				sharedData.addTime(2000);
			}
		}
		System.out.println("Thread 1 stop !");
		synchronized (sharedData) {
			sharedData.notifyAll();
		}
	}

}
