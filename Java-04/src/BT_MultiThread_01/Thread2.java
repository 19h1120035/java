package BT_MultiThread_01;


public class Thread2 extends Thread {
	SharedData sharedData;
	
	public Thread2(SharedData sharedData) {
		this.sharedData = sharedData;
	}

	@Override

	public void run() {
		while (sharedData.checkAvaiable()) {
			synchronized (sharedData) {
				sharedData.notifyAll();
				try {
					sharedData.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				char c = sharedData.getRadC();
				// chuyển ký tự c => ký tự C (chuyển thường thành hoa)
				int code = (int) c - 32;
				c = (char) code;
				System.out.println("Thread2 : "+ c);

				// delay thời gian => 1s
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// add thêm vào SharedData
				sharedData.addTime(1000);
			}
		}
		System.out.println("Thread 2 stop !");
	}

}
