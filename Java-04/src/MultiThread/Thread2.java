package MultiThread;

import DeadLock.SharedData;

public class Thread2 extends Thread {
	SharedData sharedData;

	public Thread2(SharedData sharedData) {
		this.sharedData = sharedData;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			synchronized (sharedData) {
//				System.out.println("T2 > i >  " + i);
				sharedData.notifyAll();
				if (i == 9) {
					System.out.println("Stop t2!");
					stop();
				}
				try {
					sharedData.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				int result = sharedData.rad * sharedData.rad;
				System.out.println("t2 > " + result);
			}
		}
	}

}
