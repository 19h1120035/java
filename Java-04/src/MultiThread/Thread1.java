package MultiThread;

import java.util.Random;

import DeadLock.SharedData;

public class Thread1 extends Thread {
	SharedData sharedData;

	public Thread1(SharedData sharedData) {
		this.sharedData = sharedData;
	}

	@Override
	public void run() {
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
			synchronized (sharedData) {
//				System.out.println("T1 > i >  " + i);
//				try {
//					sharedData.wait();
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

				int rad = random.nextInt(10);
				sharedData.rad = rad;
				System.out.println("t1 > " + rad);

				sharedData.notifyAll();
				if (i == 9) {
					System.out.println("Stop t1");
					stop();
				}
				try {
					sharedData.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}
}
