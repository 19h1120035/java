package Train_Thread;

import BT_MultiThread.Thread2;

public class Main {
	public static void main(String[] args) {
		// implement từ interface Runnable
		MyThread1 my1 = new MyThread1();
		Thread t = new Thread(my1);
		
		// extends từ class thread
		MyThread2 my2 = new MyThread2();
		t.start();
		my2.start(); 
	}
}
