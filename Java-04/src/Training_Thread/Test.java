package Training_Thread;

import java.util.ArrayList;
import java.util.List;


public class Test {
	public static void main(String[] args) {
		MyBank mybank = new MyBank(10000000);
		
//		mybank.withDraw(800, "T1");
//		mybank.withDraw(800, "T2");
//		mybank.withDraw(800, "T3");
		
		List<WithDrawThread> threadList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			threadList.add(new WithDrawThread("Thread :"+i,mybank));
		}
		
		for (WithDrawThread withDrawThread : threadList) {
			withDrawThread.start();
		}
	}
}
