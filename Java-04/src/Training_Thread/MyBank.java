package Training_Thread;

public class MyBank {
	int money;
	
	public MyBank() {
		
	}

	public MyBank(int money) {
		super();
		this.money = money;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}
	
	public synchronized void withDraw(int money, String threadName) {
		if (money <=this.money) {
			System.out.println("So tien can rut: "+ money+", Thread: "+threadName);
			this.money -= money;
		} else {
			System.out.println("So tien can rut vuot qua so tien hien tai ! , "+threadName);
		}
		System.out.println("So tien hien tai: "+ this.money);
	}
	
}
