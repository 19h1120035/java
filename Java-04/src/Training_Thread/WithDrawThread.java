package Training_Thread;

public class WithDrawThread extends Thread {
	String name;
	MyBank myBank;
	
	public WithDrawThread(String name, MyBank myBank) {
		this.name = name;
		this.myBank = myBank;
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			myBank.withDraw(800000,name);
		}
	}
}
