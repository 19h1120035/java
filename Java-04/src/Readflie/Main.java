package Readflie;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public void writeToFile(List<Student> list) {
		try {
			FileWriter fw = new FileWriter("data.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			for (Student o: list) {
				bw.write(o.toString());
				bw.newLine();
			}
			bw.close();
			fw.close();
		} catch (IOException e) {
		}
	}
	public List<Student> readFromFile(){
		List<Student> list = new ArrayList<>();
		try {
			FileReader fr = new FileReader("data.txt");
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			while (true){
				line = br.readLine();
				if(line ==null ) {
					break;
				}
				String txt[] = line.split(";");
				String name = txt[0];
				int age = Integer.parseInt(txt[1]);
				double mark = Double.parseDouble(txt[2]);
				list.add (new Student(name, age, mark));
				System.out.println(line);
			}
		} catch (Exception e) {
		}
		return list;
	}
	public static void main(String[] args) {
		Main m = new Main();
		List<Student> list = m.readFromFile();
		for(Student o: list) {
			System.out.println(o);
		}
	}
}
