package Thread;

// C�ch 5
public class ThreadTwo implements Runnable {
	ThreadOne t1;

	public ThreadTwo() {
	}

	public ThreadTwo(ThreadOne t1) {
		this.t1 = t1;
	}

	@Override
	public void run() {
		System.out.println("T2 is running");
		try {
			System.out.println("Join t1 into t2");
			t1.join();
			System.out.println("t1 is finished");
			for (int i = 0; i < 10; i++) {
				try {
					System.out.println("ThreadTwo >" + i);
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}