package Thread;


public class Main {
	public static void main(String[] args) {
		// Thread main => luồng chính
		System.out.println("Start");
		// Cách 1
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 100; i++) {
					System.out.println("Thread 1 > " + i);

				}
			}
		});
		t.start();

		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 100; i++) {
					System.out.println("Thread 2 > " + i);

				}
			}
		});
		t2.start();
		
		Thread t3 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		});
		t3.start();
		
		// Cách 2
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		}).start();;
		
		// Cách 3: Cú pháp lambda
		new Thread(()-> {
			//code
		}).start();

		System.out.println("Ends");
	}
}
