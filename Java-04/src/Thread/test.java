package Thread;

public class test {
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Thread main start");
		ThreadOne thr1 = new ThreadOne();
		thr1.setName("A");
		thr1.start();
		ThreadTwo thr2 = new ThreadTwo();
//		new Thread(thr2).start();
		
		Thread t = new Thread(thr2);
		t.start();

		System.out.println("Thực hiện nối luồng 1 vào luồng main");
		thr1.join(); // nối luồng, thread 1 chạy xong rồi mới tới thread main khi đó luồng 1 và luồng 2 chạy song song 
		System.out.println("Thực hiện nối luồng 2 vào luồng main");
		t.join(); // tại thời điểm này luồng 1 đã chạy xong, thì ở đây chỉ chạy luồng 2, sau khi chạy xong thì in ra luồng main ở dưới 
		System.out.println("Thread main stop");
//		try {
//			Thread.sleep(1000);
//			thr1.stop();
//			t.stop();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}
