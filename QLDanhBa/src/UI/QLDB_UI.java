package UI;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;

public class QLDB_UI extends JFrame {
	private static final DefaultTableModel modelTest = null;
	DefaultTableModel dtm;
	JTable tblDanhBa;

	JTextField txtID, txtTen, txtAddress, txtPhone, txtPhone1;
	JButton btnThem, btnXoa, btnSua, btnExit;
	Statement stmt;

	public QLDB_UI(String title) {
		super(title);
		addControls();
		addEvents();
	}

	private void addEvents() {
		btnExit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int ret = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn thoát chương trình không ?",
						"Xác nhận Thoát", JOptionPane.YES_NO_OPTION);
				if (ret == JOptionPane.YES_OPTION) {
					System.exit(0);
				}				
			}
		});
		btnThem.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				String id = txtID.getText();
				String ten = txtTen.getText();
				String address = txtAddress.getText();
				String phone = txtPhone.getText();
				String phone1 = txtPhone1.getText();
				try {
					ketNoi();
					String sql = "Insert into qldanhba values('" + id + "','" + ten + "','" + address + "','" + phone
							+ "','" + phone1 + "')";
					stmt.executeUpdate(sql);
					ResultSet res = stmt.executeQuery("select * from qldanhba where id =" + id + " order by id asc");
					while (res.next()) {
						String row2[] = { res.getString(1), res.getString(2), res.getString(3), res.getString(4),
								res.getString(5) };
						dtm.addRow(row2);
					}
					JOptionPane.showMessageDialog(null, "Thêm Danh bạ thành công !");

				} catch (SQLException e1) {
					e1.printStackTrace();

				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});
		tblDanhBa.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int rowSelected = tblDanhBa.getSelectedRow();
				if (rowSelected == -1)
					return;
				String id = tblDanhBa.getValueAt(rowSelected, 0) + "";
				String ten = tblDanhBa.getValueAt(rowSelected, 1) + "";
				String address = tblDanhBa.getValueAt(rowSelected, 2) + "";
				String phone = tblDanhBa.getValueAt(rowSelected, 3) + "";
				String phone1 = tblDanhBa.getValueAt(rowSelected, 4) + "";
				txtID.setText(id);
				txtTen.setText(ten);
				txtAddress.setText(address);
				txtPhone.setText(phone);
				txtPhone1.setText(phone1);
			}
		});
		btnSua.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				String ten = txtTen.getText();
				String address = txtAddress.getText();
				String phone = txtPhone.getText();
				String phone1 = txtPhone1.getText();
				int rowSelected = tblDanhBa.getSelectedRow();
				if (rowSelected == -1)
					return;
				String id = tblDanhBa.getValueAt(rowSelected, 0) + "";
				int idd = Integer.parseInt(id);
				try {
					ketNoi();
					String sql = "UPDATE public.qldanhba SET name= '" + ten + "', address='" + address + "', phone= '"
							+ phone + "', phone1='" + phone1 + "' WHERE id = " + idd + "";
					stmt.executeUpdate(sql);
					ResultSet res = stmt.executeQuery("select * from qldanhba where id= " + idd + " order by id");
					while (res.next()) {
						String row2[] = { res.getString(1), res.getString(2), res.getString(3), res.getString(4),
								res.getString(5) };
						dtm.addRow(row2);
					}
					JOptionPane.showMessageDialog(null, "Sửa Danh bạ thành công !");

				} catch (SQLException e1) {
					e1.printStackTrace();

				}
			}
		});
		btnXoa.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				xuLyXoa();
			}
		});
	}

	protected void xuLyXoa() {
		int ret = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn xóa danh bạ này không ?",
				"Xác nhận Xóa danh bạ", JOptionPane.YES_NO_OPTION);
		int rowSelected = tblDanhBa.getSelectedRow();
		if (rowSelected == -1)
			return;
		String id = tblDanhBa.getValueAt(rowSelected, 0) + "";
		if (ret == JOptionPane.YES_OPTION) {
			try {
				ketNoi();
				String sql = "DELETE FROM public.qldanhba WHERE id =" + id + "";
				stmt.executeUpdate(sql);
				dtm.fireTableDataChanged();
				tblDanhBa.setModel(dtm);
				JOptionPane.showMessageDialog(null, "Xóa Danh bạ thành công !");

			} catch (SQLException e1) {
				e1.printStackTrace();

			}
		}
	}

	private void addControls() {
		Container con = getContentPane();
		con.setLayout(new BorderLayout());
		JPanel pnTop = new JPanel();
		pnTop.setPreferredSize(new Dimension(0, 350));
		JPanel pnBottom = new JPanel();
		JSplitPane sp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, pnTop, pnBottom);
		con.add(sp, BorderLayout.CENTER);

		pnTop.setLayout(new BorderLayout());
		dtm = new DefaultTableModel();
		dtm.addColumn("ID");
		dtm.addColumn("Tên");
		dtm.addColumn("Địa chỉ");
		dtm.addColumn("Số điện thoại 1");
		dtm.addColumn("Số điện thoại 2");
		try {
			ketNoi();
			String sql = "select * from qldanhba order by id asc";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String row1[] = { rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5) };
				dtm.addRow(row1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		tblDanhBa = new JTable(dtm);
		JScrollPane scTable = new JScrollPane(tblDanhBa, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnTop.add(scTable, BorderLayout.CENTER);

		pnBottom.setLayout(new BoxLayout(pnBottom, BoxLayout.Y_AXIS));
		JPanel pnID = new JPanel();
		pnID.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblID = new JLabel("ID: ");
		txtID = new JTextField(20);
		pnID.add(lblID);
		pnID.add(txtID);
		pnBottom.add(pnID);

		JPanel pnTen = new JPanel();
		pnTen.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblTen = new JLabel("Tên: ");
		txtTen = new JTextField(20);
		pnTen.add(lblTen);
		pnTen.add(txtTen);
		pnBottom.add(pnTen);

		JPanel pnAddress = new JPanel();
		pnAddress.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblAddress = new JLabel("Địa chỉ: ");
		txtAddress = new JTextField(20);
		pnAddress.add(lblAddress);
		pnAddress.add(txtAddress);
		pnBottom.add(pnAddress);

		JPanel pnPhone = new JPanel();
		pnPhone.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPhone = new JLabel("Số điện thoại: ");
		txtPhone = new JTextField(20);
		pnPhone.add(lblPhone);
		pnPhone.add(txtPhone);
		pnBottom.add(pnPhone);

		JPanel pnPhone1 = new JPanel();
		pnPhone1.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblPhone1 = new JLabel("Số điện thoại khác (nếu có): ");
		txtPhone1 = new JTextField(20);
		pnPhone1.add(lblPhone1);
		pnPhone1.add(txtPhone1);
		pnBottom.add(pnPhone1);

		JPanel pnButton = new JPanel();
		pnButton.setLayout(new FlowLayout(FlowLayout.LEFT));
		btnThem = new JButton("Thêm");
		btnSua = new JButton("Sửa");
		btnXoa = new JButton("Xóa");
		btnExit = new JButton("Thoát");
		btnExit.setLayout(new FlowLayout(FlowLayout.RIGHT));
		pnButton.add(btnThem);
		pnButton.add(btnSua);
		pnButton.add(btnXoa);
		pnButton.add(btnExit);
		pnBottom.add(pnButton);

		// canh chỉnh đều lable
		lblID.setPreferredSize(lblPhone1.getPreferredSize());
		lblTen.setPreferredSize(lblPhone1.getPreferredSize());
		lblAddress.setPreferredSize(lblPhone1.getPreferredSize());
		lblPhone.setPreferredSize(lblPhone1.getPreferredSize());
		lblPhone1.setPreferredSize(lblPhone1.getPreferredSize());

	}

	public void ketNoi() throws SQLException {
		String url = "jdbc:postgresql://localhost:2004/danhba";
		Properties props = new Properties();
		props.setProperty("user", "postgres");
		props.setProperty("password", "20042001");
		Connection conn = DriverManager.getConnection(url, props);
		stmt = conn.createStatement();

	}

	public void showWindow() {
		this.setSize(800, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
}
